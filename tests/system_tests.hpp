// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_SYSTEM_TESTS_HPP
#define BTK_SYSTEM_TESTS_HPP

#include <vector>
#include <cstdlib>

#include <boost/detail/lightweight_test.hpp>

#include <btk/core/concepts/system_concept.hpp>
#include <btk/core/molecules/molecule.hpp>
#include <btk/core/molecules/polymer.hpp>
#include <btk/core/molecules/monomer.hpp>
#include <btk/core/atoms/atom.hpp>

#include "polymer_structure_tests.hpp"

typedef BTK::ATOMS::Atom<> atom;
typedef BTK::MOLECULES::Monomer<atom> monomer;
typedef BTK::MOLECULES::Polymer<monomer> polymer;
typedef BTK::MOLECULES::Molecule<atom> molecule;

struct RandomPolymerGenerator
{
  polymer operator()(void) const
  {
    unsigned size = std::rand() / (unsigned)(((unsigned)RAND_MAX + 1) / 30) + 1;
 
    polymer p(size);
    std::generate(p.polymer_begin(),p.polymer_end(),RandomMonomerGenerator());

    return p;
  }
};

struct RandomMoleculeGenerator
{
  molecule operator()(void) const
  {
    unsigned size = std::rand() / (unsigned)(((unsigned)RAND_MAX + 1) / 300) + 1;

    molecule m(size);
    std::generate(m.structure_begin(),m.structure_end(),RandomAtomGenerator());
    
    return m;
  }
};

template <typename SystemType, typename ChainGeneratorType>
void test_system_type()
{
  using BTK::CONCEPTS::MutableSystemConcept;
  boost::function_requires<MutableSystemConcept<SystemType> >();

  test_btk_sequence_type<SystemType,ChainGeneratorType>();
  
  SystemType s(5);
  std::generate(s.system_begin(),s.system_end(),ChainGeneratorType());
  
  test_system(s);
}

template <typename SystemType>
void test_system(SystemType & s)
{
  BOOST_TEST(!s.empty());

  using BTK::CONCEPTS::SystemConcept;
  boost::function_requires<SystemConcept<SystemType> >();

  test_btk_container(s);
  test_const_system(s);

  // test chain iteration
  test_iteration(s.system_begin(),s.system_end(),
		 s.num_chains());
  test_iteration(s.system_rbegin(),s.system_rend(),
		 s.num_chains());

  // test atom iteration
  test_iteration(s.structure_begin(),s.structure_end(),
		 s.num_atoms());
  test_iteration(s.structure_rbegin(),s.structure_rend(),
		 s.num_atoms());

  SystemType s2(s);
  BOOST_TEST(s2 == s);
  BOOST_TEST(std::equal(s.system_begin(), s.system_end(),
			s2.system_begin()));
}

template <typename SystemType>
void test_const_system(SystemType const & s)
{
  BOOST_TEST(!s.empty());
  
  using BTK::CONCEPTS::SystemConcept;
  boost::function_requires<SystemConcept<SystemType> >();

  BOOST_TEST(s.size() == s.num_chains());

  test_const_btk_container(s);
  
  // test chain iteration
  test_iteration(s.system_begin(),s.system_end(),
		 s.num_chains());
  test_iteration(s.system_rbegin(),s.system_rend(),
		 s.num_chains());

  // test atom iteration
  test_iteration(s.structure_begin(),s.structure_end(),
		 s.num_atoms());
  test_iteration(s.structure_rbegin(),s.structure_rend(),
		 s.num_atoms());
}

#endif // BTK_SYSTEM_TESTS_HPP
