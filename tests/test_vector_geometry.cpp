// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004-2006, Christopher Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <iostream>
#include <boost/detail/lightweight_test.hpp>

#include <btk/core/math/linear_algebra.hpp>

using namespace std;
using namespace BTK;
using BTK::MATH::BTKVector;
using BTK::MATH::rad2deg;
using BTK::MATH::deg2rad;

int main()
{
  // setup hypothetical build of C from N,CA,CB
  // using the C-CA-N angle and the C-CA-CB angle
  //

  BTKVector ca(0.000,  0.000,  0.000);
  BTKVector  n(-0.409,  1.392,  0.000);
  BTKVector cb(-0.595, -0.774, -1.216);

  double ca_c_length   = 1.5229;
  double n_ca_c_angle  = deg2rad(106.3800);
  double cb_ca_c_angle = deg2rad(112.4500);

  cerr << "testing: set_vector_from_two_angles" << endl;

  BTKVector c = set_vector_from_two_angles( ca, n, cb, ca_c_length, n_ca_c_angle, cb_ca_c_angle);

  cerr << "c: " << c << endl;

  cerr << "norm_2(c-ca) " << norm_2(c-ca) << endl;
  cerr << "angle c-ca-cb " << rad2deg(point_angle(c,ca,cb)) << endl;
  cerr << "angle c-ca-n " << rad2deg(point_angle(c,ca,n)) << endl;


  BOOST_TEST( fabs( norm_2(c-ca)-1.5229 )  < 0.0001 );
  BOOST_TEST( fabs( rad2deg(point_angle(c,ca,cb)) - 112.45 ) < 0.01 );
  BOOST_TEST( fabs( rad2deg(point_angle(c,ca,n)) - 106.38 ) < 0.01 );

  BOOST_TEST( fabs(c[0] - 1.5229) < 0.001 );
  BOOST_TEST( fabs(c[1] - 0.0000) < 0.001 );
  BOOST_TEST( fabs(c[2] - 0.0000) < 0.001 );

  return boost::report_errors();
}

