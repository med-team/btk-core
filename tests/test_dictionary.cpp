// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <ostream>
#include <boost/detail/lightweight_test.hpp>
#include <btk/core/io/dictionary.hpp>

typedef BTK::IO::Dictionary<int> dictionary;

int main() 
{
  dictionary d;
  BOOST_TEST(d.begin() == d.end());

  // insert tests
  BOOST_TEST(d.insert(dictionary::value_type(1,"ONE")));
  
  BOOST_TEST(d.insert(dictionary::value_type(2,"TWO")));
  BOOST_TEST(d.insert(dictionary::value_type(3,"THREE")));
  BOOST_TEST(d.insert(dictionary::value_type(4,"FOUR")));
  BOOST_TEST(d.insert(dictionary::value_type(314,"THREE HUNDRED FOURTEEN")));

  // try to insert a duplicate ID.
  BOOST_TEST(!d.insert(dictionary::value_type(4,"DUPE_FOUR")));
  BOOST_TEST(d.find("DUPE_FOUR") == d.end());

  // try to insert a new ID with a duplicate symbol
  BOOST_TEST(!d.insert(dictionary::value_type(10,"TWO")));
  BOOST_TEST(d.find("TWO")->first == 2);
    
  {
    // find by type
    dictionary::const_iterator i = d.find(4);
    
    BOOST_TEST(i != d.end());
    BOOST_TEST(i->first == 4);
    BOOST_TEST(i->second == "FOUR");
  }

  {
    // find by symbol
    dictionary::const_iterator i = d.find("THREE");

    BOOST_TEST(i != d.end());
    BOOST_TEST(i->first == 3);
    BOOST_TEST(i->second == "THREE");
  }

  // try to find non-existent values
  BOOST_TEST(d.find(666) == d.end());
  BOOST_TEST(d.find("FOOBAR") == d.end());

  // add an alternate symbol to a type
  BOOST_TEST(d.add_alternate_symbol(314,"FOOBAR"));
  BOOST_TEST(d.find("FOOBAR") != d.end());
  BOOST_TEST(d.find("FOOBAR")->first == 314);
  BOOST_TEST(d.find("THREE HUNDRED FOURTEEN")->first == 314);

  // try to define a duplicate alternate symbol.
  BOOST_TEST(!d.add_alternate_symbol(4,"FOOBAR"));
  BOOST_TEST(d.find("FOOBAR")->first == 314);
  
  // try to define an alternate symbol for a non-existent type
  BOOST_TEST(!d.add_alternate_symbol(666,"BETTER NOT WORK"));
  BOOST_TEST(d.find("BETTER NOT WORK") == d.end());
  
  return boost::report_errors();
} 
