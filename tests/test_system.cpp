// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <btk/core/molecules/system.hpp>

#include "system_tests.hpp"

typedef BTK::MOLECULES::System<polymer> polymer_system;
typedef BTK::MOLECULES::System<molecule> molecule_system;

int main()
{
  {
    polymer_system p;
    molecule_system m;
    
    test_system_type<polymer_system,RandomPolymerGenerator>();
    test_system_type<molecule_system,RandomMoleculeGenerator>();

    // uncomment this line to check that monomer iteration on an atomic system
    // object fails to compile (as it should!):
    //test_iteration(m.polymer_begin(),m.polymer_end(),m.num_monomers());
  }

  {
    // test iteration for polymer system
    polymer_system p(10);
    std::generate(p.system_begin(),p.system_end(),RandomPolymerGenerator());
    test_iteration(p.polymer_begin(),p.polymer_end(),p.num_monomers());
    
    // atom iteration was already tested by test_system_type<>(), but you
    // can uncomment this line if you're especially paranoid:
    //test_iteration(p.structure_begin(),p.structure_end(),p.num_atoms());
  }

  return boost::report_errors();
}
