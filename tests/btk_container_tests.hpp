// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONTAINER_TESTS_HPP
#define BTK_CONTAINER_TESTS_HPP

#include <vector>
#include <algorithm>
#include <iterator>

#include <btk/core/concepts/btk_container_concept.hpp>

#include "test_common.hpp"


template <typename SequenceType, typename Generator>
void test_btk_sequence_type()
{
  typedef SequenceType sequence;
  typedef typename SequenceType::value_type value_type;
  typedef typename SequenceType::size_type size_type;
  typedef typename SequenceType::iterator iterator;
  typedef typename SequenceType::const_iterator const_iterator;
  typedef std::vector<value_type> value_vector;

  // Create and fill a value vector
  value_vector vec(123);
  Generator gen;
  std::generate(vec.begin(),vec.end(),gen);

  {
    sequence c1;
    BOOST_TEST(c1.empty());
  
    sequence c2(30);
    BOOST_TEST(c2.size() == 30);
  
    sequence c3(size_type(30),value_type());
    BOOST_TEST(c3.size() == 30);
  }

  sequence c4(vec.begin(),vec.end());
  BOOST_TEST(std::equal(vec.begin(),vec.end(),c4.begin()));

  sequence c5(c4);
  BOOST_TEST(c4 == c5);
  BOOST_TEST(std::equal(c4.begin(),c4.end(),c5.begin()));

  // Test BTKContainer methods (const and non-const)
  test_btk_container(c5);
  
  //
  // Test STL Sequence methods.
  //

  // insert(pos,value)
  value_type v = gen();
  c5.insert(c5.begin(),v);
  BOOST_TEST(c5.front() == v);
  
  c5.insert(c5.end(),v);
  BOOST_TEST(*(--c5.end()) == v);

  // clear()
  c5.clear();
  BOOST_TEST(c5.empty());
  
  // insert(pos,n,value)
  c5.insert(c5.begin(),size_type(30),v);
  BOOST_TEST(c5.size() == 30);
  BOOST_TEST(c5.front() == v);

  // insert(pos,InputIterator,InputIterator)
  c5.clear();
  c5.insert(c5.begin(),vec.begin(),vec.end());
  BOOST_TEST(std::equal(vec.begin(),vec.end(),c5.begin()));

  // erase(pos)
  c5.erase(c5.begin());
  BOOST_TEST(c5.size() == vec.size()-1);

  // erase(p,q)
  iterator i = c5.begin();
  i++; i++; i++;
  c5.erase(i,c5.end());
  BOOST_TEST(c5.size() == 3);
  c5.erase(c5.begin(),c5.end());
  BOOST_TEST(c5.empty());

  // resize(n,t)
  c5.resize(10,v);
  BOOST_TEST(c5.size() == 10);
  BOOST_TEST(c5.front() == v);

  // resize(n)
  c5.resize(5);
  BOOST_TEST(c5.size() == 5);
  BOOST_TEST(c5.front() == v);
}

template <typename T>
void test_btk_container(T & container)
{
  boost::function_requires<BTK::CONCEPTS::MutableBTKContainerConcept<T> >();

  BOOST_TEST(!container.empty());

  // test const methods.
  test_const_btk_container(container);

  // test non-const iteration
  test_iteration(container.begin(),container.end(),
                 container.size());
  test_iteration(container.rbegin(),container.rend(),
                 container.size());

  // test bracket operators (get last element)
  unsigned idx = container.size() - 1;
  typename T::const_iterator i = container.begin();
  std::advance(i,idx);

  BOOST_TEST(container[idx] == *i);			       

  // test ReversibleContainer methods
  T c2, c3;
  
  c2 = container;
  BOOST_TEST(container == c2);
  
  c3.swap(c2);
  BOOST_TEST(container != c2);
  BOOST_TEST(container == c3);
  BOOST_TEST(c2.empty());
}

template <typename T>
void test_const_btk_container(T const & container)
{
  boost::function_requires<BTK::CONCEPTS::BTKContainerConcept<T> >();

  BOOST_TEST(!container.empty());

  test_iteration(container.begin(),container.end(),
                  container.size());
  test_iteration(container.rbegin(),container.rend(),
                 container.size());

  // test bracket operators (get last element)
  unsigned idx = container.size() - 1;
  typename T::const_iterator i = container.begin();
  std::advance(i,idx);

  BOOST_TEST(container[idx] == *i);
}

#endif // BTK_CONTAINER_TESTS_HPP
