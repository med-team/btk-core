// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_POLYMER_STRUCTURE_TESTS_HPP
#define BTK_POLYMER_STRUCTURE_TESTS_HPP

#include <vector>
#include <cstdlib>

#include <boost/detail/lightweight_test.hpp>

#include <btk/core/concepts/polymer_structure_concept.hpp>
#include <btk/core/concepts/chain_concept.hpp>
#include <btk/core/molecules/monomer.hpp>
#include <btk/core/atoms/atom.hpp>

#include "atomic_structure_tests.hpp"

typedef BTK::MOLECULES::Monomer<BTK::ATOMS::Atom<> > monomer;

struct RandomMonomerGenerator
{
  monomer operator()(void) const
  {
    unsigned size = std::rand() / (unsigned)(((unsigned)RAND_MAX + 1) / 30) + 1;
    
    std::vector<BTK::ATOMS::Atom<> > v(size);
    std::generate(v.begin(),v.end(),RandomAtomGenerator());

    int type = std::rand() / (int)(((int)RAND_MAX + 1) / 500);
    int num = std::rand() / (int)(((int)RAND_MAX + 1) / 1000);
    
    return monomer(v.begin(),v.end(),type,num);
  }
};

template <typename MutablePolymerStructureType>
void test_mutable_polymer_structure_type()
{
  using BTK::CONCEPTS::MutablePolymerStructureConcept;
  boost::function_requires<MutablePolymerStructureConcept<MutablePolymerStructureType> >();
  using BTK::CONCEPTS::ChainConcept;
  boost::function_requires<ChainConcept<MutablePolymerStructureType> >();

  // Test the BTKSequence-required methods.
  test_btk_sequence_type<MutablePolymerStructureType,RandomMonomerGenerator>();
  
  MutablePolymerStructureType ps(29);
  std::generate(ps.polymer_begin(),ps.polymer_end(),RandomMonomerGenerator());

  // Test the Polymer-specific methods.
  test_polymer_structure(ps);
}

template <typename PolymerStructureType>
void test_polymer_structure(PolymerStructureType & ps)
{
  BOOST_TEST(!ps.empty());

  using BTK::CONCEPTS::PolymerStructureConcept;
  boost::function_requires<PolymerStructureConcept<PolymerStructureType> >();
  using BTK::CONCEPTS::ChainConcept;
  boost::function_requires<ChainConcept<PolymerStructureType> >();

  test_btk_container(ps);
  test_const_polymer_structure(ps);

  test_iteration(ps.polymer_begin(),ps.polymer_end(),
		 ps.size());
  test_iteration(ps.polymer_rbegin(),ps.polymer_rend(),
		 ps.size());

  PolymerStructureType p2(ps);
  BOOST_TEST(p2 == ps);
  BOOST_TEST(std::equal(ps.polymer_begin(), ps.polymer_end(),
			p2.polymer_begin()));  
}

template <typename PolymerStructureType>
void test_const_polymer_structure(PolymerStructureType const & ps)
{
  BOOST_TEST(!ps.empty());

  using BTK::CONCEPTS::PolymerStructureConcept;
  boost::function_requires<PolymerStructureConcept<PolymerStructureType> >();
  using BTK::CONCEPTS::ChainConcept;
  boost::function_requires<ChainConcept<PolymerStructureType> >();

  BOOST_TEST(ps.size() == ps.num_monomers());

  test_const_btk_container(ps);
  test_iteration(ps.polymer_begin(),ps.polymer_end(),ps.size());
  test_iteration(ps.polymer_rbegin(),ps.polymer_rend(),ps.size());
}

#endif 
