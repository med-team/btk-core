// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_TEST_COMMON_HPP
#define BTK_TEST_COMMON_HPP

#include <boost/detail/lightweight_test.hpp>

template <typename Iterator>
void test_iteration(Iterator begin, Iterator end,
                    unsigned size,
		    bool extra_paranoid = false)
{
  Iterator i(begin);
  unsigned N = 0;

  while (i != end) { ++i; ++N; }
  BOOST_TEST(N == size);
  BOOST_TEST(i == end);

  N = 0;
  while (i != begin) { --i; ++N; }
  BOOST_TEST(N == size);
  BOOST_TEST(i == begin);

  if (extra_paranoid) {
    // Iterate using the postfix notation, just in case...
    N = 0;

    while (i++ != end) ++N;
    BOOST_TEST(N == size);
    BOOST_TEST(i == end);

    N = 0;

    while (i-- != begin) ++N;
    BOOST_TEST(N == size);
  }

}

#endif // BTK_TEST_COMMON_HPP
