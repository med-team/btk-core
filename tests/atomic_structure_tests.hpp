// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_ATOMIC_STRUCTURE_TESTS_HPP
#define BTK_ATOMIC_STRUCTURE_TESTS_HPP

#include <cstdlib>
#include <algorithm>

#include <boost/detail/lightweight_test.hpp>

#include <btk/core/concepts/atomic_structure_concept.hpp>
#include <btk/core/atoms/atom.hpp>
#include <btk/core/math/btk_vector.hpp>

#include "btk_container_tests.hpp"

struct RandomAtomGenerator
{
  typedef BTK::ATOMS::Atom<>::chemical_type_system type_system;
  typedef BTK::ATOMS::Atom<>::atom_id_type atom_id_type;
  typedef BTK::ATOMS::Atom<>::element_id_type element_id_type;
  
  BTK::ATOMS::Atom<> operator()(void) const 
  {
    double x = std::rand();
    double y = std::rand();
    double z = std::rand();

    return BTK::ATOMS::Atom<>(BTK::MATH::BTKVector(x,y,z),
                              atom_id_type(),
                              element_id_type(),
                              _num++,
                              _dictionary);
  }

  static type_system _dictionary;
  static int _num;
};

RandomAtomGenerator::type_system RandomAtomGenerator::_dictionary;
int RandomAtomGenerator::_num = 1;

template <typename MutableAtomicStructureType>
void test_mutable_atomic_structure_type()
{
  using BTK::CONCEPTS::MutableAtomicStructureConcept;
  boost::function_requires<MutableAtomicStructureConcept<MutableAtomicStructureType> >();

  // Test the methods required by BTKSequence
  test_btk_sequence_type<MutableAtomicStructureType,RandomAtomGenerator>();
  
  MutableAtomicStructureType as(314);

  std::generate(as.structure_begin(),as.structure_end(),RandomAtomGenerator());

  // Test the methods required by AtomicStructure
  test_atomic_structure(as); 
}

template <typename AtomicStructureType>
void test_atomic_structure(AtomicStructureType & a)
{
  BOOST_TEST(!a.empty());

  using BTK::CONCEPTS::AtomicStructureConcept;
  boost::function_requires<AtomicStructureConcept<AtomicStructureType> >();

  // Test the methods required by BTKContainer.
  test_btk_container(a);

  // test const methods
  test_const_atomic_structure(a);

  // test atom iteration
  test_iteration(a.structure_begin(),a.structure_end(),
                 a.size());

  // test reverse atom iteration
  test_iteration(a.structure_rbegin(),a.structure_rend(),
                 a.size());

  // test copy construction.
  AtomicStructureType a2(a);
  BOOST_TEST(a2 == a);
  BOOST_TEST(std::equal(a.structure_begin(),a.structure_end(),
			a2.structure_begin()));
}

template <typename AtomicStructureType>
void test_const_atomic_structure(AtomicStructureType const & a)
{
  BOOST_TEST(!a.empty());

  using BTK::CONCEPTS::AtomicStructureConcept;
  boost::function_requires<AtomicStructureConcept<AtomicStructureType> >();

  BOOST_TEST(a.size() == a.num_atoms());

  // test the const methods required by BTKContainer
  test_const_btk_container(a);
  
  // test atom iteration
  test_iteration(a.structure_begin(),a.structure_end(),
                 a.size());

  // test reverse atom iteration
  test_iteration(a.structure_rbegin(),a.structure_rend(),
                 a.size());
}

#endif
