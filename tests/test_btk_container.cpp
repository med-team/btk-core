// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <vector>
#include <list>

#include <btk/core/utility/btk_sequence.hpp>
#include "btk_container_tests.hpp"

struct gen_seq {
  
  gen_seq() : i(0) {}

  int operator()() const { return i++; }

  mutable int i;
};

int main()
{
  typedef BTK::UTILITY::BTKSequence<int,std::vector<int> > vector_seq;
  typedef BTK::UTILITY::BTKSequence<int,std::list<int> > list_seq;

  // These tests are currently disabled because the BTKContainer
  // and BTKSequence classes have protected comparison and assignment
  // methods (for good reason!)
  //
  // It would be nice to rewrite the test harness to conditionally enable
  // the assignment and comparison operator tests only when the tested 
  // type supports these methods, but that's a lot of work for very little
  // gain -- the BTKContainer and BTKSequence code gets tested when the
  // AtomicStructure class hierarchy gets tested anyway.
 
  //test_btk_sequence_type<vector_seq,gen_seq>();
  //test_btk_sequence_type<list_seq,gen_seq>();

  return boost::report_errors();
}



