// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004-2006, Christopher Saunders <ctsa@users.sourceforge.net>,
//                         Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <iostream>
#include <list>

#include <btk/core/atoms/atom.hpp>
#include <btk/core/concepts/chain_concept.hpp>
#include <btk/core/concepts/atomic_structure_concept.hpp>
#include <btk/core/molecules/molecule.hpp>

#include "atomic_structure_tests.hpp"

using BTK::ATOMS::Atom;
using BTK::MOLECULES::Molecule;
using BTK::CONCEPTS::ChainConcept;
using BTK::CONCEPTS::AtomicStructureConcept;

typedef Molecule<Atom<> > molecule;
typedef molecule::atom_type atom_type;

int main() {
  boost::function_requires<ChainConcept<molecule> >();
  boost::function_requires<AtomicStructureConcept<molecule> >();

  // test Molecule-specific constructors
  molecule m(10,atom_type(),-1,'X');
  molecule m2(m.structure_begin(),m.structure_end(),-1,'Y');
  BOOST_TEST(m.chain_id() == 'X');
  BOOST_TEST(m2.chain_id() == 'Y');
  BOOST_TEST(m.num_atoms() == m2.num_atoms());
  BOOST_TEST(m != m2);

  // test get/set chain
  m2.set_chain_id('X');
  BOOST_TEST(m.chain_id() == 'X');
  BOOST_TEST(m == m2);
  
  // test the rest of the required methods
  test_mutable_atomic_structure_type<molecule>();
  
  return boost::report_errors();
}
