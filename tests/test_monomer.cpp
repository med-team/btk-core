// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <vector>

#include <boost/detail/lightweight_test.hpp>

#include <btk/core/concepts/monomer_concept.hpp>
#include <btk/core/molecules/monomer.hpp>
#include <btk/core/atoms/atom.hpp>

#include "atomic_structure_tests.hpp"

using BTK::MOLECULES::Monomer;
using BTK::ATOMS::Atom;

typedef Monomer<Atom<> > monomer;

int main() {
  boost::function_requires<BTK::CONCEPTS::MonomerConcept<monomer> >();
  
  std::vector<Atom<> > atoms(111);
  std::generate(atoms.begin(),atoms.end(),RandomAtomGenerator());

  // test Monomer-specific constructors
  monomer::id_type t = 128;
  int number = 314;

  monomer m(atoms.begin(),atoms.end(),t,number);
  BOOST_TEST(std::equal(atoms.begin(),atoms.end(),
                        m.structure_begin()));
  BOOST_TEST(m.number() == number);

  monomer m2(m);
  BOOST_TEST(m.type() == t);
  BOOST_TEST(m.number() == number);
             
  // test all methods required by the AtomicStructure concept.
  test_atomic_structure(m2);
  
  return boost::report_errors();
}

