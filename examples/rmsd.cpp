// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2001-2006, Christopher Saunders <ctsa@users.sourceforge.net>,
//                         Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <cstddef>
#include <fstream>
#include <functional>
#include <iostream>
#include <string>

#include "boost/bind.hpp"
#include "boost/iterator/filter_iterator.hpp"
#include "boost/lexical_cast.hpp"

#include <btk/core/atoms/pdb_atom.hpp>
#include <btk/core/molecules/monomer.hpp>
#include <btk/core/molecules/polymer.hpp>
#include <btk/core/io/pdb_system.hpp>
#include <btk/core/molecules/polymer.hpp>
#include <btk/core/algorithms/selections.hpp>
#include <btk/core/algorithms/transforms.hpp>
#include <btk/core/algorithms/predicates.hpp>
#include <btk/core/algorithms/rmsd.hpp>

using namespace std;

using BTK::ALGORITHMS::select;
using BTK::ALGORITHMS::select_if;
using BTK::ALGORITHMS::is_selected;
using BTK::ALGORITHMS::is_named;
using BTK::ALGORITHMS::get_rmsd;
using BTK::ALGORITHMS::leastsquares_superposition;
using BTK::ALGORITHMS::transform;

using boost::make_filter_iterator;

typedef BTK::ATOMS::PDBAtom atom;
typedef BTK::MOLECULES::Monomer<atom> monomer;
typedef BTK::MOLECULES::Polymer<monomer> polymer;
typedef BTK::IO::PDBSystem<polymer> pdb_system;

void
usage_error(const char* progname)
{
  cerr << "usage: " << progname << " -pdb1 pdb_file1 -pdb2 pdb_file2 [options]\n"
       << "\n"
       << "Calculates the optimal superposition of the structure in pdb2 to the structure\n"
       << "in pdb1.  Reports the root mean square deviation between corresponding atoms\n"
       << "in the two structures.  By default, all non-heterogen atoms in the structures\n"
       << "are aligned, unless a selection is specified.\n"
       << "\n"
       << "\n"
       << "  Selection options:\n"
       << "    -pdb1_chain <chain id> - specify a chain to use in pdb_file1\n"
       << "    -pdb2_chain <chain id> - specify a chain to use in pdb_file2\n"
       << "    -bb              - select protein backbone atoms\n"
       << "    -calpha          - select C-alpha atoms\n"
       << "    -start <N>       - select atoms with residue number higher than N.\n"
       << "    -stop  <N>       - select atoms with residue number lower than N.\n"
       << "    -hetatoms        - include hetatoms in alignment (default: exclude hetatoms)\n"
       << "\n"
       << "    (the intersection of multiple selection options is used.)\n"
       << "\n"
       << "  Output options:\n"
       << "    -pdb2_out <file>       - output a copy of pdb2, in its optimal superposition to pdb1\n"
       << "    -pdb2_trans_out <file> - write to file the rotation R and translation T that superimpose\n"
       << "                             the selected atoms of pdb2 to those of pdb1\n"
       << endl
       << endl;

  exit(EXIT_FAILURE);
}

struct is_protein_bb
{
  bool operator()(atom const & a) const {
    if (a.name() == "N" || a.name() == "CA" ||
        a.name() == "C" || a.name() == "O") return true;
    return false;
  }
};

struct res_number_gt
{
  res_number_gt(int num) : _num(num) {}

  bool operator()(atom const & a) const {
    return (a.res_number() > _num);
  }

  int _num;
};

struct res_number_lt
{
  res_number_lt(int num) : _num(num) {}

  bool operator()(atom const & a) const {
    return (a.res_number() < _num);
  }

  int _num;
};

template <typename Container,
          typename UnaryPredicate>
void
inverse_selection(Container & c1,
                  Container & c2,
                  UnaryPredicate f)
{
  // do the inverse of a normal selection with the given predicate.
  // in other words, if f is false, set the selection to false.

  select_if(c1.structure_begin(),c1.structure_end(),
            boost::bind<bool>(logical_not<bool>(),
                              boost::bind<bool>(f,_1)),
            false);

  select_if(c2.structure_begin(),c2.structure_end(),
            boost::bind<bool>(logical_not<bool>(),
                              boost::bind<bool>(f,_1)),
            false);
}

int main(int argc, char *argv[]) {

  string file1,file2;
  string pdb2_outfile,pdb2_trans_outfile;

  string chain1_list;
  string chain2_list;

  bool load_hetatoms = false;
  bool is_bb_filter = false;
  bool is_calpha_filter = false;
  bool is_start_filter = false;
  bool is_stop_filter = false;

  bool write_super_pdb = false;
  bool write_super_pdb_trans = false;

  int start_group = 0,stop_group = 0;

  for (int i=1;i<argc;++i) {
    string argstr(argv[i]);
    if (argstr=="-pdb1") {
      if(++i>=argc) usage_error(argv[0]);
      file1 = argv[i];

    } else if(argstr=="-pdb2"){
      if(++i>=argc) usage_error(argv[0]);
      file2 = argv[i];

    } else if (argstr=="-pdb1_chain") {
      if(++i>=argc) usage_error(argv[0]);
      chain1_list.push_back(argv[i][0]);

    } else if (argstr=="-pdb2_chain") {
      if(++i>=argc) usage_error(argv[0]);
      chain2_list.push_back(argv[i][0]);

    } else if (argstr=="-bb") {
      is_bb_filter = true;

    } else if (argstr=="-calpha") {
      is_calpha_filter = true;

    } else if (argstr=="-start") {
      if(++i>=argc) usage_error(argv[0]);
      start_group = boost::lexical_cast<int>(argv[i]);
      is_start_filter = true;

    } else if (argstr=="-stop") {
      if(++i>=argc) usage_error(argv[0]);
      stop_group = boost::lexical_cast<int>(argv[i]);
      is_stop_filter = true;

    } else if (argstr=="-pdb2_out") {
      if(++i>=argc) usage_error(argv[0]);
      write_super_pdb = true;
      pdb2_outfile = argv[i];

    } else if (argstr=="-pdb2_trans_out") {
      if(++i>=argc) usage_error(argv[0]);
      write_super_pdb_trans = true;
      pdb2_trans_outfile = argv[i];

    } else if (argstr == "-hetatoms") {
      load_hetatoms = true;
    } else {
      usage_error(argv[0]);
    }
  }

  if ( file1 == "" || file2 == "") usage_error(argv[0]);

  pdb_system pdb1(file1,load_hetatoms);
  pdb_system pdb2(file2,load_hetatoms);

  // The selection logic is slightly wonky:
  //
  // Start by *selecting* everything, then deselect those atoms that
  // don't meet the selection criteria.  This way, we get the default
  // behaviour (align everything) with minimal effort.
  select(pdb1.structure_begin(),pdb1.structure_end());
  select(pdb2.structure_begin(),pdb2.structure_end());

  // if chains were specified for pdb1, deselect all other chains
  if (chain1_list.size() > 0) {
    pdb_system::const_chain_iterator ci = pdb1.system_begin();

    while (ci != pdb1.system_end()) {
      if (chain1_list.find(ci->chain_id()) == string::npos) {
        select(ci->structure_begin(),
               ci->structure_end(),
               false);
      }
      ++ci;
    }
  }

  // same thing for pdb2
  if (chain2_list.size() > 0) {
    pdb_system::const_chain_iterator ci = pdb2.system_begin();

    while (ci != pdb2.system_end()) {
      if (chain2_list.find(ci->chain_id()) == string::npos) {
        select(ci->structure_begin(),
               ci->structure_end(),
               false);
      }
      ++ci;
    }
  }

  // deselect those atoms that don't meet the requested filters.
  // (the _inverse_ of a normal selection)
  if(is_bb_filter)
    inverse_selection(pdb1,pdb2,is_protein_bb());
  if(is_calpha_filter)
    inverse_selection(pdb1,pdb2,is_named("CA"));
  if(is_start_filter)
    inverse_selection(pdb1,pdb2,res_number_gt(start_group-1));
  if(is_stop_filter)
    inverse_selection(pdb1,pdb2,res_number_lt(stop_group+1));

  double rmsd;

  if (write_super_pdb || write_super_pdb_trans) {
    BTK::MATH::BTKVector T;
    BTK::MATH::BTKMatrix R;

    rmsd =
      leastsquares_superposition(make_filter_iterator<is_selected>(pdb1.structure_begin(),
                                                                   pdb1.structure_end()),
                                 make_filter_iterator<is_selected>(pdb1.structure_end(),
                                                                   pdb1.structure_end()),
                                 make_filter_iterator<is_selected>(pdb2.structure_begin(),
                                                                   pdb2.structure_end()),
                                 make_filter_iterator<is_selected>(pdb2.structure_end(),
                                                                   pdb2.structure_end()),
                                 R,
                                 T);

    if (write_super_pdb_trans) {
      ofstream transout(pdb2_trans_outfile.c_str());
      transout << R << T;
    }

    if (write_super_pdb) {
      transform(pdb2.structure_begin(),pdb2.structure_end(),R,T);
      ofstream pdbout(pdb2_outfile.c_str());
      pdbout << pdb2;
    }
  } else {
    rmsd =
      get_rmsd(make_filter_iterator<is_selected>(pdb1.structure_begin(),
                                                 pdb1.structure_end()),
               make_filter_iterator<is_selected>(pdb1.structure_end(),
                                                 pdb1.structure_end()),
               make_filter_iterator<is_selected>(pdb2.structure_begin(),
                                                 pdb2.structure_end()),
               make_filter_iterator<is_selected>(pdb2.structure_end(),
                                                 pdb2.structure_end()));
  }

  cout << "rmsd: " << rmsd << endl;
}
