// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <iostream>
#include <string>
#include <cmath>

#include <btk/core/math/vector_math.hpp>
#include <btk/core/atoms/pdb_atom.hpp>
#include <btk/core/molecules/monomer.hpp>
#include <btk/core/molecules/polymer.hpp>
#include <btk/core/io/pdb_system.hpp>
#include <btk/core/algorithms/properties.hpp>

typedef BTK::ATOMS::PDBAtom atom;
typedef BTK::MOLECULES::Monomer<atom> monomer;
typedef BTK::MOLECULES::Polymer<monomer> polymer;
typedef BTK::IO::PDBSystem<polymer> pdb_system;

int main(int argc, char * argv[])
{
  std::string filename;

  if (argc == 2) {
    filename = argv[1];
  } else {
    std::cerr << "usage: " << argv[0]
	      << " <pdb filename>"
	      << std::endl;
    return -1;
  }
  
  // load the PDB file
  pdb_system pdb(filename);

  // calculate the geometric center of all atoms in the PDB file
  BTK::MATH::BTKVector gc = 
    BTK::ALGORITHMS::geometric_center(pdb.structure_begin(),
				      pdb.structure_end());

  // iterate through every atom in the PDB file, and calculate its
  // distance from the geometric center of the molecule
  pdb_system::atom_iterator ai;
  double sum_rg_squared = 0.0;
  unsigned N = 0;

  for (ai = pdb.structure_begin(); ai != pdb.structure_end(); ++ai) {
    double r = BTK::MATH::length(ai->position() - gc); // r == distance
    sum_rg_squared += r * r;  // keep a running sum of the distance squared
    ++N; // count the number of atoms
  }
  
  std::cout << "radius of gyration: " 
	    << std::sqrt(sum_rg_squared / N) << std::endl;
  
  return 0;
}
