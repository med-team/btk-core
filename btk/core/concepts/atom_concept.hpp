// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file atom_concept.hpp
//! Specification of the Atom Concept.

#ifndef BTK_CONCEPTS_ATOM_CONCEPT_HPP
#define BTK_CONCEPTS_ATOM_CONCEPT_HPP

//! \page btk_core_concepts BTK Core Concepts
//! Documented Concepts:
//! \subpage atom_concept\n
//! \subpage atom_iterable_concept\n
//! \subpage atom_iterator_concept\n
//! \subpage atomic_structure_concept\n
//! \subpage mutable_atomic_structure_concept\n
//! \subpage strictly_chemically_typed_concept\n
//! \subpage chemically_typed_concept\n
//!
//! To be documented:\n
//!
//!  btk_container_concept\n
//!  chain_concept\n
//!  chain_iterable_concept\n
//!  chain_iterator_concept\n
//!  monomer_concept\n
//!  monomer_iterable_concept\n
//!  monomer_iterator_concept\n
//!  polymer_structure_concept\n
//!  system_concept\n
//!  type_id_concept\n
//!  type_system_concept\n

//! \page atom_concept The BTK Atom Concept
//!
//! \section description Description:
//!
//! An object that models the Atom Concept contains data and
//! paramters that are essential for the representation of atoms in
//! molecular modelling software.
//!
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!   DefaultConstructible,
//!   CopyConstructible, 
//!   EqualityConstructible,
//!   Assignable
//! \subsection btkconcepts BTK Concepts:
//!   \ref chemically_typed_concept ChemicallyTypedConcept
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the Atom Concept<br>
//! \c x -- An object of type \c X<br>
//!
//! \section types Associated Types:
//! <table>
//! <tr>
//!  <td>Atom ID Type</td>
//!  <td>\code X::atom_id_type \endcode \endcode </td>
//!  <td>A type conforming to the TypeID Concept</td>
//! </tr>
//! <tr>
//!  <td>Element ID Type</td>
//!  <td>\code X::element_id_type \endcode \endcode </td>
//!  <td>A type conforming to the TypeID Concept</td>
//! </tr>
//! </table>
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//! 
//! In addition to those expressions specified by the 
//! \ref refines "refined concepts", the following expressions 
//! must be valid:
//!
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Requirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td>\code X(BTKVector const & v, 
//! atom_id_type a,
//! element_id_type e, 
//! int n,
//! type_sysetem t) \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Creates an object of type X.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code element_id_type x.element_type() \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns the element type of the atom.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code x.set_element_type(element_id_type e) \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Sets the element type id.</td>
//! <td>x.element_type() will return the value e</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code BTKVector x.position() \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns the position of the atom.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code x.set_position(BTKVector const & v) \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Sets the position of the atom.</td>
//! <td>x.position() will return the value v</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code int x.number() \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns the position of the atom.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code x.set_number(int n) \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Sets the number of the atom.</td>
//! <td>x.number() will return the value n</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code x.select(b) \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Sets the selection flag to the value of b.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code bool x.selected() \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns true iff the atom is selected.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//! <tr>
//!
//! <tr>
//! <td>\code std::ostream & x.print(std::ostream & os) \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Writes a text representation of x to an output stream.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code std::ostream & x.print(std::ostream & os,
//! int atom_num,
//! int group_num,
//! char chain_id,
//! std::string const & group_name) \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Writes a text representation of x to an output stream 
//! (\ref n1 "deprecated")</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! </table>
//!
//! \section invariants Invariants:
//! None.
//! \section models Models:
//! - BTK::ATOMS::Atom 
//! - BTK::ATOMS::PDBAtom
//!
//! \section notes Notes:
//! - \anchor n1 The second form of the print() method is deprecated.  In future 
//!   versions of the BTK, there will be no way to pass atom and group
//!   number information or chain ID information to the print methods of
//!   individual atoms.
//!

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <string>
#include <iostream>

#include <boost/concept_check.hpp>

#include <btk/core/concepts/chemically_typed_concept.hpp>
#include <btk/core/math/btk_vector.hpp>

namespace BTK {
namespace CONCEPTS {

template <class T>
struct AtomConcept
{
  typedef typename T::atom_id_type at;
  typedef typename T::element_id_type et;
  typedef typename T::chemical_type_system cts_t;

  void constraints() {
    boost::function_requires<boost::DefaultConstructibleConcept<T> >();
    boost::function_requires<boost::CopyConstructibleConcept<T> >();
    boost::function_requires<boost::EqualityComparableConcept<T> >();
    boost::function_requires<boost::AssignableConcept<T> >();
    boost::function_requires<ChemicallyTypedConcept<T> >();

    // construct from position, type ids, number and dictionary
    T atom2(pos,atom_type,element_type,atom_num,cts);

    atom.set_element_type(element_type);
    atom.set_position(pos);
    atom.set_number(atom_num);

    const_constraints(atom);
  }

  void const_constraints(T const & atom) {
    element_type = atom.element_type();
    pos = atom.position();
    atom_num = atom.number();

    s = atom.selected();
    atom.select(s);

    std::cout << atom.print(std::cout);
    std::cout << atom.print(std::cout,
                            atom_num,
                            group_num,
                            chain_id,
                            group_name);
  }

  T atom;
  BTK::MATH::BTKVector pos;
  at atom_type;
  et element_type;
  bool s;
  int atom_num, group_num;
  std::string atom_name, group_name;
  char chain_id;
  cts_t cts;
};

} // namespace CONCEPTS
} // namespace BTK

#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BTK_CONCEPTS_ATOM_CONCEPT_HPP
