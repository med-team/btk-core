// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_MONOMER_ITERATOR_CONCEPT_HPP
#define BTK_CONCEPTS_MONOMER_ITERATOR_CONCEPT_HPP

#include <iterator>
#include <boost/concept_check.hpp>

#include <btk/core/concepts/monomer_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct MonomerIteratorConcept
{
  typedef typename std::iterator_traits<T>::value_type value_type;

  void constraints() {
    boost::function_requires<boost::BidirectionalIteratorConcept<T> >();
    boost::function_requires<MonomerConcept<value_type> >();
  }
};

template <typename T>
struct MutableMonomerIteratorConcept
{
  void constraints() {
    boost::function_requires<MonomerIteratorConcept<T> >();
    boost::function_requires<boost::Mutable_BidirectionalIteratorConcept<T> >();
  }
};

} // namespace CONCEPTS
} // namespace BTK

#endif
