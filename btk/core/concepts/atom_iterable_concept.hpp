// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file atom_iterable_concept.hpp
//! Specification of the AtomIterable Concept.

#ifndef BTK_CONCEPTS_ATOM_ITERABLE_CONCEPT_HPP
#define BTK_CONCEPTS_ATOM_ITERABLE_CONCEPT_HPP

//! \page atom_iterable_concept The BTK AtomIterable Concept
//!
//! \section description Description:
//!
//! An object that models the AtomIterable Concept allows iteration
//! over a range of Atom objects, and allows the user to query the size
//! of that range.
//!
//! This concept specifies only that an object should be able to <i>iterate</i>
//! over a range of atom objects, not necessarily that it should <i>contain</i>
//! those objects.  In particular, while all 
//! \ref atomic_structure_concept "AtomicStructure" objects
//! are AtomIterable, not all AtomIterable objects are AtomicStructures.
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the AtomIterable Concept<br>
//! \c x -- An object of type \c x</br>
//!
//! \section types Associated Types:
//! <table>
//! <tr>
//!  <td>Size Type</td>
//!  <td>\code X::size_type \endcode </td>
//!  <td>An unsigned integral type</td>
//! </tr>
//!
//! <tr>
//!  <td>Atom Iterator Type</td>
//!  <td>\code X::atom_iterator \endcode \endcode </td>
//!  <td>A type conforming to the \ref atom_iterator_concept "AtomIterator" 
//!      Concept</td>
//! </tr>
//!
//! <tr>
//!  <td>Const Atom Iterator Type</td>
//!  <td>\code X::const_atom_iterator \endcode \endcode </td>
//!  <td>A type conforming to the \ref atom_iterator_concept "AtomIterator" 
//!      Concept, that may only be used to examine (not modify) atoms.</td>
//! </tr>
//!
//! <tr>
//!  <td>Reverse Atom Iterator Type</td>
//!  <td>\code X::reverse_atom_iterator \endcode \endcode </td>
//!  <td>An STL ReverseIterator whose base iterator type is an atom_iterator</td>
//! </tr>
//!
//! <tr>
//!  <td>Const Reverse Atom Iterator Type</td>
//!  <td>\code X::const_reverse_atom_iterator \endcode \endcode </td>
//!  <td>An STL ReverseIterator whose base iterator type is a 
//!      const_atom_iterator</td>
//! </tr>
//!
//! </table>
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Reqirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td>\code size_type x.num_atoms() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns the size of the atom range.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code atom_iterator x.structure_begin() \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Returns an iterator pointing to the first atom in the range.</td>
//! <td>x.atom_begin() is either dereferenceable, or past-the-end.  It is
//!     past the end iff x.atom_size() == 0</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code atom_iterator x.structure_end() \endcode </td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Returns an iterator pointing one past the last atom in the range.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code const_atom_iterator x.structure_begin() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns an iterator pointing to the first atom in the range.</td>
//! <td>x.atom_begin() is either dereferenceable, or past-the-end.  It is
//!     past the end iff x.atom_size() == 0</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code const_atom_iterator x.structure_end() const \endcode </td>
//! <td></td>
//! <td></td>
//! <td>Returns an iterator pointing one past the last atom in the range.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code reverse_atom_iterator x.structure_rbegin() \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Equivalent to reverse_atom_iterator(x.structure_end())</td>
//! <td>x.atom_rbegin() is either dereferenceable, or past-the-end.  It is
//!     past the end iff x.atom_size() == 0</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code reverse_atom_iterator x.structure_rend() \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Equivalent to reverse_atom_iterator(x.structure_begin())</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code const_reverse_atom_iterator x.structure_rbegin() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Equivalent to const_reverse_atom_iterator(x.structure_end())</td>
//! <td>x.atom_rbegin() is either dereferenceable, or past-the-end.  It is
//!     past the end iff x.atom_size() == 0</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code const_reverse_atom_iterator x.structure_rend() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Equivalent to const_reverse_atom_iterator(x.structure_begin())</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! </table>
//!
//! \section invariants Invariants:
//! - [x.atom_begin(),x.atom_end()) is a valid range.
//! - [x.atom_rbegin(),x.atom_rend()) is a valid range.
//! - The distance from x.atom_begin() to x.atom_end() is the same as the 
//!   the distance from x.atom_rbegin() to x.atom_rend(), which is the same
//!   as x.atom_size().
//!
//! \section models Models:
//! - BTK::MOLECULES::AtomicStructure
//! - BTK::MOLECULES::Chain
//! - BTK::MOLECULES::Molecule
//! - BTK::MOLECULES::Polymer
//! - Any BTK::MOLECULES::System of AtomIterable objects.
//!
//! \section notes Notes:
//! - Just as for STL containers, there is no complexity requirement for 
//!   x.num_atoms().  Use with care.
//!

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <boost/concept_check.hpp>

#include <btk/core/concepts/atom_iterator_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct AtomIterableConcept
{
  typedef typename T::size_type st;
  typedef typename T::const_atom_iterator const_atom_it;
  typedef typename T::const_reverse_atom_iterator const_rev_atom_it;
  typedef typename T::atom_iterator atom_it;
  typedef typename T::reverse_atom_iterator rev_atom_it;

  void constraints() {
    boost::function_requires<AtomIterableConcept<T> >();
    boost::function_requires<MutableAtomIteratorConcept<atom_it> >();
    boost::function_requires<MutableAtomIteratorConcept<rev_atom_it> >();

    ai = obj.structure_begin();
    ai = obj.structure_end();
    rai = obj.structure_rbegin();
    rai = obj.structure_rend();

    const_constraints(obj);
  };

  void const_constraints(T const & ac) {
    boost::function_requires<AtomIteratorConcept<const_atom_it> >();
    boost::function_requires<AtomIteratorConcept<const_rev_atom_it> >();

    size = ac.num_atoms();

    cai = ac.structure_begin();
    cai = ac.structure_end();
    crai = ac.structure_rbegin();
    crai = ac.structure_rend();  
  }

  T obj;
  st size;
  const_atom_it cai;
  const_rev_atom_it crai;
  atom_it ai;
  rev_atom_it rai;  
};

} // namespace CONCEPTS
} // namespace BTK
#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BTK_CONCEPTS_ATOM_ITERABLE_CONCEPT_HPP
