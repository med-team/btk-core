// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_MONOMER_ITERABLE_CONCEPT_HPP
#define BTK_CONCEPTS_MONOMER_ITERABLE_CONCEPT_HPP

#include <boost/concept_check.hpp>

#include <btk/core/concepts/monomer_iterator_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct MonomerIterableConcept
{
  typedef typename T::size_type st;
  typedef typename T::const_monomer_iterator const_monomer_it;
  typedef typename T::const_reverse_monomer_iterator const_rev_monomer_it;

  void constraints()
  {
    boost::function_requires<MonomerIteratorConcept<const_monomer_it> >();
    boost::function_requires<MonomerIteratorConcept<const_rev_monomer_it> >();

    const_constraints(obj);
  }

  void const_constraints(T const & mc)
  {
    size = mc.num_monomers();
    
    cmi = mc.polymer_begin();
    cmi = mc.polymer_end();

    crmi = mc.polymer_rbegin();
    crmi = mc.polymer_rend();
  }

  T obj;
  st size;
  const_monomer_it cmi;
  const_rev_monomer_it crmi;
};

template <typename T>
struct MutableMonomerIterableConcept
{
  typedef typename T::monomer_iterator monomer_it;
  typedef typename T::reverse_monomer_iterator rev_monomer_it;

  void constraints()
  {
    boost::function_requires<MonomerIterableConcept<T> >();
    boost::function_requires<MutableMonomerIteratorConcept<monomer_it> >();
    boost::function_requires<MutableMonomerIteratorConcept<rev_monomer_it> >();

    mi = mc.polymer_begin();
    mi = mc.polymer_end();

    rmi = mc.polymer_rbegin();
    rmi = mc.polymer_rend();
  }

  T mc;
  monomer_it mi;
  rev_monomer_it rmi;
};

} // namespace CONCEPTS
} // namespace BTK

#endif
