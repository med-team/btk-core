// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file chemically_typed_concept.hpp
//! Specification of the ChemicallyTyped and StrictlyChemicallyTyped concepts.

#ifndef BTK_CONCEPTS_CHEMICALLY_TYPED_CONCEPT_HPP
#define BTK_CONCEPTS_CHEMICALLY_TYPED_CONCEPT_HPP

//! \page strictly_chemically_typed_concept The BTK StrictlyChemicallyTyped Concept
//!
//! \section description Description:
//!
//! A type that models StrictlyChemicallyTyped has an associated "chemical
//! type" identifier (e.g. an atom or residue type), that is part of a 
//! self-consistent "chemical type system" (e.g. a set of atom and residue 
//! types that are specific to a particular type of molecule).  The concept
//! is "strict," because it does not allow the modification of either of these
//! associated objects.  This is essential when dealing with complex molecule
//! types for which a change in chemical type following construction is difficult
//! or undesirable.  
//! 
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!   None.
//! \subsection btkconcepts BTK Concepts:
//!   None.
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the StrictlyChemicallyTyped Concept<br>
//! \c x -- An object of type \c X<br>
//!
//! \section types Associated Types:
//! <table>
//! <tr>
//!  <td>Chemical Type System Type</td>
//!  <td>\code X::chemical_type_system \endcode </td>
//!  <td>A type modelling the \ref type_system_concept "TypeSystem Concept"</td>
//! </tr>
//! 
//! <tr>
//!  <td>Dictionary Type</td>
//!  <td>\code X::dictionary \endcode </td>
//!  <td>A type modelling the \ref dictionary_concept "Dictionary Concept"</td>
//! </tr>
//! 
//! <tr>
//!  <td>Chemical ID Type</td>
//!  <td>\code X::id_type \endcode </td>
//!  <td>A type modelling the \ref type_id_concept "TypeID Concept"</td>
//! </tr>
//! </table>
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//! 
//! In addition to those expressions specified by the 
//! \ref refines "refined concepts", the following expressions 
//! must be valid:
//!
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Requirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td>\code X::id_type x.type() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns the chemical type ID of x.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td>\code std::string x.name() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>If x.type() returns a defined chemical type, x.name() returns a
//!     non-empty string.  Otherwise, x.name() returns an empty string.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code X::chemical_type_system const &
//! x.get_chemical_type_system() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns a const reference to the chemical type system associated with x.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//! 
//! <tr>
//! <td>\code X::dictionary const & x.get_dictionary() const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Returns a const reference to the dictionary_type associated with x.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//! </table>
//!
//! \section models Models:
//! - BTK::ATOMS::Atom
//! - BTK::MOLECULES::AtomicStructure
//! - BTK::MOLECULES::Chain
//! - BTK::MOLECULES::Molecule
//! - BTK::MOLECULES::Monomer
//! - BTK::MOLECULES::PolymerStructure
//! - BTK::MOLECULES::Polymer
//!
//! \section invariants Invariants:
//! - X::dictionary is one of dictionary types defined in X::chemical_type_system. 
//! - x.name() == (x.get_dictionary().find(x.type())).second
//! - x.type() == (x.get_dictionary().find(x.name())).first
//! 
//! \section notes Notes:
//! - For situations where a modifiable chemical type is desired, see the
//!   \ref chemically_typed_concept "ChemicallyTyped Concept".

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <string>
#include <boost/concept_check.hpp>
#include <btk/core/concepts/type_system_concept.hpp>
#include <btk/core/concepts/type_id_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct StrictlyChemicallyTypedConcept
{
  typedef typename T::chemical_type_system ts_t;
  typedef typename T::dictionary dict_t;
  typedef typename T::id_type id_t;
  
  void constraints() {
    boost::function_requires<TypeSystemConcept<ts_t> >();
    boost::function_requires<TypeIDConcept<id_t> >();

    const_constraints(obj);
  }

  void const_constraints(T const & t) {
    id = obj.type();
    name = obj.name();
    ts_t const & ctsr = obj.get_chemical_type_system();
    dict_t const & dr = obj.get_dictionary();

    boost::ignore_unused_variable_warning(ctsr);
    boost::ignore_unused_variable_warning(dr);
  }

  T obj;
  id_t id;
  std::string name;
};

} // namespace CONCEPTS
} // namespace BTK

#endif // DOXYGEN_SHOULD_SKIP_THIS

//! \page chemically_typed_concept The BTK ChemicallyTyped Concept
//!
//! \section description Description:
//!
//! A type that models ChemicallyTyped has an associated "chemical
//! type" identifier (e.g. an atom or residue type), that is part of a 
//! self-consistent "chemical type system" (e.g. a set of atom and residue 
//! types that are specific to a particular type of molecule).
//!
//! ChemicallyTyped is a refinement of StrictlyChemicallyTyped, in that it
//! requires everything that the latter concept defines, with the addition of 
//! methods to set the chemical type and type system associated with an object.
//! 
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!   None.
//! \subsection btkconcepts BTK Concepts:
//!   \ref strictly_chemically_typed_concept "StrictlyChemicallyTyped"
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the ChemicallyTyped Concept<br>
//! \c x -- An object of type \c X<br>
//!
//! \section types Associated Types:
//! None beyond those of 
//! \ref strictly_chemically_typed_concept "StrictlyChemicallyTyped".
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//! 
//! In addition to those expressions specified by  
//! \ref strictly_chemically_typed_concept "StrictlyChemicallyTyped", the 
//! following expressions must be valid:
//!
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Requirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td> \code void x.set_type(X::type_id t) \endcode </td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>The type_id of x will be set to the value t.</td>
//! <td>x.type() will return the value t</td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td> \code X::chemical_type_system & 
//! x.get_chemical_type_system() \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>A reference to the chemical_type_system associated with x is returned.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//!
//! <tr>
//! <td> \code void x.set_chemical_type_system(X::chemical_type_system const & ts) \endcode</td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>The type_system associated with x will be set to a copy of ts.</td>
//! <td>ts will be copied. x.clone_type_system() will return a copy of ts.</td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td> \code X::dictionary & x.get_dictionary() \endcode </td>
//! <td>x is mutable</td>
//! <td></td>
//! <td>Returns a reference to the dictionary object associated with x.</td>
//! <td></td>
//! <td>O(1)</td>
//! </tr>
//! </table>
//!
//! \section models Models:
//! - BTK::ATOMS::Atom
//! - BTK::MOLECULES::Chain
//! - BTK::MOLECULES::Molecule
//! - BTK::MOLECULES::Polymer
//!
//! \section invariants Invariants:
//! None.
//!
//! \section notes Notes:
//! None.

#ifndef DOXYGEN_SHOULD_SKIP_THIS

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct ChemicallyTypedConcept
{
  typedef typename T::chemical_type_system ts_t;
  typedef typename T::dictionary dict_t;
  typedef typename T::id_type id_t;

  void constraints() {
    boost::function_requires<StrictlyChemicallyTypedConcept<T> >();
 
    dict_t & d = obj.get_dictionary();
    ts_t & t = obj.get_chemical_type_system();

    obj.set_type(id);
    obj.set_chemical_type_system(cts);

    boost::ignore_unused_variable_warning(d);
    boost::ignore_unused_variable_warning(t);
  }

  T obj;
  id_t id;
  ts_t cts;
};

} // namespace CONCEPTS
} // namespace BTK

#endif // DOXYGEN_SHOULD_SKIP_THIS

#endif // BTK_CONCEPTS_CHEMICALLY_TYPED_CONCEPT_HPP 
