// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_TYPE_ID_CONCEPT_HPP
#define BTK_CONCEPTS_TYPE_ID_CONCEPT_HPP

#include <boost/concept_check.hpp>
#include <btk/core/utility/type_id_traits.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct TypeIDConcept
{
  typedef BTK::UTILITY::TypeIDTraits<T> id_traits;  

  void constraints() {
    boost::function_requires<boost::DefaultConstructibleConcept<T> >();
    boost::function_requires<boost::CopyConstructibleConcept<T> >();
    boost::function_requires<boost::AssignableConcept<T> >();
    boost::function_requires<boost::EqualityComparableConcept<T> >();
    boost::function_requires<boost::LessThanComparableConcept<T> >();
  }
};

} // namespace CONCEPTS
} // namespace BTK

#endif  
