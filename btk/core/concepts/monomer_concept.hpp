// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_MONOMER_CONCEPT_HPP
#define BTK_CONCEPTS_MONOMER_CONCEPT_HPP

#include <string>
#include <vector>

#include <boost/concept_check.hpp>

#include <btk/core/concepts/atomic_structure_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <class T>
struct MonomerConcept
{
  typedef typename T::atom_type at;
  typedef typename T::atom_iterator atom_it;
  typedef typename T::const_atom_iterator const_atom_it;
  typedef typename T::reverse_atom_iterator rev_atom_it;
  typedef typename T::const_reverse_atom_iterator const_rev_atom_it;
  typedef typename T::monomer_id_type monomer_id_type;

  void constraints() {
    boost::function_requires<AtomicStructureConcept<T> >();
    boost::function_requires<boost::DefaultConstructibleConcept<T> >();

    // constructors
    {
      // construct from arbitrary atom iterators
      T m1(first,last,type,num);

      // construct from atom iterators
      T m2(begin,end,type,num);
    }

    monomer.set_number(num);

    ai = monomer.monomer_begin();
    ai = monomer.monomer_end();
    rai = monomer.monomer_rbegin();
    rai = monomer.monomer_rend();

    const_constraints(monomer);
  }

  void const_constraints(T const & monomer) {
    num = monomer.number();

    cai = monomer.monomer_begin();
    cai = monomer.monomer_end();
    crai = monomer.monomer_rbegin();
    crai = monomer.monomer_rend();
  }

  T monomer;
  monomer_id_type type;
  typename std::vector<at>::iterator first,last;
  atom_it begin,end,ai;
  const_atom_it cai;
  rev_atom_it rai;
  const_rev_atom_it crai;
  int num;
};

} // namespace CONCEPTS
} // namespace BTK

#endif
