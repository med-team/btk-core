// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_CHAIN_ITERABLE_CONCEPT_HPP
#define BTK_CONCEPTS_CHAIN_ITERABLE_CONCEPT_HPP

#include <boost/concept_check.hpp>

#include <btk/core/concepts/chain_iterator_concept.hpp>

namespace BTK {
namespace CONCEPTS { 

template <typename T>
struct ChainIterableConcept
{
  typedef typename T::size_type st;
  typedef typename T::const_chain_iterator const_chain_it;
  typedef typename T::const_reverse_chain_iterator const_rev_chain_it;

  void constraints()
  {
    boost::function_requires<ChainIteratorConcept<const_chain_it> >();
    boost::function_requires<ChainIteratorConcept<const_rev_chain_it> >();
    
    const_constraints(obj);
  }

  void const_constraints(T const & c_obj)
  {
    size = c_obj.num_chains();
    
    cci = c_obj.system_begin();
    cci = c_obj.system_end();
    
    crci = c_obj.system_rbegin();
    crci = c_obj.system_rend();
  }

  T obj;
  st size;
  const_chain_it cci;
  const_rev_chain_it crci;
};

template <typename T>
struct MutableChainIterableConcept
{
  typedef typename T::chain_iterator chain_it;
  typedef typename T::reverse_chain_iterator rev_chain_it;

  void constraints() 
  {
    boost::function_requires<ChainIterableConcept<T> >();
    boost::function_requires<MutableChainIteratorConcept<chain_it> >();
    boost::function_requires<MutableChainIteratorConcept<rev_chain_it> >();

    ci = obj.system_begin();
    ci = obj.system_end();
    
    rci = obj.system_rbegin();
    rci = obj.system_rend();
  }

  T obj;
  chain_it ci;
  rev_chain_it rci;
};

} // CONCEPTS
} // BTK

#endif
