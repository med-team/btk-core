// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_TYPE_SYSTEM_CONCEPT_HPP
#define BTK_CONCEPTS_TYPE_SYSTEM_CONCEPT_HPP

#include <boost/concept_check.hpp>

namespace BTK {
namespace CONCEPTS {

template <class T>
struct TypeSystemConcept
{
  typedef typename T::element_dictionary edt;
  typedef typename T::atom_dictionary adt;
  typedef typename T::monomer_dictionary mdt;
  typedef typename T::structure_dictionary sdt;
  
  typedef typename T::element_id_type eid;
  typedef typename T::atom_id_type aid;
  typedef typename T::monomer_id_type mid;
  typedef typename T::structure_id_type sid;

  // a way to avoid annoying "unused variable" warnings.
  template<class U> inline void unused_variable(const U&) {}

  void constraints() {
    boost::function_requires<boost::DefaultConstructibleConcept<T> >();
    boost::function_requires<boost::CopyConstructibleConcept<T> >();
    boost::function_requires<boost::AssignableConcept<T> >();
    
    edt & e = type_system.get_element_dictionary();
    adt & a = type_system.get_atom_dictionary();
    mdt & m = type_system.get_monomer_dictionary();
    sdt & s = type_system.get_structure_dictionary();

    unused_variable(e);
    unused_variable(a);
    unused_variable(m);
    unused_variable(s);

    const_constraints(type_system);
  }

  void const_constraints(T const & ts) {
    
    edt const & e = ts.get_element_dictionary();
    adt const & a = ts.get_atom_dictionary();
    mdt const & m = ts.get_monomer_dictionary();
    sdt const & s = ts.get_structure_dictionary();

    unused_variable(e);
    unused_variable(a);
    unused_variable(m);
    unused_variable(s);
  }

  T type_system;
};

} // namespace CONCEPTS
} // namespace BTK

#endif 
