// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file atomic_structure_concept.hpp
//! Specification of the AtomicStructure Concept.

#ifndef BTK_CONCEPTS_ATOMIC_STRUCTURE_CONCEPT_HPP
#define BTK_CONCEPTS_ATOMIC_STRUCTURE_CONCEPT_HPP

//! \page atomic_structure_concept The BTK AtomicStructure Concept
//!
//! \section description Description:
//!
//! An object that models an immutable atomic structure (i.e. a structure that
//! does not support the insertion or deletion of atoms).
//! 
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!  - ReversibleContainer
//! \subsection btkconcepts BTK Concepts:
//!  - \ref atom_iterable_concept "AtomIterable"
//!  - \ref strictly_chemically_typed_concept "StrictlyChemicallyTyped"
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the AtomicStructure Concept<br>
//! \c x -- An object of type \c X<br>
//!
//! \section types Associated Types:
//! In addition to the types required by the \ref refines "refined concepts":
//! <table>
//! <tr>
//!  <td>Atom Type</td>
//!  <td>\code X::atom_type \endcode </td>
//!  <td>A type that models the \ref atom_concept "Atom Concept"</td>
//! </tr>
//! </table>
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//! 
//! In addition to those expressions specified by the 
//! \ref refines "refined concepts", the following expressions 
//! must be valid:
//!
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Requirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td>\code X::const_reference x.operator[](X::size_type n) const \endcode</td>
//! <td></td>
//! <td>n is in the range [0,x.size())</td>
//! <td>Gets a constant reference to the n'th atom in the structure.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code std::ostream & x.print(std::ostream & s) const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Writes the contents of x to stream s.</td>
//! <td></td>
//! <td></td>
//! </tr>
//!
//! <tr>
//! <td>\code std::ostream & x.print(std::ostream & s, size_type n) const \endcode</td>
//! <td></td>
//! <td></td>
//! <td>Writes the contents of x to stream, using n as the first atom number.</td>
//! <td></td>
//! <td></td>
//! </tr>
//! </table>
//!
//! \section models Models:
//! - BTK::MOLECULES::AtomicStructure
//! - BTK::MOLECULES::Chain
//! - BTK::MOLECULES::Molecule
//! - BTK::MOLECULES::Monomer

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <iostream>
#include <boost/concept_check.hpp>

#include <btk/core/concepts/atom_concept.hpp>
#include <btk/core/concepts/atom_iterable_concept.hpp>
#include <btk/core/concepts/chemically_typed_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <class T>
struct AtomicStructureConcept
{
  typedef typename T::atom_type at;
  typedef typename T::size_type st;

  void constraints() {
    boost::function_requires<boost::Mutable_ReversibleContainerConcept<T> >();
    boost::function_requires<StrictlyChemicallyTypedConcept<T> >();
    boost::function_requires<AtomIterableConcept<T> >();
    boost::function_requires<AtomConcept<at> >();

    const_constraints(obj);
  }

  void const_constraints(T const & c_obj) {

    at const & atom_ref = c_obj[atom_num];
    boost::ignore_unused_variable_warning(atom_ref);

    std::cout << c_obj.print(std::cout);
    std::cout << c_obj.print(std::cout,atom_num);
  }

  T obj;
  st atom_num;
};

#endif // DOXYGEN_SHOULD_SKIP_THIS

//! \page mutable_atomic_structure_concept The BTK MutableAtomicStructure Concept
//!
//! \section description Description:
//!
//! An object that models a mutable atomic structure (i.e. a structure that
//! supports the insertion or deletion of atoms or changes in type).
//! 
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!  - Sequence
//! \subsection btkconcepts BTK Concepts:
//!  - \ref atomic_structure_concept "AtomicStructure"
//!  - \ref chemically_typed_concept "ChemicallyTyped"
//!
//! \section notation Notation:
//! 
//! \c X -- A type that models the MutableAtomicStructure Concept<br>
//! \c x -- An object of type \c X<br>
//!
//! \section types Associated Types:
//! None beyond those of the \ref refines "refined concepts."
//!
//! \section definitions Definitions:
//! None.
//!
//! \section expressions Valid Expressions:
//!
//! In addition to those expressions specified by the 
//! \ref refines "refined concepts", the following expressions 
//! must be valid:
//!
//! <table>
//! <tr>
//! <td><b>Expression</b></td> 
//! <td><b>Type Requirements</b></td> 
//! <td><b>Precondition</b></td>
//! <td><b>Semantics</b></td>
//! <td><b>Postcondition</b></td>
//! <td><b>Complexity</b></td>
//! </tr>
//!
//! <tr>
//! <td>\code X::reference x.operator[](X::size_type n) \endcode</td>
//! <td></td>
//! <td>n is in the range [0,x.size())</td>
//! <td>Gets a non-const reference to the n'th atom in the structure.</td>
//! <td></td>
//! <td></td>
//! </tr>
//! </table>
//!
//! \section models Models:
//! - BTK::MOLECULES::AtomicStructure
//! - BTK::MOLECULES::Molecule
  
#ifndef DOXYGEN_SHOULD_SKIP_THIS

template <typename T>
struct MutableAtomicStructureConcept
{
  typedef typename T::atom_type at;
  typedef typename T::size_type st;

  void constraints() {
    boost::function_requires<boost::SequenceConcept<T> >();
    boost::function_requires<ChemicallyTypedConcept<T> >();
    boost::function_requires<AtomicStructureConcept<T> >();

    at & atom_ref = obj[atom_num];
    boost::ignore_unused_variable_warning(atom_ref);
  }

  T obj;
  st atom_num;
};

} // namespace CONCEPTS
} // namespace BTK

#endif //DOXYGEN_SHOULD_SKIP_THIS

#endif 
