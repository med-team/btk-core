// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file atom_iterator_concept.hpp
//! Specification of the AtomIterator Concept.

#ifndef BTK_CONCEPTS_ATOM_ITERATOR_CONCEPT_HPP
#define BTK_CONCEPTS_ATOM_ITERATOR_CONCEPT_HPP

//! \page atom_iterator_concept The BTK AtomIterator Concept
//!
//! \section description Description:
//!
//! An AtomIterator is an STL BidirectionalIterator \ref n1 "(1)" whose 
//! value_type \ref n2 "(2)" is a model of the \ref atom_concept "Atom Concept".
//!
//! \section refines Concept Refines:
//! \subsection stlconcepts STL Concepts:
//!   BidirectionalIterator
//!
//! \section types Associated Types:
//! The same as for an STL BidirectionalIterator.
//!
//! \section expressions Valid Expressions:
//! The same as for an STL BidirectionalIterator.
//!
//! \section models Models:
//! - BTK::MOLECULES::AtomicStructure::atom_iterator
//! - BTK::MOLECULES::Chain::atom_iterator
//! - BTK::MOLECULES::Molecule::atom_iterator
//! - BTK::MOLECULES::Polymer::atom_iterator
//! - BTK::MOLECULES::System::atom_iterator (if defined)
//!
//! \section notes Notes:
//! - \anchor n1 The AtomIterator is defined as a BidirectionalIterator (as 
//!   opposed to a RandomAccessIterator) because the BTK supports the use of 
//!   containers that are not STL RandomAccessContainers for atom storage 
//!   (e.g. std::list). 
//! - \anchor n2 All AtomIterator objects are expected to work with the 
//!   std::iterator_traits<> template.

#ifndef DOXYGEN_SHOULD_SKIP_THIS

#include <iterator>
#include <boost/concept_check.hpp>

#include <btk/core/concepts/atom_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <class T>
struct AtomIteratorConcept
{
  typedef typename std::iterator_traits<T>::value_type value_type;

  void constraints() {
    boost::function_requires<boost::BidirectionalIteratorConcept<T> >();
    boost::function_requires<AtomConcept<value_type> >();
  }
};

template <class T>
struct MutableAtomIteratorConcept
{
  void constraints() {
    boost::function_requires<AtomIteratorConcept<T> >();
    boost::function_requires<boost::Mutable_BidirectionalIteratorConcept<T> >();
  }
};

} // namespace CONCEPTS
} // namespace BTK

#endif //DOXYGEN_SHOULD_SKIP_THIS

#endif // BTK_CONCEPTS_ATOM_ITERATOR_CONCEPT_HPP
