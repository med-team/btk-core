// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_SYSTEM_CONCEPT_HPP
#define BTK_CONCEPTS_SYSTEM_CONCEPT_HPP

#include <iostream>
#include <boost/concept_check.hpp>

#include <btk/core/concepts/chemically_typed_concept.hpp>
#include <btk/core/concepts/atom_iterable_concept.hpp>
#include <btk/core/concepts/chain_iterable_concept.hpp>
#include <btk/core/concepts/chain_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct SystemConcept
{
  typedef typename T::size_type size_type;
  typedef typename T::chain_type chain_type;
  typedef typename T::chain_iterator chain_iterator;
  typedef typename T::const_chain_iterator const_chain_iterator;
  typedef typename T::reverse_chain_iterator reverse_chain_iterator;
  typedef typename T::const_reverse_chain_iterator const_reverse_chain_iterator;

  void constraints() {
    boost::function_requires<boost::Mutable_ReversibleContainerConcept<T> >();
    boost::function_requires<StrictlyChemicallyTypedConcept<T> >();
    boost::function_requires<AtomIterableConcept<T> >();
    boost::function_requires<ChainIterableConcept<T> >();
    boost::function_requires<ChainConcept<chain_type> >();

    const_constraints(obj);
  }

  void const_constraints(T const & system) {
    std::cout << system.print(std::cout);
    std::cout << system.print(std::cout,atom_num);
  }
  
  T obj;
  size_type size, atom_num;
};

template <typename T>
struct MutableSystemConcept
{
  void constraints() {
    boost::function_requires<boost::SequenceConcept<T> >();
    boost::function_requires<SystemConcept<T> >();
    boost::function_requires<ChemicallyTypedConcept<T> >();
  }
};

} // CONCEPTS
} // BTK

#endif
