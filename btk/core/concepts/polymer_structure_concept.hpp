// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_CONCEPTS_POLYMER_STRUCTURE_CONCEPT_HPP
#define BTK_CONCEPTS_POLYMER_STRUCTURE_CONCEPT_HPP

#include <iostream>
#include <boost/concept_check.hpp>

#include <btk/core/concepts/chemically_typed_concept.hpp>
#include <btk/core/concepts/monomer_concept.hpp>
#include <btk/core/concepts/monomer_iterable_concept.hpp>

namespace BTK {
namespace CONCEPTS {

template <typename T>
struct PolymerStructureConcept {
  typedef typename T::monomer_type mt;
  typedef typename T::monomer_iterator mi;
  typedef typename T::reverse_monomer_iterator rmi;
  typedef typename T::const_monomer_iterator cmi;
  typedef typename T::const_reverse_monomer_iterator crmi;
  typedef typename T::size_type st;

  void constraints() {
    boost::function_requires<boost::Mutable_ReversibleContainerConcept<T> >();
    boost::function_requires<StrictlyChemicallyTypedConcept<T> >();
    boost::function_requires<MonomerIterableConcept<T> >();
    boost::function_requires<MonomerConcept<mt> >();

    const_constraints(obj);
  }

  void const_constraints(T const & polymer) {
    mt const & monomer_ref = polymer[monomer_num];
    boost::ignore_unused_variable_warning(monomer_ref);

    std::cout << polymer.print(std::cout);
    std::cout << polymer.print(std::cout,atom_num,monomer_num);
  }

  T obj;
  st atom_num, monomer_num;
};

template <typename T>
struct MutablePolymerStructureConcept 
{
  typedef typename T::monomer_type mt;
  typedef typename T::size_type st;

  void constraints() {
    boost::function_requires<boost::SequenceConcept<T> >();
    boost::function_requires<ChemicallyTypedConcept<T> >();
    boost::function_requires<PolymerStructureConcept<T> >();
    boost::function_requires<MutableMonomerIterableConcept<T> >();

    mt & monomer_ref = polymer[monomer_num];
    boost::ignore_unused_variable_warning(monomer_ref);
  }

  T polymer;
  st monomer_num;
};

} // CONCEPTS
} // BTK

#endif
