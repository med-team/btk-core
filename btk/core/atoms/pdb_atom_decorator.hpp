// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file pdb_atom_decorator.hpp
/// Definition of the PDBAtomDecorator class.

#ifndef BTK_ATOMS_PDB_ATOM_DECORATOR_HPP
#define BTK_ATOMS_PDB_ATOM_DECORATOR_HPP

#include <string>
#include <ostream>

#include <btk/core/atoms/atom.hpp>
#include <btk/core/elements/element_types.hpp>
#include <btk/core/io/default_type_system.hpp>

namespace BTK {
namespace ATOMS {

/// A mixin-style atom decorator that provides support for PDB ATOM record data.
///
/// This is an atom decorator class that provides support for the storage and 
/// retrieval of data used in Brookhaven Protein Data Bank (PDB) ATOM records.
///
/// Because it is frequently necessary to use PDB I/O on a custom atom type
/// (e.g. a previously-decorated Atom object), this object allows for the
/// addition of PDB data to any atom type that conforms to the AtomConcept.  
///
/// See pdb_atom.hpp for an example of atom decoration using this class.
///
template <typename BaseAtomType = BTK::ATOMS::Atom<> >
class PDBAtomDecorator : public BaseAtomType
{
  typedef BaseAtomType base_type;
  typedef PDBAtomDecorator<base_type> self_type;
 public:
  IMPORT_ATOM_TYPES(base_type);
  typedef typename chemical_type_system::monomer_id_type monomer_id_type;
  
  /// \group AtomConcept required constructors.
  //@{

  /// \brief Default constructor.
  PDBAtomDecorator() : 
    base_type(), _res_num(1), _res_type(),
    _chain_id(' '), _alt_loc(' '), _insert_code(' '),
    _occupancy(1), _b_factor(0), _segment_id(), _charge(),
    _is_hetatom(false) {}

  /// \brief Construct from position, types and number.
  PDBAtomDecorator(::BTK::MATH::BTKVector const & pos,
                   atom_id_type type,
                   element_id_type element_type,
                   int number = 0,
                   chemical_type_system const & cts = chemical_type_system()) :
    base_type(pos,type,element_type,number,cts),
    _res_num(1), _res_type(), _chain_id(' '), _alt_loc(' '), 
    _insert_code(' '), _occupancy(1), _b_factor(0), _segment_id(), _charge(), 
    _is_hetatom(false) {}

  //@}
    
  /// \brief Construct a PDBAtom from information in a PDB ATOM or HETATM record.
  PDBAtomDecorator(chemical_type_system const & cts,
		   bool is_hetatom,
		   int atom_number,
                   atom_id_type atom_type,
		   char alt_loc,
                   monomer_id_type res_type,
		   char chain_id,
		   int res_number,
		   char insert_code,
		   ::BTK::MATH::BTKVector const & position,
		   double occupancy = 1.0,
		   double b_factor = 1.0,
		   std::string const & segment_id = "",
                   element_id_type element_type = element_id_type(), // fix me! 
		   std::string charge = "") :
    base_type(position,
              atom_type,
              element_type,
              atom_number,
              cts),
    _res_num(res_number), _res_type(res_type),
    _chain_id(chain_id), _alt_loc(alt_loc), _insert_code(insert_code),
    _occupancy(occupancy), _b_factor(b_factor),
    _segment_id(segment_id), _charge(charge),
    _is_hetatom(is_hetatom)
  {
  }
 
  /// \name Copy constructors.
  /// These constructors allow the creation of a PDBAtomDecorator from
  /// any atom type that happens to be decorated with a PDBAtomDecorator
  /// (assuming that the base_atom types are compatible).
  //@{

  /// Default copy constructor.
  /// Called iff the base atom types are identical.
  PDBAtomDecorator(self_type const & src) :
    base_type(src), _res_num(src._res_num), _res_type(src._res_type),
    _chain_id(src._chain_id), _alt_loc(src._alt_loc),
    _insert_code(src._insert_code), _occupancy(src._occupancy),
    _b_factor(src._b_factor), _segment_id(src._segment_id),
    _charge(src._charge), _is_hetatom(src._is_hetatom) {}
    
  /// Foreign atom type copy constructor.
  /// Called iff the base atom types are different.
  template <typename AtomType>
  PDBAtomDecorator(PDBAtomDecorator<AtomType> const & src) :
    base_type(src),
    _res_num(src.res_number()), _res_type(),
    _chain_id(src.chain_id()), _alt_loc(src.alt_loc()),
    _insert_code(src.insert_code()), _occupancy(src.occupancy()),
    _b_factor(src.b_factor()), _segment_id(src.segment_id()),
    _charge(src.charge()), _is_hetatom(src.is_hetatom()) 
  {
    // If we're using this constructor, the source atom doesn't have the same
    // base atom type as this class.  Thus, there's no guarantee that the 
    // monomer_id_type of the source class is the same as the monomer_id_type
    // of this class.  For this reason, look up the residue type using the
    // source atom's residue name.
    typename chemical_type_system::monomer_dictionary::const_iterator i =
      base_type::get_chemical_type_system().get_monomer_dictionary().find(src.res_name());
    
    if (i != base_type::get_chemical_type_system().get_monomer_dictionary().end())
      _res_type = i->first;
    else
      _res_type = BTK::UTILITY::TypeIDTraits<monomer_id_type>::unknown();
  }
  //@}

  /// \group PDBAtom-specific  access methods.
  //@{
  int res_number() const { return _res_num; }
  void set_res_number(int num) { _res_num = num; }

  monomer_id_type res_type() const { return _res_type; }
  void set_res_type(monomer_id_type type) { _res_type = type; }

  std::string res_name() const
  {
    typename chemical_type_system::monomer_dictionary::const_iterator i =
      base_type::get_chemical_type_system().get_monomer_dictionary().find(_res_type);

    if (i != base_type::get_chemical_type_system().get_monomer_dictionary().end()) 
      return i->second;
    else 
      return "";
  }

  char chain_id() const { return _chain_id; }
  void set_chain_id(char id) { _chain_id = id; }

  char alt_loc() const { return _alt_loc; }
  void set_alt_loc(char altloc) { _alt_loc = altloc; }

  char insert_code() const { return _insert_code; }
  void set_insert_code(char code) { _insert_code = code; }

  double occupancy() const { return _occupancy; }
  void set_occupancy(double occ) { _occupancy = occ; }

  double b_factor() const { return _b_factor; }
  void set_b_factor(double bfactor) { _b_factor = bfactor; }
  
  std::string const & segment_id() const { return _segment_id; }
  void set_segment_id(std::string const & s_id) { _segment_id = s_id; }

  std::string const & charge() const { return _charge; }
  void set_charge(std::string const & charge) { _charge = charge; }
  
  bool is_hetatom() const { return _is_hetatom; }
  void set_hetatom_flag(bool h) { _is_hetatom = h; }
  //@}

  virtual std::ostream & print(std::ostream & os,
                               int atom_number,
                               int group_number,
                               char chain_id,
                               std::string const & group_name) const
  {
    return base_type::print(os,
                            atom_number,
                            group_number,
                            chain_id,
                            group_name,
                            is_hetatom(),
                            alt_loc(),
                            insert_code(),
                            occupancy(),
                            b_factor(),
                            segment_id(),
                            base_type::element_name(),
                            charge());
  }

  virtual std::ostream & print(std::ostream & os) const
  {
    return print(os,
                 base_type::number(),
                 res_number(),
                 chain_id(),
                 res_name());
  }

  self_type const & operator=(self_type const & rhs)
    {
      base_type::operator=(rhs);
      
      _res_num = rhs._res_num;
      _res_type = rhs._res_type;
      _chain_id = rhs._chain_id;
      _alt_loc = rhs._alt_loc;
      _insert_code = rhs._insert_code;
      _occupancy = rhs._occupancy;
      _b_factor = rhs._b_factor;
      _segment_id = rhs._segment_id;
      _charge = rhs._charge;
      _is_hetatom = rhs._is_hetatom;
      
      return *this;
    }
  
  bool operator==(self_type const & rhs) const 
  {
    if (base_type::operator!=(rhs)) return false;

    return (_res_num == rhs._res_num &&
            _res_type == rhs._res_type &&
            _chain_id == rhs._chain_id &&
            _alt_loc == rhs._alt_loc &&
            _insert_code == rhs._insert_code &&
            _occupancy == rhs._occupancy &&
            _b_factor == rhs._b_factor &&
            _segment_id == rhs._segment_id &&
            _charge == rhs._charge &&
            _is_hetatom == rhs._is_hetatom);
  }

  bool operator!=(self_type const & rhs) const 
  {
    return !(*this == rhs);
  }

 private:
  int _res_num;
  monomer_id_type _res_type;
  char _chain_id, _alt_loc, _insert_code;
  double _occupancy, _b_factor;
  std::string _segment_id, _charge;
  bool _is_hetatom;
};

template <typename AT>
std::ostream & operator<<(std::ostream & os, 
			  PDBAtomDecorator<AT> const & pdb_atom)
{
  return pdb_atom.print(os);
}

} // namespace ATOMS
} // namespace BTK

#endif // BTK_ATOMS_PDB_ATOM_DECORATOR_HPP
