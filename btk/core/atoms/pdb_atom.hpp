// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file pdb_atom.hpp
/// Declaration of the PDBAtom type.

#ifndef BTK_ATOMS_PDB_ATOM_HPP
#define BTK_ATOMS_PDB_ATOM_HPP

#include <btk/core/atoms/atom.hpp>
#include <btk/core/atoms/pdb_atom_decorator.hpp>

namespace BTK {
namespace ATOMS {

typedef PDBAtomDecorator<Atom<> > PDBAtom;

} // namespace ATOMS
} // namespace BTK

#endif //BTK_PDB_ATOM_H
