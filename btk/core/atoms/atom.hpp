// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson (kid50@users.sourceforge.net)
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file atom.hpp
/// \brief definition of the Atom class.

#ifndef BTK_ATOMS_ATOM_HPP
#define BTK_ATOMS_ATOM_HPP

#include <string>
#include <ostream>
#include <cstdio>

#include <btk/core/math/btk_vector.hpp>
#include <btk/core/io/default_type_system.hpp>
#include <btk/core/utility/chemically_typed_object.hpp>

namespace BTK {
namespace ATOMS {

/// A minimal Atom class.
/// \todo Make this class abstract, so that decorated atoms cannot be 
///       used in mixed-type assignments.
///
/// This is a lightweight class that meets all of the requirements of the
/// AtomConcept.  It is intended to be a base class for further decoration
/// and customization by the user of the library.
///
/// Element and atom types are enforced at compile time through the use
/// of TypeSystem objects, which specify the atom and element ID types used
/// by Atom, as well as the type dictionaries used to associate ID values to
/// atom and element names.  If not specified by the user, every Atom uses
/// the DefaultTypeSystem object, which is a generic TypeSystem that provides
/// dynamic name resolution for atom types.
///
template <typename TS = BTK::IO::DefaultTypeSystem>
class Atom : 
    public BTK::UTILITY::ChemicallyTypedObject<TS,typename TS::atom_dictionary>
{
  typedef Atom<TS> self_type;
  typedef BTK::UTILITY::
  ChemicallyTypedObject<TS,typename TS::atom_dictionary> base_type;
  
 public:
  /// Type definitions required by the ChemicallyTypedConcept.
  //@{
  /// The type of the ChemicalTypeSystem for this class.
  typedef TS chemical_type_system;
  /// The type of the dictionary for this class.
  typedef typename base_type::dictionary dictionary;
  /// The ID type for this class (same as the atom ID type).
  typedef typename base_type::id_type id_type;
  //@}

  /// The type of the atom dictionary.
  typedef typename base_type::dictionary atom_dictionary;
  /// The type of the atom ID for this class (same as the ID type).
  typedef id_type atom_id_type;

  /// The type of the element dictionary.
  typedef typename chemical_type_system::element_dictionary element_dictionary;
  /// The type of the element ID for this class.
  typedef typename chemical_type_system::element_id_type element_id_type;

  /// Default constructor.
  Atom() : base_type(), _pos(),_number(0), _element_type(), _selected(false) {}
  
  /// Construct from data.
  /// \param pos The position of the atom.
  /// \param type The type of the atom.
  /// \param element_type The element type of the atom.
  /// \param number The number/index of the atom.
  /// \param ts An instance of the TypeSystem for this Atom class.
  Atom(BTK::MATH::BTKVector const & pos,
       atom_id_type type,
       element_id_type etype,
       int number = 0,
       chemical_type_system const & ts = chemical_type_system()) : 
    base_type(ts,type),
    _pos(pos), _number(number), _element_type(etype), _selected(false) {}

  /// Copy constructor.
  Atom(self_type const & src) :
    base_type(src), _pos(src._pos), _number(src._number),
    _element_type(src._element_type), _selected(src._selected) {}

  virtual ~Atom() {};

  using base_type::set_type;
  using base_type::set_chemical_type_system;

  virtual dictionary const & get_dictionary() const 
  {
    return base_type::get_chemical_type_system().get_atom_dictionary();
  }

  virtual dictionary & get_dictionary() 
  {
    return base_type::get_chemical_type_system().get_atom_dictionary();
  }

  /// Get the element type.
  element_id_type element_type() const { return _element_type; }
  
  /// Set the element type.
  void set_element_type(element_id_type et) { _element_type = et; }

  /// Get the element name.
  std::string element_name() const
  {
    typename element_dictionary::const_iterator i =
      base_type::get_chemical_type_system().
        get_element_dictionary().find(_element_type);
    
    if (i != base_type::
          get_chemical_type_system().get_element_dictionary().end()) 
      return i->second;
    else
      return "";
  }

  /// Get the atom number.
  int number() const { return _number; }
  /// Set the atom number.
  void set_number(int number) { _number = number; }

  /// Get the atom position.
  BTK::MATH::BTKVector const & position() const {return _pos;}
  /// Set the atom position.
  void set_position(BTK::MATH::BTKVector const & position) {_pos = position;}

  /// Is this atom selected?
  bool selected() const { return _selected; }
  /// Set the selection flag for this atom.
  void select(bool s = true) const { _selected = s; }

  /// Print an atom to a stream, in PDB format.
  /// \deprecated
  /// \todo Simplify the stream output system. PDB-formatted output
  ///       should be relegated to specialized PDB classes, so that 
  ///       simple classes (like Atom) don't have to have complicated
  ///       methods (like Atom::print()) that are only complex due to
  ///       PDB output requirements.
  virtual std::ostream & print(std::ostream & os,
                               int atom_number = 1,
                               int group_number = 1,
                               char chain_id = ' ',
                               std::string const & group_name = "",
                               bool is_hetatom = false,
                               char alt_loc = ' ',
                               char i_code = ' ',
                               double occupancy = 1.0,
                               double b_factor = 1.0,
                               std::string const & seg_id = "",
                               std::string const & element_name = "",
                               std::string const & charge = "") const
  {
    char buf[81];
    const char * leading_int_atom_format =
      "%-6.6s%5d %-4.4s%c%3.3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f      %4.4s%2.2s%2.2s";
    const char * leading_char_atom_format =
      "%-6.6s%5d  %-3.3s%c%3.3s %c%4d%c   %8.3f%8.3f%8.3f%6.2f%6.2f      %4.4s%2.2s%2.2s";
    const char * format = leading_char_atom_format;
    
    const char * record_type;

    if (is_hetatom) record_type = "HETATM";
    else record_type = "ATOM";

    std::string el_name(element_name);

    if (el_name == "") {
      el_name = Atom::element_name();
    }

    std::string atom_name = Atom::name();

    if (atom_name.size() && isdigit(atom_name[0])) {
      format = leading_int_atom_format;
    }

    snprintf(buf,81,format,
             record_type,
             atom_number,
             atom_name.c_str(),
             alt_loc,
             group_name.c_str(),
             chain_id,
             group_number,
             i_code,
             position()[0],
             position()[1],
             position()[2],
             occupancy,
             b_factor,
             seg_id.c_str(),
             el_name.c_str(),
             charge.c_str());

    os << buf << std::endl;

    return os;
  }

  /// Assignment operator.
  /// \todo The Atom class should be made abstract, and the assignment
  ///       operator protected.
  self_type const & operator=(self_type const & src)
  {
    if (this == &src) return *this;

    base_type::operator=(src);
    _pos = src._pos;
    _number = src._number;
    _element_type = src._element_type;
    _selected = src._selected;
    return *this;
  }

  /// Test atom equality.
  /// Two atoms are equivalent iff their types and positions are equivalent.
  bool operator==(self_type const & rhs) const
  {
    return (_element_type == rhs._element_type &&
            _number == rhs._number &&
            _pos == rhs._pos &&
            base_type::operator==(rhs));
  }

  /// Test atom inequality.
  bool operator!=(self_type const & rhs) const
  {
    return !(*this == rhs);
  }

private:
  MATH::BTKVector _pos;
  int _number;
  element_id_type _element_type;

  mutable bool _selected;
};

template <typename TS>
std::ostream & operator<<(std::ostream & os, Atom<TS> const & a)
{
  return a.print(os);
}

#define IMPORT_ATOM_TYPES(AT)                                     \
  typedef typename AT::chemical_type_system chemical_type_system; \
  typedef typename AT::dictionary dictionary;                     \
  typedef typename AT::id_type id_type;                           \
  typedef typename AT::atom_dictionary atom_dictionary;           \
  typedef typename AT::element_dictionary element_dictionary;     \
  typedef typename AT::atom_id_type atom_id_type;                 \
  typedef typename AT::element_id_type element_id_type; 

} // namespace ATOMS
} // namespace BTK

#endif // BTK_ATOMS_ATOM_HPP
