// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file btk_sequence.hpp
//! Declaration of the BTKSequence class.

#ifndef BTK_UTILITY_BTK_SEQUENCE_HPP
#define BTK_UTILITY_BTK_SEQUENCE_HPP

#include <btk/core/utility/btk_container.hpp>

namespace BTK {
namespace UTILITY {
  
template <typename T, typename StorageStrategy>
class BTKSequence : public BTKContainer<T,StorageStrategy>
{
  BOOST_CLASS_REQUIRE(StorageStrategy,boost,SequenceConcept);
  typedef BTKSequence<T,StorageStrategy> self_type;
  typedef BTKContainer<T,StorageStrategy> base_type;

public:
  IMPORT_BTK_CONTAINER_TYPES(base_type);

  //! \group Constructors required by the STL Reversible Container Concept.
  //@{
  
  BTKSequence(self_type const & source) : base_type(source) {}
  virtual ~BTKSequence() {}

  //@}

  //! \group Constructors required by the STL Sequence Concept.
  //@{
  BTKSequence(size_type n = 0, const_reference t = value_type()) : 
    base_type(n,t) {}
  
  template <typename InputIterator>
  BTKSequence(InputIterator i, InputIterator j) : base_type(i,j) {}

  //@}

  //! \group Methods required by the STL Sequence Concept
  //@{
  
  reference front() { return base_type::_storage.front(); }
  const_reference front() const { return base_type::_storage.front(); }
  
  virtual iterator insert(iterator p, const_reference t) 
  {
    return base_type::_storage.insert(p,t);
  }

  virtual void insert(iterator p, size_type n, const_reference t) 
  {
    base_type::_storage.insert(p,n,t);
  }

  template <typename InputIterator>
  void insert(iterator p, InputIterator i, InputIterator j)
  {
    boost::function_requires<boost::InputIteratorConcept<InputIterator> >();
    base_type::_storage.insert(p,i,j);
  }

  virtual iterator erase(iterator p) { return base_type::_storage.erase(p); }
  virtual iterator erase(iterator p, iterator q) 
  { return base_type::_storage.erase(p,q); }

  virtual void clear() { base_type::_storage.clear(); }

  virtual void resize(size_type n, const_reference t = value_type()) 
  { base_type::_storage.resize(n,t); }
  
  //@}
};

// The declaration of the template function in this macro is a workaround
// for g++ versions < 4, b/c they get pissy about using declarations w/
// template member functions.
#define IMPORT_BTK_SEQUENCE_METHODS(Sequence)                       \
  using Sequence::front;                                            \
  using Sequence::insert;                                           \
  template <typename InputIterator>                                 \
  void insert(iterator p, InputIterator i, InputIterator j)         \
  { Sequence::insert(p,i,j); }                                      \
  using Sequence::erase;                                            \
  using Sequence::clear;                                            \
  using Sequence::resize;

} // namespace UTILITY
} // namespace BTK

#endif // BTK_UTILITY_BTK_SEQUENCE_HPP
