// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//! \file chemically_typed_object.hpp
//! Definition of the ChemicallyTypedObject template class.

#ifndef BTK_UTILITY_CHEMICALLY_TYPED_OBJECT_HPP
#define BTK_UTILITY_CHEMICALLY_TYPED_OBJECT_HPP

#include <string>
#include <algorithm>

#include <boost/operators.hpp>

#include <btk/core/concepts/type_system_concept.hpp>

namespace BTK {
namespace UTILITY {

//! \brief A base class for objects that need to conform to the 
//!        \ref chemically_typed_concept "ChemicallyTyped" concept.
//!
//! This is a base class useful for implementing types that 
//! need to model the 
//! \ref strictly_chemically_typed_concept "StrictlyChemicallyTyped"
//! and \ref chemically_typed_concept "ChemicallyTyped" concepts. 
template <typename ChemicalTypeSystemType,
	  typename DictionaryType>
class ChemicallyTypedObject :
    protected boost::less_than_comparable<ChemicallyTypedObject<ChemicalTypeSystemType,
                                                                DictionaryType> >
{
  BOOST_CLASS_REQUIRE(ChemicalTypeSystemType,BTK::CONCEPTS,TypeSystemConcept);
  typedef ChemicallyTypedObject<ChemicalTypeSystemType,
                                DictionaryType> self_type;
public:
  typedef ChemicalTypeSystemType chemical_type_system;
  typedef DictionaryType dictionary;
  typedef typename dictionary::id_type id_type;

  ChemicallyTypedObject(chemical_type_system const & ts = chemical_type_system(),
			id_type t = id_type()):
    _type_system(ts), _type(t) {}

  ChemicallyTypedObject(self_type const & src) :
    _type_system(src._type_system), _type(src._type) {}

  virtual ~ChemicallyTypedObject() {}

  //! Returns the chemical id type of the object.
  id_type type() const { return _type; }

  //! Returns the name of the object.
  //! A "name," in this context, is a textual representation of the chemical 
  //! type (i.e. an atom name or a residue name).  If the chemical type is
  //! undefined or unknown, the empty string is returned.
  std::string name() const
  {
    typename dictionary::const_iterator i = get_dictionary().find(_type);

    if (i != get_dictionary().end()) return i->second;
    return "";
  }

  //! Returns a const reference to the chemical type system for the object.
  chemical_type_system const & get_chemical_type_system() const 
  {
    return _type_system;
  }

  //! Returns a mutable reference to the chemical type system for the object.
  chemical_type_system & get_chemical_type_system()
  {
    return _type_system;
  }
 
  virtual dictionary const & get_dictionary() const = 0;
  virtual dictionary & get_dictionary() = 0;

protected:
  
  //! Set the chemical ID type of the object to a new value.
  virtual void set_type(id_type t) { _type = t; }
  
  //! Set a new chemical type system for the object.
  virtual void set_chemical_type_system(chemical_type_system const & ts) 
  {
    _type_system = ts;
  }

  self_type const & operator=(self_type const & src)
  {
    if (this == &src) return *this;
    _type_system = src._type_system;
    _type = src._type;
    return *this;
  }

  void swap(self_type & rhs)
  {
    std::swap(_type_system,rhs._type_system);
    std::swap(_type,rhs._type);
  }

  //! Comparison operators.
  //! Note: the full EqualityComparable and LessThanComparable concepts are 
  //! supported through the use of the boost::operators template classes.
  //! These operators are protected, in order to prevent unintentional 
  //! heterogeneous type comparisons.
  //@{
  bool operator==(self_type const & rhs) const
  {
    return (_type_system == rhs._type_system &&
            _type == rhs._type);
  }

  bool operator!=(self_type const & rhs) const
  {
    return !(*this == rhs);
  }

  bool operator<(self_type const & rhs) const
  {
    return (_type_system == rhs._type_system &&
            _type < rhs._type);
  }
  //@}

private:
  chemical_type_system _type_system;
  id_type _type;
};

#define IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(CTOType)                  \
  typedef typename CTOType::chemical_type_system chemical_type_system; \
  typedef typename CTOType::dictionary dictionary;                     \
  typedef typename CTOType::id_type id_type;

} // UTILITY
} // BTK

#endif //BTK_UTILITY_CHEMICALLY_TYPED_OBJECT_HPP 
