// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file type_id.hpp
/// Definition of the TypeID mixin base class.

#ifndef BTK_UTILITY_TYPE_ID_HPP
#define BTK_UTILITY_TYPE_ID_HPP

#include <ostream>

namespace BTK {
namespace UTILITY {

/// \brief TypeID is a mixin-base class for defining ID types.
///
/// It is frequently a pain to define a new ID type for a given purpose,
/// because a number of operators and constructors need to be written
/// for each ID type.  This class is a tool to speed up this process.
/// Essentially, the author of a new ID type need only derive from this
/// class, with the new ID type as the first template parameter.
template <typename T, typename V = int>
class TypeID
{
 public:
  typedef TypeID<T,V> self_type;

  TypeID(self_type const & src) : _value(src._value) {}
  
  /// \brief The destructor for this class is intentionally non-virtual.
  /// TypeID is intended to be a lightweight class.  Vtables are inappropriate.
  ~TypeID() {}

  /// \brief Implicit conversion to value type.
  operator V() const { return _value; }

  bool operator==(self_type const & rhs) const
  {
    return _value == rhs._value;
  }

  bool operator!=(self_type const & rhs) const
  {
    return _value != rhs._value;
  } 

  bool operator<(self_type const & rhs) const 
  {
    return _value < rhs._value;
  }

  bool operator<=(self_type const & rhs) const 
  {
    return _value <= rhs._value;
  }

  bool operator>(self_type const & rhs) const
  {
    return _value > rhs._value;
  }

  bool operator>=(self_type const & rhs) const
  {
    return _value >= rhs._value;
  }

  self_type & operator=(self_type const & rhs) 
  {
    _value = rhs._value;
    return *this;
  }

  friend std::ostream & operator<<(std::ostream & os,
				   self_type const & t)
  {
    os << t._value;
    return os;
  }
  
protected:
  TypeID(V value) : _value(value) {}

  void increment() { ++_value; }
  void decrement() { --_value; }
  
private:
  V _value;
};

} // namespace UTILITY
} // namespace BTK

#endif
