// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_UTILITY_TYPE_ID_TRAITS_HPP
#define BTK_UTILITY_TYPE_ID_TRAITS_HPP

#include <limits>

namespace BTK {
namespace UTILITY {

template <typename ID>
struct TypeIDTraits
{
  static const bool dynamic;

  static ID first();
  static ID last();
  static ID unknown();
  static ID next(ID id);
};

template <>
struct TypeIDTraits<int>
{
  static const bool dynamic = true;

  static int first() { return 0; }
  static int last() { return std::numeric_limits<int>::max()-1; }
  static int unknown() { return std::numeric_limits<int>::max(); }

  static int next(int id) { return ++id; }
};

template <>
struct TypeIDTraits<unsigned>
{ 
  static const bool dynamic = true;

  static unsigned first() { return std::numeric_limits<unsigned>::min(); }
  static unsigned last() { return std::numeric_limits<unsigned>::max()-1; }
  static unsigned unknown() { return std::numeric_limits<unsigned>::max(); }

  static unsigned next(unsigned id) { return ++id; }
};

} // UTILITY
} // BTK

#endif
