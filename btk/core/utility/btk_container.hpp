// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Christopher Saunders <ctsa@users.sourceforge.net>,
//                         Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file btk_container.hpp
//! Declaration of BTKContainer class.

#ifndef BTK_UTILITY_BTK_CONTAINER_HPP
#define BTK_UTILITY_BTK_CONTAINER_HPP

#include <iterator>
#include <vector>

#include <boost/concept_check.hpp>

namespace BTK {
namespace UTILITY {

#ifndef DOXYGEN_SHOULD_IGNORE_THIS
namespace internal {

template <typename C>
typename C::reference
get_element(C & container, unsigned n) 
{
  typename C::iterator i = container.begin();
  std::advance(i,n);
  return *i;
} 

template <typename V>
typename std::vector<V>::reference
get_element(std::vector<V> & container, unsigned n)
{
  return container[n];
}

template <typename C>
typename C::const_reference
get_element(C const & container, unsigned n) 
{
  typename C::const_iterator i = container.begin();
  std::advance(i,n);
  return *i;
} 

template <typename V>
typename std::vector<V>::const_reference
get_element(std::vector<V> const & container, unsigned n)
{
  return container[n];
}

} // BTK::UTILITY::internal
#endif // DOXYGEN_SHOULD_IGNORE_THIS

template <typename T, typename StorageStrategy>
class BTKContainer
{
  BOOST_CLASS_REQUIRE(StorageStrategy,boost,Mutable_ReversibleContainerConcept);

public:
  typedef BTKContainer<T,StorageStrategy> self_type;

  typedef T value_type;
  typedef typename StorageStrategy::iterator iterator;
  typedef typename StorageStrategy::const_iterator const_iterator;
  typedef typename StorageStrategy::reverse_iterator reverse_iterator;
  typedef typename StorageStrategy::const_reverse_iterator const_reverse_iterator;
  typedef typename StorageStrategy::reference reference;
  typedef typename StorageStrategy::const_reference const_reference;
  typedef typename StorageStrategy::pointer pointer;
  typedef typename StorageStrategy::const_pointer const_pointer;
  typedef typename StorageStrategy::difference_type difference_type;
  typedef typename StorageStrategy::size_type size_type;

  //! \group Methods required by the STL Reversible Container Concept
  //@{
  BTKContainer(self_type const & source) : _storage(source._storage) {}
  virtual ~BTKContainer() {}

  iterator begin() { return _storage.begin(); }
  const_iterator begin() const { return _storage.begin(); }
  
  iterator end() { return _storage.end(); }
  const_iterator end() const { return _storage.end(); }

  reverse_iterator rbegin() { return _storage.rbegin(); }
  const_reverse_iterator rbegin() const { return _storage.rbegin(); }
  
  reverse_iterator rend() { return _storage.rend(); }
  const_reverse_iterator rend() const { return _storage.rend(); }
  
  size_type size() const { return _storage.size(); }
  size_type max_size() const { return _storage.max_size(); }

  bool empty() const { return _storage.empty(); }
  //@}

  reference operator[](size_type n) 
  {
    return internal::get_element(_storage,n);
  }

  const_reference operator[](size_type n) const
  {
    return internal::get_element(_storage,n);
  }

protected:
  
  //! \group Methods required by the STL Reversible Container Concept,
  //!        but protected to prevent heterogenous type operations in
  //!        derived types.
  //@{
  void swap(self_type & b) { _storage.swap(b._storage); }
  
  self_type & operator=(self_type const & rhs) 
  { 
    _storage = rhs._storage; 
    return *this; 
  }

  bool operator==(self_type const & rhs) const
  {
    return _storage == rhs._storage;
  }

  bool operator!=(self_type const & rhs) const
  {
    return _storage != rhs._storage;
  }

  bool operator<(self_type const & rhs) const
  {
    return _storage < rhs._storage;
  }
  //@}
  
#define DOXYGEN_SHOULD_IGNORE_THIS
  // These are constructors that are needed for the STL Sequence concept and
  // the BTKSequence class, but aren't part of the STL Reversible Container concept.
  BTKContainer(size_type n = 0, const_reference t = value_type()) : _storage(n,t) {}
  
  template <typename InputIterator>
  BTKContainer(InputIterator i, InputIterator j) : _storage(i,j) 
  {
    boost::function_requires<boost::InputIteratorConcept<InputIterator> >();
  }
#undef DOXYGEN_SHOULD_IGNORE_THIS

  StorageStrategy _storage;
};

#define IMPORT_BTK_CONTAINER_TYPES(Container)                           \
  typedef typename Container::value_type value_type;                    \
  typedef typename Container::iterator iterator;                        \
  typedef typename Container::const_iterator const_iterator;            \
  typedef typename Container::reverse_iterator reverse_iterator;        \
  typedef typename Container::const_reverse_iterator const_reverse_iterator; \
  typedef typename Container::reference reference;                      \
  typedef typename Container::const_reference const_reference;          \
  typedef typename Container::pointer pointer;                          \
  typedef typename Container::const_pointer const_pointer;              \
  typedef typename Container::difference_type difference_type;          \
  typedef typename Container::size_type size_type;

#define IMPORT_BTK_CONTAINER_METHODS(Container)                 \
  using Container::begin;                                       \
  using Container::end;                                         \
  using Container::rbegin;                                      \
  using Container::rend;                                        \
  using Container::size;                                        \
  using Container::max_size;                                    \
  using Container::empty;                                       \
  using Container::operator[];                                  

} // namesapce UTILITY
} // namespace BTK

#endif // BTK_UTILITY_BTK_CONTAINER_HPP
