// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file molecule.hpp
//! \brief Definition of the Molecule class.

#ifndef BTK_MOLECULES_MOLECULE_HPP
#define BTK_MOLECULES_MOLECULE_HPP

#include <btk/core/molecules/chain.hpp>

namespace BTK {
namespace MOLECULES {

//! \brief Generic molecule class.
//!
//! Molecule is a rigid-body molecule class, meaning that it can
//! represent molecules as fixed collections of atoms/points that
//! may be translated, rotated and transformed like any other fixed
//! set of points in space.  
//!
//! Because Molecule is a simple, "bag of atoms" class, the interface
//! is more permissive than a regular AtomicStructure.  It is safe to 
//! insert/erase individual atoms in a Molecule, and to support this,
//! Molecule conforms to the complete STL Sequence Concept.
//!
template <typename AtomType,
          typename ChemicalTypeSystemType = 
            typename AtomType::chemical_type_system,
          typename DictionaryType = 
            typename ChemicalTypeSystemType::structure_dictionary,
          typename StorageStrategy = std::vector<AtomType> >
class Molecule :
    public Chain<AtomType,
                 ChemicalTypeSystemType,
                 DictionaryType,
                 StorageStrategy>,
    public boost::less_than_comparable<Chain<AtomType,
                                             ChemicalTypeSystemType,
                                             DictionaryType,
                                             StorageStrategy> >
{
  typedef Chain<AtomType,
                ChemicalTypeSystemType,
                DictionaryType,
                StorageStrategy> base_type;
  typedef Molecule<AtomType,
                   ChemicalTypeSystemType,
                   DictionaryType,
                   StorageStrategy> self_type;

public:
  IMPORT_ATOMIC_STRUCTURE_TYPES(base_type);

  Molecule(size_type n = 0, 
           const_reference t = atom_type(),
           id_type type = id_type(),
           char chain_id = ' ') :
    base_type(n,t,type,chain_id) {}

  template <typename AtomIterator>
  Molecule(AtomIterator i, AtomIterator j, 
           id_type type = id_type(), 
           char chain_id = ' ') :
    base_type(i,j,type,chain_id) {}

  Molecule(self_type const & source) : base_type(source) {}

  virtual ~Molecule() {}

  //
  // Bring the STL Sequence Concept methods into the public interface.
  // (It's OK to insert/delete individual atoms into a Molecule).
  //
  IMPORT_BTK_SEQUENCE_METHODS(base_type);

  using base_type::set_type;
  using base_type::set_chemical_type_system;

  virtual dictionary const & get_dictionary() const 
  {
    return base_type::get_chemical_type_system().get_structure_dictionary();
  }

  virtual dictionary & get_dictionary() 
  {
    return base_type::get_chemical_type_system().get_structure_dictionary();
  }

  void swap(self_type & b)
  {
    base_type::swap(b);
  }

  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    base_type::operator=(rhs);
    return *this;
  }

  bool operator==(self_type const & rhs) const
  {
    return base_type::operator==(rhs);
  }

  bool operator!=(self_type const & rhs) const
  {
    return base_type::operator!=(rhs);
  }

  bool operator<(self_type const & rhs) const
  {
    return base_type::operator<(rhs);
  }
};

} // namespace MOLECULES
} // namespace BTK

#endif
