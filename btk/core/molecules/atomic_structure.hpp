// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file atomic_structure.hpp
//! \brief definition of AtomicStructure base class.

#ifndef BTK_MOLECULES_ATOMIC_STRUCTURE_HPP
#define BTK_MOLECULES_ATOMIC_STRUCTURE_HPP

#include <vector>
#include <ostream>
#include <algorithm>
#include <iterator>

#include <btk/core/utility/btk_sequence.hpp>
#include <btk/core/utility/chemically_typed_object.hpp>
#include <btk/core/concepts/atom_concept.hpp>
#include <btk/core/concepts/atom_iterator_concept.hpp>

namespace BTK {
namespace MOLECULES {


//! A class for representing atomic structures.
//!
//! AtomicStructure is a base class for all atom-based molecules in
//! the BTK.  It is an STL Bidirectional Container of atom objects
//! (i.e. objects that conform to the AtomConcept), and as such, does
//! not support the direct insertion or deletion of atoms, but does
//! support atom iteration, assignment, equality comparison and other
//! methods required by the Bidirectional Container concept.
//! 
//! This class is a model of the AtomicStructureConcept.
//!
template <typename AtomType,
          typename ChemicalTypeSystemType,
          typename DictionaryType,
          typename StorageStrategy = std::vector<AtomType> >
class AtomicStructure : 
    public BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                               DictionaryType>,
    protected BTK::UTILITY::BTKSequence<AtomType,StorageStrategy>
{
  typedef BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                              DictionaryType> cto_type;
  typedef BTK::UTILITY::BTKSequence<AtomType,StorageStrategy> btk_seq_type;
  typedef AtomicStructure<AtomType,
                          ChemicalTypeSystemType,
                          DictionaryType,
                          StorageStrategy> self_type;
public:  
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(cto_type);
  IMPORT_BTK_CONTAINER_TYPES(btk_seq_type);

  typedef value_type atom_type;
  typedef iterator atom_iterator;
  typedef const_iterator const_atom_iterator;
  typedef reverse_iterator reverse_atom_iterator;
  typedef const_reverse_iterator const_reverse_atom_iterator;
  typedef id_type structure_id_type;
  typedef typename atom_type::id_type atom_id_type;

  BOOST_CLASS_REQUIRE(atom_type,BTK::CONCEPTS,AtomConcept);
  BOOST_CLASS_REQUIRE(iterator,BTK::CONCEPTS,MutableAtomIteratorConcept);
  BOOST_CLASS_REQUIRE(reverse_iterator,BTK::CONCEPTS,MutableAtomIteratorConcept);
  BOOST_CLASS_REQUIRE(const_iterator,BTK::CONCEPTS,AtomIteratorConcept);                      
  BOOST_CLASS_REQUIRE(const_reverse_iterator,BTK::CONCEPTS,AtomIteratorConcept);

  AtomicStructure(self_type const & source) : 
    cto_type(source), btk_seq_type(source) {}

  virtual ~AtomicStructure() {}

  IMPORT_BTK_CONTAINER_METHODS(btk_seq_type)

  //! Get the size of an AtomicStructure, in atoms.
  size_type num_atoms() const { return btk_seq_type::size(); }

  //@{
  //! \brief Beginning of atom range.
  atom_iterator structure_begin() { return btk_seq_type::begin(); }
  const_atom_iterator structure_begin() const { return btk_seq_type::begin(); }
  //@}

  //@{
  //! \brief Beginning of reversed atom range.
  reverse_atom_iterator structure_rbegin() { return btk_seq_type::rbegin(); }
  const_reverse_atom_iterator structure_rbegin() const
  { return btk_seq_type::rbegin(); }
  //@}

  //@{
  //! \brief End of atom range.
  atom_iterator structure_end() { return btk_seq_type::end(); }
  const_atom_iterator structure_end() const { return btk_seq_type::end(); }
  //@}

  //@{
  //! \brief End of reversed atom range.
  reverse_atom_iterator structure_rend() { return btk_seq_type::rend(); }
  const_reverse_atom_iterator structure_rend() const { return btk_seq_type::rend(); }
  //@}

  using cto_type::type;
  using cto_type::name;
  using cto_type::get_chemical_type_system;
  
  //! Set the TypeSystem object associated with this AtomicStructure.
  virtual void set_chemical_type_system(chemical_type_system const & cts) 
  { 
    // call base implementation
    cto_type::set_chemical_type_system(cts);

    // set the type systems of every atom
    for (atom_iterator ai = structure_begin(); ai != structure_end(); ++ai)
      ai->set_chemical_type_system(cts);
  }

  //! Print an AtomicStructure to a stream.
  //! \deprecated
  //! \todo Simplify the stream output system.  See Atom for details.
  virtual std::ostream & print(std::ostream & os, 
                               size_type first_atom_num = 1,
                               size_type group_num = 1,
                               char chain_id = ' ') const
  {
    const_atom_iterator ai;
    size_type atom_num = first_atom_num;
    size_type group = group_num;
    
    for (ai = structure_begin(); ai != structure_end(); ++ai)
      ai->print(os,atom_num++,
                group,chain_id,
                name());
    
    return os;
  }

  virtual dictionary const & get_dictionary() const = 0;
  virtual dictionary & get_dictionary() = 0;

protected:

  //! Constructors used to implement derived classes.
  //@{
  AtomicStructure(size_type n = 0, 
                  const_reference t = value_type(),
                  id_type type = id_type()) : 
    cto_type(t.get_chemical_type_system(),type), btk_seq_type(n,t) {}
  
  template <typename AtomIterator>
  AtomicStructure(AtomIterator i, AtomIterator j, 
                  id_type type = id_type()) : 
    cto_type(i->get_chemical_type_system(),type), btk_seq_type(i,j)
  {
    boost::function_requires<BTK::CONCEPTS::AtomIteratorConcept<AtomIterator> >();
    
    // The atoms in the range passed to this constructor must be convertible
    // to the atom_type of the class!  If you get a compile error here,
    // it probably means that you're trying to construct some kind of 
    // AtomicStructure using an atom type that is incompatible with the 
    // atom type of the class (a different TypeSystem, for example).
    typedef typename std::iterator_traits<AtomIterator>::value_type a_t;
    boost::function_requires<boost::ConvertibleConcept<a_t,atom_type> >();
  }
  //@}

  using cto_type::set_type;

  //! Assignment operator.
  //! The contents of this AtomicStructure are replaced with those of rhs.
  self_type const & operator=(self_type const & rhs) 
  {
    if (this == &rhs) return *this; 
    cto_type::operator=(rhs);
    btk_seq_type::operator=(rhs);
    return *this;
  }

  //! Swap method.
  //! Swaps this AtomicStructure with another.
  void swap(self_type & b)
  {
    cto_type::swap(b);
    btk_seq_type::swap(b);
  }
  
  //! Equality comparison.
  //! Two AtomicStructures are equivalent iff their types are equivalent,
  //! and all of their contained atoms are equivalent.
  //@{
  bool operator==(self_type const & rhs) const {
    return (cto_type::operator==(rhs) && btk_seq_type::operator==(rhs));
  }
  
  bool operator!=(self_type const & rhs) const {
    return !(operator==(rhs));
  }
  //@}
  
  //! Less-than comparision.
  //! AtomicStructure A is less than AtomicStructure B if the type of
  //! A is less than the type of B, or if the types of A and B are equal, and 
  //! the atoms of A are less than B using lexicographical ordering rules.
  bool operator<(self_type const & rhs) const {
    if (cto_type::operator<(rhs)) 
      return true;
    else if (cto_type::operator==(rhs))
      return btk_seq_type::operator<(rhs);
    else 
      return false;
  }
};

#define IMPORT_ATOMIC_STRUCTURE_TYPES(ASType)                           \
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(ASType)                          \
  IMPORT_BTK_CONTAINER_TYPES(ASType)                                    \
                                                                        \
  typedef typename ASType::atom_type atom_type;                         \
  typedef typename ASType::atom_iterator atom_iterator;                 \
  typedef typename ASType::const_atom_iterator const_atom_iterator;     \
  typedef typename ASType::reverse_atom_iterator reverse_atom_iterator; \
  typedef typename ASType::const_reverse_atom_iterator                  \
  const_reverse_atom_iterator;                                          \
  typedef typename ASType::structure_id_type structure_id_type;         \
  typedef typename ASType::atom_id_type atom_id_type;

template <typename AT, typename CTO, typename DICT, typename SS>
std::ostream & operator<<(std::ostream & os, AtomicStructure<AT,CTO,DICT,SS> const & a)
{
  return a.print(os);
}

} // namespace MOLECULES
} // namespace BTK

#endif
