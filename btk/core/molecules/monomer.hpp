// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2003-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file monomer.hpp
/// \brief Definition of Monomer class.
///

#ifndef BTK_MOLECULES_MONOMER_HPP
#define BTK_MOLECULES_MONOMER_HPP

#include <boost/operators.hpp>

#include <btk/core/molecules/atomic_structure.hpp>

namespace BTK {
namespace MOLECULES {

template <typename AtomType,
          typename ChemicalTypeSystemType =
            typename AtomType::chemical_type_system,
          typename DictionaryType =
            typename ChemicalTypeSystemType::monomer_dictionary,
          typename StorageStrategy = std::vector<AtomType> >
class Monomer : 
    public AtomicStructure<AtomType,
                           ChemicalTypeSystemType,
                           DictionaryType,
                           StorageStrategy>,
    public boost::less_than_comparable<Monomer<AtomType,
                                               ChemicalTypeSystemType,
                                               DictionaryType,
                                               StorageStrategy> >
{
  typedef AtomicStructure<AtomType,
                          ChemicalTypeSystemType,
                          DictionaryType,
                          StorageStrategy> base_type;
  typedef Monomer<AtomType,
                  ChemicalTypeSystemType,
                  DictionaryType,
                  StorageStrategy> self_type;
public:
  IMPORT_ATOMIC_STRUCTURE_TYPES(base_type);
  typedef id_type monomer_id_type;

  Monomer(unsigned n = 0,
          atom_type const & a = atom_type(),
          id_type type = id_type(),
          int number = 0) : 
    base_type(n,a,type), _number(number), _selected(false) {}
  
  template <typename AtomIterator>
  Monomer(AtomIterator i, AtomIterator j,
          monomer_id_type type,
          int number) : 
    base_type(i,j,type), _number(number), _selected(false) {}

  Monomer(self_type const & source) : 
    base_type(source), _number(source._number), _selected(false) {}

  virtual ~Monomer() {}

  //
  // Methods required for the BTKContainer concept.
  //
  IMPORT_BTK_CONTAINER_METHODS(base_type);

  //
  // Methods required for the Monomer concept.
  //
  atom_iterator monomer_begin() { return base_type::structure_begin(); }
  const_atom_iterator monomer_begin() const 
  { 
    return base_type::structure_begin();
  }

  atom_iterator monomer_end() { return base_type::structure_end(); }
  const_atom_iterator monomer_end() const 
  { 
    return base_type::structure_end();
  }

  reverse_atom_iterator monomer_rbegin() { return base_type::structure_rbegin(); }
  const_reverse_atom_iterator monomer_rbegin() const 
  { 
    return base_type::structure_rbegin();
  }

  reverse_atom_iterator monomer_rend() { return base_type::structure_rend(); }
  const_reverse_atom_iterator monomer_rend() const 
  { 
    return base_type::structure_rend();
  }

  int number() const { return _number; }
  void set_number(int number) { _number = number; }

  bool selected() const { return _selected; }
  void select(bool s = true) const 
  { 
    _selected = s; 
    const_atom_iterator end = base_type::structure_end();

    for (const_atom_iterator i = base_type::structure_begin(); i != end; ++i)
      i->select(s);
  }

  virtual dictionary const & get_dictionary() const 
  {
    return base_type::get_chemical_type_system().get_monomer_dictionary();
  }

  virtual dictionary & get_dictionary() 
  {
    return base_type::get_chemical_type_system().get_monomer_dictionary();
  }

  void swap(self_type & b)
  {
    base_type::swap(b);
    std::swap(_number,b._number);
    std::swap(_selected,b._selected);
  }

  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    base_type::operator=(rhs);
    _number = rhs._number;
    _selected = rhs._selected;
    return *this;
  }

  bool operator==(self_type const & rhs) const 
  {
    return (_number == rhs._number &&
            base_type::operator==(rhs));
  }

  bool operator!=(self_type const & rhs) const
  {
    return !(operator==(rhs));
  }

  bool operator<(self_type const & rhs) const
  {
    return (_number < rhs._number || 
            (_number == rhs._number && base_type::operator<(rhs)));
  }

private:
  int _number;
  mutable bool _selected;
};

#define IMPORT_MONOMER_TYPES(MonomerType)                          \
  IMPORT_ATOMIC_STRUCTURE_TYPES(MonomerType)                       \
  typedef typename MonomerType::monomer_id_type monomer_id_type;

} // namespace MOLECULES
} // namespace BTK

#endif //BTK_MOLECULES_MONOMER_HPP
