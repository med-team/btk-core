// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file system.hpp
//! \brief Definition of the System class.

#ifndef BTK_MOLECULES_SYSTEM_HPP
#define BTK_MOLECULES_SYSTEM_HPP

#include <ostream>
#include <list>
#include <iterator>

#include <btk/core/utility/chemically_typed_object.hpp>
#include <btk/core/utility/btk_sequence.hpp>
#include <btk/core/concepts/chain_iterator_concept.hpp>
#include <btk/core/molecules/system_base.hpp>

namespace BTK {
namespace MOLECULES {

//! \brief A class for representing systems of molecules.
//!
//! \param ChainType The type of chain/molecule stored in the system (must be a 
//!                  model of the BTK ChainConcept).
//!
//! \param StorageStrategy The underlying storage method (must be a model of 
//!                        the STL Sequence concept).
//!
//! A System stores molecules (or more strictly, chains of atoms).
//! It is a BTKSequence of ChainType objects, and therefore, a model of the 
//! BTKMutableContainerConcept (with a value_type equivalent to the ChainType).  
//!
//! This class requires that its ChainType be a model of the ChainConcept,
//! but beyond that, no further requirements are imposed on the type of the
//! object used to instantiate the class.  If the ChainType is also a model of
//! the MonomerIterableConcept, the resulting System type will be a model of
//! the MonomerIterableConcept.  
//!
template <typename ChainType,
          typename ChemicalTypeSystemType =
            typename ChainType::chemical_type_system,
	  typename StorageStrategy = std::list<ChainType> >
class System :
    public BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                               typename ChemicalTypeSystemType::
                                               structure_dictionary>,
    public BTK::UTILITY::BTKSequence<ChainType,StorageStrategy>,
    public detail::PolymerSystemBase<System<ChainType,
                                            ChemicalTypeSystemType,
                                            StorageStrategy>,
                                     ChainType,
                                     typename BTK::UTILITY::BTKSequence<ChainType,
                                                                        StorageStrategy>::iterator,
                                     typename BTK::UTILITY::BTKSequence<ChainType,
                                                                        StorageStrategy>::const_iterator,
                                     detail::IsMonomerIterable<ChainType>::value>
{
  typedef BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                              typename ChemicalTypeSystemType::
                                              structure_dictionary> cto_type;
  typedef BTK::UTILITY::BTKSequence<ChainType,
                                    StorageStrategy> btk_seq_type;
  typedef System<ChainType,
                 ChemicalTypeSystemType,
                 StorageStrategy> self_type;

public:
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(cto_type);
  IMPORT_BTK_CONTAINER_TYPES(btk_seq_type);

  ///\group Type definitions required for all System classes.
  //@{
  typedef value_type chain_type;
  typedef iterator chain_iterator;
  typedef const_iterator const_chain_iterator;
  typedef reverse_iterator reverse_chain_iterator;
  typedef const_reverse_iterator const_reverse_chain_iterator;
  typedef id_type structure_id_type;
  //@}

  BOOST_CLASS_REQUIRE(chain_type,BTK::CONCEPTS,ChainConcept);
  BOOST_CLASS_REQUIRE(chain_iterator,BTK::CONCEPTS,MutableChainIteratorConcept);
  BOOST_CLASS_REQUIRE(const_chain_iterator,BTK::CONCEPTS,ChainIteratorConcept);
  BOOST_CLASS_REQUIRE(reverse_chain_iterator,BTK::CONCEPTS,MutableChainIteratorConcept);
  BOOST_CLASS_REQUIRE(const_reverse_chain_iterator,BTK::CONCEPTS,ChainIteratorConcept);

  //\group Type definitions required by the AtomIterableConcept.
  //@{
  typedef typename chain_type::atom_type atom_type;

  typedef typename BTK::UTILITY::
  GroupedElementIterator<chain_iterator,
			 typename chain_type::atom_iterator,
			 chain_type,
			 atom_type> atom_iterator;
  
  typedef BTK::UTILITY::
  GroupedElementIterator<const_chain_iterator,
			 typename chain_type::const_atom_iterator,
			 chain_type const,
			 atom_type const> const_atom_iterator;

  typedef boost::reverse_iterator<atom_iterator> reverse_atom_iterator;
  typedef boost::reverse_iterator<const_atom_iterator> const_reverse_atom_iterator;
  //@}

  //! Create an empty System with a given instance of a ChemicalTypeSystem.
  System(chemical_type_system const & cts,
         id_type type = id_type()) :
    cto_type(cts,type), btk_seq_type() {}

  //! \group Constructors required by the STL Sequence concept.
  //@{
  System(size_type n = 0, 
         const_reference t = chain_type(),
         id_type type = id_type()):
    cto_type(t.get_chemical_type_system(),type), btk_seq_type(n,t) {}
  
  template <typename ChainIterator>
  System(ChainIterator i, ChainIterator j,
         id_type type = id_type()) :
    cto_type(i->get_chemical_type_system(),type), btk_seq_type(i,j)
  {
    boost::function_requires<BTK::CONCEPTS::
      ChainIteratorConcept<ChainIterator> >();

    // The chains in the range passed to this constructor must be convertible
    // to the chain_type of the class!  If you get a compile error here,
    // it probably means that you're trying to construct some kind of 
    // System using a chain type that is incompatible with the 
    // chain type of the class (e.g. a different atom or monomer type). 
    typedef typename std::iterator_traits<ChainIterator>::value_type c_t;
    boost::function_requires<boost::ConvertibleConcept<c_t,chain_type> >();
  }

  System(self_type const & src) :
    cto_type(src), btk_seq_type(src) {}
  //@}

  virtual ~System() {}
  
  ///\group Methods required for all System objects.
  //@{
  size_type num_chains() const { return btk_seq_type::size(); }

  iterator system_begin() { return btk_seq_type::begin(); }
  const_iterator system_begin() const { return btk_seq_type::begin(); }

  reverse_iterator system_rbegin() { return btk_seq_type::rbegin(); }
  const_reverse_iterator system_rbegin() const { return btk_seq_type::rbegin(); }

  iterator system_end() { return btk_seq_type::end(); }
  const_iterator system_end() const { return btk_seq_type::end(); }

  reverse_iterator system_rend() { return btk_seq_type::rend(); }
  const_reverse_iterator system_rend() const { return btk_seq_type::rend(); }
  
  virtual std::ostream & print(std::ostream & os,
                               size_type first_atom_num = 1) const
  {
    const_chain_iterator ci;
    size_type atom_num = first_atom_num;
    
    for (ci = system_begin(); ci != system_end(); ++ci) {
      ci->print(os,atom_num);
      atom_num += ci->num_atoms();
    }

    return os;
  }
  //@}

  ///\group Methods required by the AtomIterableConcept.
  //@{
  typename chain_type::size_type num_atoms() const
  {    
    typename chain_type::size_type N = 0;
    const_chain_iterator ci;
    
    for (ci = system_begin(); ci != system_end(); ++ci) 
      N += ci->num_atoms();

    return N;
  }
  
  atom_iterator structure_begin()
  {
    return atom_iterator(system_begin(),
                         system_end(),
                         &chain_type::structure_begin,
                         &chain_type::structure_end);
  }

  atom_iterator structure_end()
  {
    return atom_iterator(system_begin(),
                         system_end(),
                         &chain_type::structure_begin,
                         &chain_type::structure_end,
                         true);
  }

  reverse_atom_iterator structure_rbegin()
  {
    return reverse_atom_iterator(structure_end());
  }

  reverse_atom_iterator structure_rend()
  {
    return reverse_atom_iterator(structure_begin());
  }

  const_atom_iterator structure_begin() const
  {
    return const_atom_iterator(system_begin(),
                               system_end(),
                               &chain_type::structure_begin,
                               &chain_type::structure_end);
  }

  const_atom_iterator structure_end() const 
  {
    return const_atom_iterator(system_begin(),
                               system_end(),
                               &chain_type::structure_begin,
                               &chain_type::structure_end,
                               true);
  }

  const_reverse_atom_iterator structure_rbegin() const
  {
    return const_reverse_atom_iterator(structure_end());
  }
  
  const_reverse_atom_iterator structure_rend() const
  {
    return const_reverse_atom_iterator(structure_begin());
  }
  //@}

  using cto_type::type;
  using cto_type::set_type;
  using cto_type::name;
  using cto_type::get_chemical_type_system;

  virtual dictionary const & get_dictionary() const 
  {
    return cto_type::get_chemical_type_system().get_structure_dictionary();
  }
  
  virtual dictionary & get_dictionary() 
  {
    return cto_type::get_chemical_type_system().get_structure_dictionary();
  }

  //! Set the TypeSystem object associated with this System.
  virtual void set_chemical_type_system(chemical_type_system const & cts) 
  { 
    // call base implementation
    cto_type::set_chemical_type_system(cts);

    // set the type systems of every chain
    for (chain_iterator ci = system_begin(); ci != system_end(); ++ci)
      ci->set_chemical_type_system(cts);
  }

  ///\group Methods required for STL Reversible containers.
  //@{
  virtual void swap(self_type & b)
  {
    cto_type::swap(b);
    btk_seq_type::swap(b);
  }

  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    cto_type::operator=(rhs);
    btk_seq_type::operator=(rhs);
    return *this;
  }

  bool operator==(self_type const & rhs) const
  {
    return (cto_type::operator==(rhs) &&
            btk_seq_type::operator==(rhs));
  }

  bool operator!=(self_type const & rhs) const
  {
    return !(operator==(rhs));
  }
  
  bool operator<(self_type const & rhs) const
  {
    if (cto_type::operator<(rhs)) 
      return true;
    else if (cto_type::operator==(rhs))
      return btk_seq_type::operator<(rhs);
    else 
      return false;
  }
  //@}
};

#define IMPORT_BTK_SYSTEM_TYPES(SysType)                                \
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(SysType)                         \
  IMPORT_BTK_CONTAINER_TYPES(SysType)                                   \
                                                                        \
  typedef typename SysType::chain_type chain_type;                      \
  typedef typename SysType::chain_iterator chain_iterator;              \
  typedef typename SysType::const_chain_iterator const_chain_iterator;  \
  typedef typename SysType::reverse_chain_iterator reverse_chain_iterator; \
  typedef typename SysType::const_reverse_chain_iterator                \
  const_reverse_chain_iterator;                                         \
  typedef typename SysType::structure_id_type structure_id_type;        \
  typedef typename SysType::atom_type atom_type;                        \
  typedef typename SysType::atom_iterator atom_iterator;                \
  typedef typename SysType::const_atom_iterator const_atom_iterator;    \
  typedef typename SysType::reverse_atom_iterator reverse_atom_iterator; \
  typedef typename SysType::const_reverse_atom_iterator                 \
  const_reverse_atom_iterator;                                          \
  typedef typename SysType::monomer_type monomer_type;                  \
  typedef typename SysType::monomer_iterator monomer_iterator;          \
  typedef typename SysType::const_monomer_iterator const_monomer_iterator; \
  typedef typename SysType::reverse_monomer_iterator reverse_monomer_iterator; \
  typedef typename SysType::const_reverse_monomer_iterator              \
  const_reverse_monomer_iterator;
  
template <typename CH, typename CTO, typename SS>
std::ostream & operator<<(std::ostream & os, System<CH,CTO,SS> const & s)
{
  return s.print(os);
}

} // namespace MOLECULES
} // namespace BTK
 
#endif
