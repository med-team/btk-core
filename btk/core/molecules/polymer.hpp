// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>,
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file polymer.hpp
/// Definition of the Polymer class.

#ifndef BTK_MOLECULES_POLYMER_HPP
#define BTK_MOLECULES_POLYMER_HPP

#include <list>

#include <boost/operators.hpp>
#include <boost/iterator/reverse_iterator.hpp>

#include <btk/core/utility/grouped_element_iterator.hpp>
#include <btk/core/molecules/polymer_structure.hpp>

namespace BTK {
namespace MOLECULES {

//! A Polymer is a chain of monomers that is also an atomic structure.
//! 
//! This class is a Sequence of objects that adhere to the MonomerConcept, 
//! (just like PolymerStructure), with the addition of methods thast allow 
//! iteration over atoms (as if the container were an AtomicStructure). This 
//! combination of features makes sense biochemically -- every polymer is a 
//! a chain of atoms, conceptually grouped into monomers.
//!
//! More specifically, Polymer implements the ChainConcept and the 
//! AtomicStructureConcept.  Thus, you can iterate over atoms and get/set the 
//! chain ID of a Polymer, but you cannot insert or delete individual atoms from a 
//! Polymer.  As with the PolymerStructure, though, you may insert and delete monomer 
//! objects from a Polymer.
//!
template <typename MonomerType,
          typename ChemicalTypeSystemType =
            typename MonomerType::chemical_type_system,
          typename DictionaryType =
            typename ChemicalTypeSystemType::structure_dictionary,
          typename StorageStrategy = std::list<MonomerType> >
class Polymer :
    public PolymerStructure<MonomerType,
                            ChemicalTypeSystemType,
                            DictionaryType,
                            StorageStrategy>,
    public boost::less_than_comparable<Polymer<MonomerType,
                                               ChemicalTypeSystemType,
                                               DictionaryType,
                                               StorageStrategy> >
{
  typedef PolymerStructure<MonomerType,
                           ChemicalTypeSystemType,
                           DictionaryType,
                           StorageStrategy> base_type;
  typedef Polymer<MonomerType,
                  ChemicalTypeSystemType,
                  DictionaryType,
                  StorageStrategy> self_type;
 public:
  IMPORT_POLYMER_STRUCTURE_TYPES(base_type);

  /// \name Type definitions required by the AtomicStructureConcept.
  ///@{

  /// The atom type (the type of the atom held by the monomer objects).
  typedef typename monomer_type::atom_type atom_type;

  /// THe atom ID type.
  typedef typename atom_type::id_type atom_id_type;

  /// \name Required atom iterator types.
  //@{
  typedef UTILITY::
  GroupedElementIterator<monomer_iterator,
                         typename monomer_type::atom_iterator,
                         monomer_type,
                         atom_type> atom_iterator;

  typedef boost::reverse_iterator<atom_iterator> reverse_atom_iterator;

  typedef UTILITY::
  GroupedElementIterator<const_monomer_iterator,
                         typename monomer_type::const_atom_iterator,
                         monomer_type const,
                         atom_type const> const_atom_iterator;

  typedef boost::reverse_iterator<const_atom_iterator> const_reverse_atom_iterator;
  //@}

  /// Construct a Polymer of a given size.
  /// \param s Size of the new Polymer.
  /// \param m An instance of the MonomerType to copy into the new Polymer.
  /// \param chain_id The chain ID value.
  /// \param t The structure ID value (currently unused).
  Polymer(size_type s = 0,
          monomer_type const & m = monomer_type(),
          char chain_id = ' ',
          structure_id_type t = structure_id_type()) : 
    base_type(s,m,t), _chain_id(chain_id) {}
  
  /// Construct a Polymer from a range of monomer objects.
  /// \param first The start iterator of the monomer range.
  /// \param last The end iterator of the monomer range.
  /// \param chain_id The chain ID value.
  /// \param t The structure ID value (currently unused).
  template <typename MonomerIterator>
  Polymer(MonomerIterator first,
          MonomerIterator last,
          char chain_id = ' ',
          structure_id_type t = structure_id_type()) :
    base_type(first,last,t), _chain_id(chain_id) {}

  Polymer(self_type const & source) :
    base_type(source), _chain_id(source._chain_id) {}

  virtual ~Polymer() {}

  /// \name Methods required by the AtomicStructureConcept.
  //@{

  /// Get an atom_iterator (start of range).
  atom_iterator structure_begin()
  {
    return atom_iterator(base_type::polymer_begin(),
                         base_type::polymer_end(),
                         &monomer_type::monomer_begin,
                         &monomer_type::monomer_end);
  }

  /// Get an atom_iterator (start of range).
  const_atom_iterator structure_begin() const
  {
    return const_atom_iterator(base_type::polymer_begin(),
                               base_type::polymer_end(),
                               &monomer_type::monomer_begin,
                               &monomer_type::monomer_end);
  }

  /// Get a reverse_atom_iterator.
  reverse_atom_iterator structure_rbegin()
  {
    return reverse_atom_iterator(self_type::structure_end());
  }

  /// Get a reverse_atom_iterator.
  const_reverse_atom_iterator structure_rbegin() const
  {
    return const_reverse_atom_iterator(self_type::structure_end());
  }

  /// Get a reverse_atom_iterator (end of range).
  atom_iterator structure_end()
  {
    return atom_iterator(base_type::polymer_begin(),
                         base_type::polymer_end(),
                         &monomer_type::monomer_begin,
                         &monomer_type::monomer_end,
                         true);
  }

  /// Get an atom_iterator (end of range).
  const_atom_iterator structure_end() const
  {
    return const_atom_iterator(base_type::polymer_begin(),
                               base_type::polymer_end(),
                               &monomer_type::monomer_begin,
                               &monomer_type::monomer_end,
                               true);
  }

  /// Get a reverse_atom_iterator (end of range).
  reverse_atom_iterator structure_rend()
  {
    return reverse_atom_iterator(self_type::structure_begin());
  }

  /// Get a reverse_atom_iterator (end of range).
  const_reverse_atom_iterator structure_rend() const
  {
    return const_reverse_atom_iterator(self_type::structure_begin());
  }

  /// Get the size of the Polymer, in atoms.
  /// Runs in linear time with the number of monomers in the Polymer.
  size_type num_atoms() const
  {
    size_type s = 0;
    const_monomer_iterator mi;

    for (mi = self_type::polymer_begin();
         mi != self_type::polymer_end(); ++mi) {
      s += mi->num_atoms();
    }

    return s;
  }
  //@}

  /// Print the atoms in a Polymer to an output stream.
  /// \deprecated This interface will change when the simplified output system
  ///             is completed in future versions of the library.
  virtual std::ostream & print(std::ostream & os,
                               size_type first_atom_num = 1,
                               size_type first_monomer_num = 1) const
  {
    const_monomer_iterator mi;
    unsigned group_num = first_monomer_num;
    unsigned atom_num = first_atom_num;

    for (mi = self_type::polymer_begin();
         mi != self_type::polymer_end(); ++mi) {
      mi->print(os,
                atom_num,
                group_num++,
                _chain_id);

      atom_num += mi->size();
    }

    os << "TER\n";

    return os;
  }

  /// \name Methods required for the ChainConcept.
  //@{
  /// Get the current chain ID value.
  char chain_id() const { return _chain_id; }
  /// Set the current chain ID value.
  void set_chain_id(char chain_id) { _chain_id = chain_id; }
  //@}

  //
  // Bring the BTKSequence methods into the public interface.
  //
  IMPORT_BTK_SEQUENCE_METHODS(base_type);

  using base_type::set_type;
  using base_type::set_chemical_type_system;
  
  virtual dictionary const & get_dictionary() const 
  {
    return base_type::get_chemical_type_system().get_structure_dictionary();
  }

  virtual dictionary & get_dictionary() 
  {
    return base_type::get_chemical_type_system().get_structure_dictionary();
  }

  void swap(self_type & b)
  {
    base_type::swap(b);
    std::swap(_chain_id,b._chain_id);
  }

  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    base_type::operator=(rhs);
    _chain_id = rhs._chain_id;
    return *this;
  }

  bool operator==(self_type const & rhs) const
  {
    return (_chain_id == rhs._chain_id &&
            base_type::operator==(rhs));
  }

  bool operator!=(self_type const & rhs) const
  {
    return !(operator==(rhs));
  }

  bool operator<(self_type const & rhs) const
  {
    return (_chain_id < rhs._chain_id ||
            (_chain_id == rhs._chain_id && base_type::operator<(rhs)));
  }

 private:
  char _chain_id;
};

#define IMPORT_POLYMER_TYPES(PolymerType)                               \
  IMPORT_POLYMER_STRUCTURE_TYPES(PolymerType)                           \
  typedef typename PolymerType::atom_type atom_type;                    \
  typedef typename PolymerType::atom_id_type atom_id_type;

} // namespace MOLECULES
} // namespace BTK

#endif
