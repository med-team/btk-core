// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file polymer_structure.hpp
//! Definition of the PolymerStructure class.

#ifndef BTK_MOLECULES_POLYMER_STRUCTURE_HPP
#define BTK_MOLECULES_POLYMER_STRUCTURE_HPP

#include <ostream>
#include <algorithm>
#include <iterator>

#include <btk/core/utility/btk_sequence.hpp>
#include <btk/core/utility/chemically_typed_object.hpp>
#include <btk/core/concepts/monomer_concept.hpp>
#include <btk/core/concepts/monomer_iterator_concept.hpp>

namespace BTK {
namespace MOLECULES {

template <typename MonomerType,
          typename ChemicalTypeSystemType,
          typename DictionaryType,
          typename StorageStrategy>
class PolymerStructure :
    public BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                               DictionaryType>,
    protected BTK::UTILITY::BTKSequence<MonomerType,StorageStrategy>
{
  typedef BTK::UTILITY::ChemicallyTypedObject<ChemicalTypeSystemType,
                                              DictionaryType> cto_type;
  typedef BTK::UTILITY::BTKSequence<MonomerType,
                                    StorageStrategy> btk_seq_type;
  typedef PolymerStructure<MonomerType,
                           ChemicalTypeSystemType,
                           DictionaryType,
                           StorageStrategy> self_type;

public:
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(cto_type);
  IMPORT_BTK_CONTAINER_TYPES(btk_seq_type);

  typedef value_type monomer_type;
  typedef iterator monomer_iterator;
  typedef reverse_iterator reverse_monomer_iterator;
  typedef const_iterator const_monomer_iterator;
  typedef const_reverse_iterator const_reverse_monomer_iterator;
  typedef id_type structure_id_type;
  typedef typename monomer_type::id_type monomer_id_type;

  BOOST_CLASS_REQUIRE(monomer_type,BTK::CONCEPTS,MonomerConcept);
  BOOST_CLASS_REQUIRE(iterator,BTK::CONCEPTS,MutableMonomerIteratorConcept);
  BOOST_CLASS_REQUIRE(reverse_iterator,BTK::CONCEPTS,MutableMonomerIteratorConcept);
  BOOST_CLASS_REQUIRE(const_iterator,BTK::CONCEPTS,MonomerIteratorConcept);
  BOOST_CLASS_REQUIRE(const_reverse_iterator,BTK::CONCEPTS,MonomerIteratorConcept);

  //
  // Constructors required by STL Reversible Container Concept
  //
  PolymerStructure(self_type const & source) : 
    cto_type(source), btk_seq_type(source) {}
 
  virtual ~PolymerStructure() {}

  //
  // Methods required by STL Reversible Container concept
  //
  IMPORT_BTK_CONTAINER_METHODS(btk_seq_type);

  //
  // Methods required for all Polymer Structures
  //
  size_type num_monomers() const { return btk_seq_type::size(); }

  monomer_iterator polymer_begin() { return btk_seq_type::begin(); }
  const_monomer_iterator polymer_begin() const { return btk_seq_type::begin(); }

  reverse_monomer_iterator polymer_rbegin() { return btk_seq_type::rbegin(); }
  const_reverse_monomer_iterator polymer_rbegin() const
  { return btk_seq_type::rbegin(); }

  monomer_iterator polymer_end() { return btk_seq_type::end(); }
  const_monomer_iterator polymer_end() const { return btk_seq_type::end(); }

  reverse_monomer_iterator polymer_rend() { return btk_seq_type::rend(); }
  const_reverse_monomer_iterator polymer_rend() const
  { return btk_seq_type::rend(); }
  
  using cto_type::type;
  using cto_type::name;
  using cto_type::get_chemical_type_system;

  virtual void set_chemical_type_system(chemical_type_system const & cts)
  {
    // call base implementation
    cto_type::set_chemical_type_system(cts);

    // set the type systems of every monomer
    for (monomer_iterator mi = polymer_begin(); mi != polymer_end(); ++mi)
      mi->set_chemical_type_system(cts);
  }

  virtual std::ostream & print(std::ostream & os,
                               size_type first_atom_num = 1,
                               size_type first_group_num = 1,
                               char chain_id = ' ') const 
  {
    const_monomer_iterator mi;
    size_type atom_num = first_atom_num;
    size_type group_num = first_group_num;

    for (mi = polymer_begin(); mi != polymer_end(); ++mi) {
      mi->print(os,atom_num,group_num,chain_id);
      atom_num += mi->num_atoms();
      ++group_num;
    }

    return os;
  }

  virtual dictionary const & get_dictionary() const = 0;
  virtual dictionary & get_dictionary() = 0;

protected:
  
  //
  // Constructors useful in derived classes
  //
  PolymerStructure(size_type n = 0, 
                   const_reference t = value_type(),
                   id_type type = id_type()) :
    cto_type(t.get_chemical_type_system(),type), btk_seq_type(n,t) {}
  
  template <typename MonomerIterator>
  PolymerStructure(MonomerIterator i, MonomerIterator j, 
                   id_type type = id_type()) :
    cto_type(i->get_chemical_type_system(),type), btk_seq_type(i,j)
  {
    boost::function_requires<BTK::CONCEPTS::
      MonomerIteratorConcept<MonomerIterator> >();
    
    // The monomers in the range passed to this constructor must be 
    // convertible to the monomer_type of the class!  If you get a compile
    // error here, it probably means that you're trying to construct a
    // PolymerStructure using a monomer type that is incompatible with the 
    // monomer type of the class (a different atom type, for example).
    typedef typename std::iterator_traits<MonomerIterator>::value_type m_t;
    boost::function_requires<boost::ConvertibleConcept<m_t,monomer_type> >();
  }

  using cto_type::set_type;

  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    cto_type::operator=(rhs);
    btk_seq_type::operator=(rhs);
    return *this;
  }

  void swap(self_type & b) 
  {
    cto_type::swap(b);
    btk_seq_type::swap(b);
  }

  bool operator==(self_type const & rhs) const {
    return (cto_type::operator==(rhs) && btk_seq_type::operator==(rhs));
  }

  bool operator!=(self_type const & rhs) const {
    return !(operator==(rhs));
  }

  bool operator<(self_type const & rhs) const {
    if (cto_type::operator<(rhs))
      return true;
    else if (cto_type::operator==(rhs))
      return btk_seq_type::operator<(rhs);
    else
      return false;
  }
};

#define IMPORT_POLYMER_STRUCTURE_TYPES(PStructure)                      \
  IMPORT_CHEMICALLY_TYPED_OBJECT_TYPES(PStructure)                      \
  IMPORT_BTK_CONTAINER_TYPES(PStructure)                                \
                                                                        \
  typedef typename PStructure::monomer_type monomer_type;               \
  typedef typename PStructure::monomer_iterator monomer_iterator;       \
  typedef typename PStructure::const_monomer_iterator                   \
  const_monomer_iterator;                                               \
  typedef typename PStructure::reverse_monomer_iterator                 \
  reverse_monomer_iterator;                                             \
  typedef typename PStructure::const_reverse_monomer_iterator           \
  const_reverse_monomer_iterator;                                       \
  typedef typename PStructure::structure_id_type structure_id_type;     \
  typedef typename PStructure::monomer_id_type monomer_id_type;
  
template <typename MT, typename CTO, typename DICT, typename SS>
std::ostream & operator<<(std::ostream & os, PolymerStructure<MT,CTO,DICT,SS> const & p)
{
  return p.print(os);
}

} // namespace MOLECULES
} // namespace BTK

#endif
