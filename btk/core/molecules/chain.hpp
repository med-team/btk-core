// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file chain.hpp
/// Definition of the Chain class.

#ifndef BTK_MOLECULES_CHAIN_HPP
#define BTK_MOLECULES_CHAIN_HPP

#include <btk/core/molecules/atomic_structure.hpp>

namespace BTK {
namespace MOLECULES {

template <typename AtomType,
          typename ChemicalTypeSystemType,
          typename DictionaryType,
	  typename StorageStrategy = std::vector<AtomType> >
class Chain : 
    public AtomicStructure<AtomType,
                           ChemicalTypeSystemType,
                           DictionaryType,
                           StorageStrategy>
{
  typedef Chain<AtomType,
                ChemicalTypeSystemType,
                DictionaryType,
                StorageStrategy> self_type;
  typedef AtomicStructure<AtomType,
                          ChemicalTypeSystemType,
                          DictionaryType,
                          StorageStrategy> base_type;
public:
  IMPORT_ATOMIC_STRUCTURE_TYPES(base_type);

  Chain(self_type const & source) : 
    base_type(source), _chain_id(source._chain_id) {}

  virtual ~Chain() {}

  //! Print a chain. 
  virtual std::ostream & print(std::ostream & os,
			       size_type first_atom_num = 1,
			       size_type group_num = 1) const 
  {
    return base_type::print(os,first_atom_num,group_num,_chain_id);
  }

  //! Get the chain id.
  char chain_id() const { return _chain_id; }
  //! Set the chain id.
  void set_chain_id(char ch_id) { _chain_id = ch_id; }

protected:
  
  //! Constructors that are not in the Chain concept, but 
  //! will be useful in derived class types.
  //@{
  Chain(size_type n = 0, 
	const_reference t = atom_type(),
        id_type type = id_type(),
	char chain_id = ' ') : 
  base_type(n,t,type), _chain_id(chain_id) {}
  
  template <typename AtomIterator>
  Chain(AtomIterator i, AtomIterator j, id_type type, char chain_id) :
    base_type(i,j,type), _chain_id(chain_id) {}
  //@}

  //! Swap the contents of two Chains.
  //! Explicitly protected to prevent heterogenous type swapping.
  void swap(self_type & b) 
  {
    base_type::swap(b);
    std::swap(_chain_id,b._chain_id);
  }

  //! Assignment operator.
  //! Explicitly protected to prevent heterogenous type assignments.
  self_type const & operator=(self_type const & rhs)
  {
    if (this == &rhs) return *this;
    base_type::operator=(rhs);
    _chain_id = rhs._chain_id;
    return *this;
  }

  //! Equality comparison.
  //! Two Chain objects are equivalent iff their chain ids are equal,
  //! and their contained atoms are all equivalent.
  //!
  //! Explicitly protected to prevent heterogenous type comparisons.
  //@{
  bool operator==(self_type const & rhs) const 
  {
    return (_chain_id == rhs._chain_id && base_type::operator==(rhs));
  }

  bool operator!=(self_type const & rhs) const
  {
    return !(operator==(rhs));
  }
  //@}

  //! Less-than comparison.
  //! Chain A is less than Chain B if the chain id of A is less than
  //! the chain id of B, or if the chain ids are equivalent, and 
  //! static_cast<AtomicStructure>(A) < static_cast<AtomicStructure>(B) 
  //! is a true statement.
  //!
  //! Explicitly protected to prevent heterogenous type comparisons.
  bool operator<(self_type const & rhs) const 
  {
    if (_chain_id < rhs._chain_id) 
      return true;
    else if (_chain_id == rhs._chain_id) 
      return base_type::operator<(rhs);
    else
      return false;
  }

private:
  char _chain_id;
};

} // namespace MOLECULES
} // namespace BTK

#endif
