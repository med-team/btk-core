// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_MOLECULES_SYSTEM_BASE_HPP
#define BTK_MOLECULES_SYSTEM_BASE_HPP

#include <boost/iterator/reverse_iterator.hpp>

#include <btk/core/utility/grouped_element_iterator.hpp>

namespace BTK {
namespace MOLECULES {
namespace detail {

typedef struct { char c[1]; } yes;
typedef struct { char c[2]; } no;

template <typename T>
no is_monomer_iterable(...);

template <typename T>
yes is_monomer_iterable(typename T::monomer_type const volatile *,
                        typename T::size_type const volatile *,
                        typename T::monomer_iterator const volatile *,
                        typename T::reverse_monomer_iterator const volatile *);

template <typename T>
struct IsMonomerIterable
{
  enum { value = (sizeof(is_monomer_iterable<T>(0,0,0,0)) == sizeof(yes)) };
};

//
// Default PolymerSystemBase -- disabled.
//
template <typename S, // System type
          typename C, // chain type
          typename CI, // chain iterator
          typename CCI, // const chain iterator
          bool has_monomer_iterable_iface>
struct PolymerSystemBase 
{
  // technically speaking, these don't need to be here,
  // but it eliminates the need to create multiple import
  // macros to define them as void.
  typedef void monomer_type;
  typedef void monomer_iterator;
  typedef void const_monomer_iterator;
  typedef void reverse_monomer_iterator;
  typedef void const_reverse_monomer_iterator;
};

//
// PolymerSystemBase -- enabled.
//
template <typename S,
          typename C, 
          typename CI,
          typename CCI>
struct PolymerSystemBase<S,C,CI,CCI,true>
{
private:
  typedef C chain_type;
  typedef CI chain_iterator;
  typedef CCI const_chain_iterator;

public:
  typedef typename chain_type::monomer_type monomer_type;
    
  typedef BTK::UTILITY::
  GroupedElementIterator<chain_iterator,
                         typename chain_type::monomer_iterator,
                         chain_type,
                         monomer_type> monomer_iterator;
  
  typedef BTK::UTILITY::
  GroupedElementIterator<const_chain_iterator,
                         typename chain_type::const_monomer_iterator,
                         chain_type const,
                         monomer_type const> const_monomer_iterator;
  
  typedef boost::reverse_iterator<monomer_iterator> reverse_monomer_iterator;
  typedef boost::
  reverse_iterator<const_monomer_iterator> const_reverse_monomer_iterator;


  typename chain_type::size_type
  num_monomers() const 
  {
    typename chain_type::size_type N = 0;
    const_chain_iterator ci, c_end;

    ci = static_cast<S const *>(this)->system_begin();
    c_end = static_cast<S const *>(this)->system_end();

    while (ci != c_end) {
      N += ci->num_monomers();
      ++ci;
    }
    
    return N;
  }

  
  monomer_iterator polymer_begin()
  {
    S & sys = static_cast<S&>(*this);

    return monomer_iterator(sys.system_begin(),
			    sys.system_end(),
			    &chain_type::polymer_begin,
			    &chain_type::polymer_end);
  }

  monomer_iterator polymer_end() 
  {
    S & sys = static_cast<S&>(*this);

    return monomer_iterator(sys.system_begin(),
			    sys.system_end(),
			    &chain_type::polymer_begin,
			    &chain_type::polymer_end,
			    true);
  }

  reverse_monomer_iterator polymer_rbegin() 
  {
    return reverse_monomer_iterator(polymer_end());
  }

  reverse_monomer_iterator polymer_rend()
  {
    return reverse_monomer_iterator(polymer_begin());
  }

  const_monomer_iterator polymer_begin() const 
  {
    S const & sys = static_cast<S const &>(*this);

    return const_monomer_iterator(sys.system_begin(),
				  sys.system_end(),
				  &chain_type::polymer_begin,
				  &chain_type::polymer_end);
  }
  
  const_monomer_iterator polymer_end() const
  {
    S const & sys = static_cast<S const &>(*this);

    return const_monomer_iterator(sys.system_begin(),
				  sys.system_end(),
				  &chain_type::polymer_begin,
				  &chain_type::polymer_end,
				  true);
  }
  
  const_reverse_monomer_iterator polymer_rbegin() const 
  {
    return const_reverse_monomer_iterator(polymer_end());
  }

  const_reverse_monomer_iterator polymer_rend() const 
  {
    return const_reverse_polymer_iterator(polymer_begin());
  }
};

} // detail
} // MOLECULES
} // BTK

#endif 
