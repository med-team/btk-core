// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file element_types.hpp
/// Definition of the ElementType ID class.

#ifndef BTK_ELEMENTS_ELEMENT_TYPES_HPP
#define BTK_ELEMENTS_ELEMENT_TYPES_HPP

#include <btk/core/utility/type_id.hpp>
#include <btk/core/utility/type_id_traits.hpp>

namespace BTK {
namespace ELEMENTS {

/// A pseudo-enum class for representing element types.
///
/// Why use this class instead of an enum or an integer? Type safety.
/// Nonsensical ElementType values can never be defined, and
/// therefore, the compiler can enforce the use of "real" ElementType
/// values.
///
class ElementType :
    public BTK::UTILITY::TypeID<ElementType>
{
  typedef BTK::UTILITY::TypeID<ElementType> base_type;

public:
  ElementType() : base_type(unknown()) {}

  static const ElementType unknown() { return ElementType(-1); }

  static const ElementType H() { return ElementType(0); }
  static const ElementType He() { return ElementType(1); }

  static const ElementType Li() { return ElementType(2); }
  static const ElementType Be() { return ElementType(3); }
  static const ElementType B() { return ElementType(4); }
  static const ElementType C() { return ElementType(5); }
  static const ElementType N() { return ElementType(6); }
  static const ElementType O() { return ElementType(7); }
  static const ElementType F() { return ElementType(8); }
  static const ElementType Ne() { return ElementType(9); }

  static const ElementType Na() { return ElementType(10); }
  static const ElementType Mg() { return ElementType(11); }
  static const ElementType Al() { return ElementType(12); }
  static const ElementType Si() { return ElementType(13); }
  static const ElementType P() { return ElementType(14); }
  static const ElementType S() { return ElementType(15); }
  static const ElementType Cl() { return ElementType(16); }
  static const ElementType Ar() { return ElementType(17); }

  static const ElementType K() { return ElementType(18); }
  static const ElementType Ca() { return ElementType(19); }
  static const ElementType Sc() { return ElementType(20); }
  static const ElementType Ti() { return ElementType(21); }
  static const ElementType V() { return ElementType(22); }
  static const ElementType Cr() { return ElementType(23); }
  static const ElementType Mn() { return ElementType(24); }
  static const ElementType Fe() { return ElementType(25); }
  static const ElementType Co() { return ElementType(26); }
  static const ElementType Ni() { return ElementType(27); }
  static const ElementType Cu() { return ElementType(28); }
  static const ElementType Zn() { return ElementType(29); }
  static const ElementType Ga() { return ElementType(30); }
  static const ElementType Ge() { return ElementType(31); }
  static const ElementType As() { return ElementType(32); }
  static const ElementType Se() { return ElementType(33); }
  static const ElementType Br() { return ElementType(34); }
  static const ElementType Kr() { return ElementType(35); }
  static const ElementType Rb() { return ElementType(36); }
  static const ElementType Sr() { return ElementType(37); }
  static const ElementType Y() { return ElementType(38); }
  static const ElementType Zr() { return ElementType(39); }
  static const ElementType Nb() { return ElementType(40); }
  static const ElementType Mo() { return ElementType(41); }
  static const ElementType Tc() { return ElementType(42); }
  static const ElementType Ru() { return ElementType(43); }
  static const ElementType Rh() { return ElementType(44); }
  static const ElementType Pd() { return ElementType(45); }
  static const ElementType Ag() { return ElementType(46); }
  static const ElementType Cd() { return ElementType(47); }
  static const ElementType In() { return ElementType(48); }
  static const ElementType Sn() { return ElementType(49); }
  static const ElementType Sb() { return ElementType(50); }
  static const ElementType Te() { return ElementType(51); }
  static const ElementType I() { return ElementType(52); }
  static const ElementType Xe() { return ElementType(53); }

  static const ElementType Cs() { return ElementType(54); }
  static const ElementType Ba() { return ElementType(55); }
  static const ElementType La() { return ElementType(56); }

  static const ElementType Ce() { return ElementType(57); }
  static const ElementType Pr() { return ElementType(58); }
  static const ElementType Nd() { return ElementType(59); }
  static const ElementType Pm() { return ElementType(60); }
  static const ElementType Sm() { return ElementType(61); }
  static const ElementType Eu() { return ElementType(62); }
  static const ElementType Gd() { return ElementType(63); }
  static const ElementType Tb() { return ElementType(64); }
  static const ElementType Dy() { return ElementType(65); }
  static const ElementType Ho() { return ElementType(66); }
  static const ElementType Er() { return ElementType(67); }
  static const ElementType Tm() { return ElementType(68); }
  static const ElementType Yb() { return ElementType(69); }
  static const ElementType Lu() { return ElementType(70); }

  static const ElementType Hf() { return ElementType(71); }
  static const ElementType Ta() { return ElementType(72); }
  static const ElementType W() { return ElementType(73); }
  static const ElementType Re() { return ElementType(74); }
  static const ElementType Os() { return ElementType(75); }
  static const ElementType Ir() { return ElementType(76); }
  static const ElementType Pt() { return ElementType(77); }
  static const ElementType Au() { return ElementType(78); }
  static const ElementType Hg() { return ElementType(79); }
  static const ElementType Tl() { return ElementType(80); }
  static const ElementType Pb() { return ElementType(81); }
  static const ElementType Bi() { return ElementType(82); }
  static const ElementType Po() { return ElementType(83); }
  static const ElementType At() { return ElementType(84); }
  static const ElementType Rn() { return ElementType(85); }

  static const ElementType Fr() { return ElementType(86); }
  static const ElementType Ra() { return ElementType(87); }
  static const ElementType Ac() { return ElementType(88); }

  static const ElementType Th() { return ElementType(89); }
  static const ElementType Pa() { return ElementType(90); }
  static const ElementType U() { return ElementType(91); }
  static const ElementType Np() { return ElementType(92); }
  static const ElementType Pu() { return ElementType(93); }
  static const ElementType Am() { return ElementType(94); }
  static const ElementType Cm() { return ElementType(95); }
  static const ElementType Bk() { return ElementType(96); }
  static const ElementType Cf() { return ElementType(97); }
  static const ElementType Es() { return ElementType(98); }
  static const ElementType Fm() { return ElementType(99); }
  static const ElementType Md() { return ElementType(100); }
  static const ElementType No() { return ElementType(101); }
  static const ElementType Lr() { return ElementType(102); }

  static const ElementType Rf() { return ElementType(103); }
  static const ElementType Db() { return ElementType(104); }
  static const ElementType Sg() { return ElementType(105); }
  static const ElementType Bh() { return ElementType(106); }
  static const ElementType Hs() { return ElementType(107); }
  static const ElementType Mt() { return ElementType(108); }
  static const ElementType Ds() { return ElementType(109); }
  static const ElementType Rg() { return ElementType(110); }

private:
  ElementType(int t) : base_type(t) {}
};

} // namespace ELEMENTS

namespace UTILITY {

/// \brief A TypeIDTraits specialization for the 
///        BTK::ELEMENTS::ElementType type ID class.
template <>
struct TypeIDTraits<BTK::ELEMENTS::ElementType>
{
  static const bool dynamic = false;

  static BTK::ELEMENTS::ElementType first() 
  { return BTK::ELEMENTS::ElementType::H(); }
  static BTK::ELEMENTS::ElementType last()
  { return BTK::ELEMENTS::ElementType::Rg(); }
  static BTK::ELEMENTS::ElementType unknown()
  { return BTK::ELEMENTS::ElementType::unknown(); }

  static BTK::ELEMENTS::ElementType next(BTK::ELEMENTS::ElementType id)
  { return unknown(); }
};

} // namespace UTILITY 
} // namespace BTK

#endif
