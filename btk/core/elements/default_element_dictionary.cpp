// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <btk/core/elements/default_element_dictionary.hpp>

BTK::ELEMENTS::DefaultElementDictionary::
DefaultElementDictionary() : base_type(false) // not case-sensitive
{
#define ELEMENT(sym)                                                 \
  base_type::insert(value_type(ElementType::sym(),#sym))

  ELEMENT(H);
  ELEMENT(He);
  
  ELEMENT(Li);
  ELEMENT(Be);
  ELEMENT(B);
  ELEMENT(C);
  ELEMENT(N);
  ELEMENT(O);
  ELEMENT(F);
  ELEMENT(Ne);
    
  ELEMENT(Na);
  ELEMENT(Mg);
  ELEMENT(Al);
  ELEMENT(Si);
  ELEMENT(P);
  ELEMENT(S);
  ELEMENT(Cl);
  ELEMENT(Ar);
    
  ELEMENT(K);
  ELEMENT(Ca);
  ELEMENT(Sc);
  ELEMENT(Ti);
  ELEMENT(V);
  ELEMENT(Cr);
  ELEMENT(Mn);
  ELEMENT(Fe);
  ELEMENT(Co);
  ELEMENT(Ni);
  ELEMENT(Cu);
  ELEMENT(Zn);
  ELEMENT(Ga);
  ELEMENT(Ge);
  ELEMENT(As);
  ELEMENT(Se);
  ELEMENT(Br);
  ELEMENT(Kr);
    
  ELEMENT(Rb);
  ELEMENT(Sr);
  ELEMENT(Y);
  ELEMENT(Zr);
  ELEMENT(Nb);
  ELEMENT(Mo);
  ELEMENT(Tc);
  ELEMENT(Ru);
  ELEMENT(Rh);
  ELEMENT(Pd);
  ELEMENT(Ag);
  ELEMENT(Cd);
  ELEMENT(In);
  ELEMENT(Sn);
  ELEMENT(Sb);
  ELEMENT(Te);
  ELEMENT(I);
  ELEMENT(Xe);
    
  ELEMENT(Cs);
  ELEMENT(Ba);
  ELEMENT(La);
    
  ELEMENT(Ce);
  ELEMENT(Pr);
  ELEMENT(Nd);
  ELEMENT(Pm);
  ELEMENT(Sm);
  ELEMENT(Eu);
  ELEMENT(Gd);
  ELEMENT(Tb);
  ELEMENT(Dy);
  ELEMENT(Ho);
  ELEMENT(Er);
  ELEMENT(Tm);
  ELEMENT(Yb);
  ELEMENT(Lu);
    
  ELEMENT(Hf);
  ELEMENT(Ta);
  ELEMENT(W);
  ELEMENT(Re);
  ELEMENT(Os);
  ELEMENT(Ir);
  ELEMENT(Pt);
  ELEMENT(Au);
  ELEMENT(Hg);
  ELEMENT(Tl);
  ELEMENT(Pb);
  ELEMENT(Bi);
  ELEMENT(Po);
  ELEMENT(At);
  ELEMENT(Rn);
    
  ELEMENT(Fr);
  ELEMENT(Ra);
  ELEMENT(Ac);
    
  ELEMENT(Th);
  ELEMENT(Pa);
  ELEMENT(U); 
  ELEMENT(Np);
  ELEMENT(Pu);
  ELEMENT(Am);
  ELEMENT(Cm);
  ELEMENT(Bk);
  ELEMENT(Cf);
  ELEMENT(Es);
  ELEMENT(Fm);
  ELEMENT(Md);
  ELEMENT(No);
  ELEMENT(Lr);
    
  ELEMENT(Rf);
  ELEMENT(Db);
  ELEMENT(Sg);
  ELEMENT(Bh);
  ELEMENT(Hs);
  ELEMENT(Mt);
  ELEMENT(Ds);
  ELEMENT(Rg);    

#undef ELEMENT
}


