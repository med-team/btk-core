// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_ELEMENTS_DEFAULT_ELEMENT_DICTIONARY_HPP
#define BTK_ELEMENTS_DEFAULT_ELEMENT_DICTIONARY_HPP

#include <btk/core/io/dictionary.hpp>
#include <btk/core/elements/element_types.hpp>

namespace BTK {
namespace ELEMENTS {

class DefaultElementDictionary :
    public BTK::IO::Dictionary<ElementType>
{
public:
  typedef BTK::IO::Dictionary<ElementType> base_type;

  typedef base_type::id_type id_type;
  typedef base_type::value_type value_type;
  typedef base_type::const_iterator const_iterator;

  DefaultElementDictionary();
  DefaultElementDictionary(DefaultElementDictionary const & src) :
    base_type(src) {}

  virtual ~DefaultElementDictionary() {}
};

} // namespace ELEMENTS
} // namespace BTK

#endif
