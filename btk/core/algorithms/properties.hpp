// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file properties.hpp
/// \brief Methods that compute interesting properties of molecules.

#ifndef BTK_ALGORITHMS_PROPERTIES_HPP
#define BTK_ALGORITHMS_PROPERTIES_HPP

#include <iterator>

#include <btk/core/math/btk_vector.hpp>
#include <btk/core/math/btk_matrix.hpp>
#include <btk/core/math/linear_algebra.hpp>

namespace BTK {
namespace ALGORITHMS {

/// \name Molecular geometric properties
/// \brief Methods for computing geometric properties of atomic structures.
///
/// These are iterator-based methods for computing geometric properties
/// of atomic structures, such as principal axes and geometric centers.
/// They will work with any iterator that is a model of the AtomIterator
/// concept.
//@{

/// \brief Compute the geometric center of a range of atoms.
///
/// Calculates the geometric center of atoms in the range
/// [first,last).
template <typename AtomIterator>
BTK::MATH::BTKVector
geometric_center(AtomIterator begin,
                 AtomIterator end)
 {
   typedef typename std::iterator_traits<AtomIterator>::value_type InputT;

   BTK::MATH::BTKVector COM(0,0,0);
   unsigned N = 0;
   for(AtomIterator i=begin;i!=end;++i,++N){ COM += i->position(); }

   return COM/N;
 }

// template <typename AtomIterator>
// void
// principal_axes(AtomIterator begin,
//                AtomIterator end,
//                std::vector<BTK::MATH::EigenState>& axes,
//                BTK::MATH::BTKVector & center)
//  {
//    center = geometric_center(begin,end);

//    BTK::MATH::BTKMatrix covar(3,3);
//    covar.clear();

//    for(AtomIterator i = begin; i != end; ++i){
//      BTK::MATH::BTKVector tmp(i->position() - center);
//      covar += outer_prod(tmp,tmp);
//    }

//    axes = diagonalize_symmetric(covar);
//  }

//@}

} // namespace ALGORITHMS
} // namespace BTK

#endif
