// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Chris Saunders <ctsa@users.sourceforge.net>,
//                    Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file selections.hpp
/// \brief Methods and objects for selecting atoms.

#ifndef BTK_ALGORITHMS_SELECTIONS_HPP
#define BTK_ALGORITHMS_SELECTIONS_HPP

namespace BTK {
namespace ALGORITHMS {

/// \name Selection methods.
//@{

/// \brief Select/deselect all objects in a range.
///
/// Changes the select flag for every object in the range [begin,end) to the
/// specified value (the objects must expose the select(bool) method).
///
/// \param select_value The value to assign to the select flag of every
///                     atom in the starting range.
template <typename Iterator>
void
select(Iterator begin,
       Iterator end,
       bool select_value = true)
{
  for (Iterator i = begin; i != end; ++i)
    i->select(select_value);
}

/// \brief Select/deselect objects in a range that satisfy a predicate.
///
/// Changes the select flag for every object in the range [begin,end)
/// that satisfies the given predicate (the objects must expose the 
/// select(bool) method).
///
/// \param test An object or function satisfying the UnaryPredicate concept.
/// \param select_value The value to assign to the select flag of every object
///                     that satisfies the supplied predicate.
template <typename Iterator, typename UnaryPredicate>
void
select_if(Iterator begin,
          Iterator end,
          UnaryPredicate test,
          bool select_value = true)
{
  for(Iterator i = begin; i != end; ++i) {
    if(test(*i)) i->select(select_value);
  }
}

//@}

} // namespace ALGORITHMS
} // namespace BTK

#endif
