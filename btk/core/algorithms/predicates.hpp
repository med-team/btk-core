// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file predicates.hpp
/// \brief Functions and functors for atom/molecule selection.

#ifndef BTK_ALGORITHMS_PREDICATES_HPP
#define BTK_ALGORITHMS_PREDICATES_HPP

#include <string>

namespace BTK {
namespace ALGORITHMS {

/// \brief Indiscriminate predicate.
/// Always returns true.
struct is_any {
  template <typename T>
  bool operator()(T const & t){
    return true;
  }
};

/// Selection predicate.
/// Returns true iff the target object is selected. 
struct is_selected {
  template <typename T>
  bool operator()(T const & t){
    return t.selected();
  }
};

/// Inverse selection predicate.
/// Returns true iff the target object is not selected.
struct is_not_selected {
  template <typename T>
  bool operator()(T const & t) {
    return !t.selected();
  }
};

/// \brief Name predicate.
/// Returns true iff the target object has a particular name.
/// (For a more efficient option, see the type_is predicate).
struct is_named {
  is_named() {}
  is_named(std::string const & name) :
    _name(name) {}
  
  template <typename T>
  bool operator()(T const & t) {
    return (t.name() == _name);
  }

  template <typename T>
  bool operator()(T const & t, std::string const & s) {
    return (t.name() == s);
  }
  
  std::string _name;
};

/// \brief Type predicate.
/// Returns true iff the target object has a particular type.
/// Because type IDs can be any class, structure or value type
/// in the BTK, this predicate is a template, parameterized on the
/// type ID type of the target object. 
template <typename IDType>
struct type_is {
  type_is() {}
  type_is(IDType t) :
    _t(t) {}

  template <typename T>
  bool operator()(T const & t) {
    return (t.type() == _t);
  }

  template <typename T>
  bool operator()(T const & t, IDType const & id) {
    return (t.type() == id);
  }
  
  IDType _t;
};

/// \brief Chain ID predicate.
/// Returns true iff the target object has a particular chain ID.
/// Chain ID values are always characters in the BTK.
struct chain_id_is {
  chain_id_is() {}
  chain_id_is(char chain_id) : _c(chain_id) {}

  template <typename T>
  bool operator()(T const & t) {
    return (t.chain_id() == _c);
  }

  template <typename T>
  bool operator()(T const & t, char c) {
    return (t.chain_id() == c); 
  }
  
  char _c;
};

/// \brief Number predicate.
/// Returns true iff the target object has a particular number.
template <typename NumType = int>
struct number_is {
  
  number_is() {}
  number_is(NumType t) : _number(t) {}
  
  template <typename T>
  bool operator()(T const & t) {
    return (t.number() == _number);
  }
  
  template <typename T>
  bool operator()(T const & t, NumType const & n)
  {
    return (t.number() == n);
  }
                
  NumType _number;
};

} // namespace ALGORITHMS
} // namespace BTK

#endif


