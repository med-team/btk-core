// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2001-2006, Chris Saunders <ctsa@users.sourceforge.net>,
//                         Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file rmsd.hpp
/// \brief Functions for molecular superposition and RMSD calculations.

#ifndef BTK_ALGORITHMS_RMSD_HPP
#define BTK_ALGORITHMS_RMSD_HPP

#include <cmath>
#include <functional>
#include <algorithm>

#include <btk/core/math/btk_vector.hpp>
#include <btk/core/math/btk_matrix.hpp>
#include <btk/core/math/linear_algebra.hpp>

namespace BTK {
namespace ALGORITHMS {

/// \defgroup SuperpositionMethods Structure superposition and RMSD calculations.
/// \brief Methods for superimposing atomic structures.
///
/// These are iterator-based methods for optimally superimposing atomic
/// structures, computing the RMSD between atomic structures, or both.  For 
/// more information on the theory behind these methods, see this discussion
/// of the \ref rmsd_theory "mathematics of RMSD".
///

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace internal {

// This routine will calculate the eigenvectors corresponding
// to pre-calculated eigenvalues for use in RMSD calculations.
// It is not intended to be a solution to the full eigenvector
// calculation problem for 3D matrices.
//
// I think this will work for any 3D matrix that produces three
// real eigenvalues, but I make no guarantees. For RMSD calculations,
// it will work b/c we know that we are dealing with three orthogonal
// real vectors.  If you can't make that assumption
// in your application, don't use this code!
//
// The code checks for eigenvalues with multiplicity > 1, but the
// method used in this case may not be correct, so be careful (as I've
// already said, this won't be the case for 3D structural data).
//
void fast_solve_3D_eigenvectors(BTK::MATH::BTKMatrix & M,
                                double const evalues[3],
                                BTK::MATH::BTKVector evecs[3]);

// Solve eigenvalues (ev) for a 3x3 matrix M quickly by analytically
// finding roots of the characteristic equation: det(M - evI) = 0.
//
// It is possible that the factorization used here
// is numerically unstable (but it likely doesn't matter for RMSD
// calculations).  At the present time, there are no checks or
// warnings on the quality of the result.
void fast_solve_3D_eigenvalues(BTK::MATH::BTKMatrix const & M,
                               double e_vals[3]);


// Solves the determinant for a 3x3 matrix -- not general!
inline double determinant(BTK::MATH::BTKMatrix const & M)
{
  using boost::numeric::ublas::inner_prod;
  using BTK::MATH::cross;
  return inner_prod(row(M,0),cross(row(M,1),row(M,2)));
}

template <typename AtomIterator1, typename AtomIterator2>
 void
 setup_rmsd(AtomIterator1 first1,
            AtomIterator1 last1,
            AtomIterator2 first2,
            AtomIterator2 last2,
            BTK::MATH::BTKVector & T1,
            BTK::MATH::BTKVector & T2,
            BTK::MATH::BTKMatrix & R,
            BTK::MATH::BTKMatrix & RtR,
            double & Eo,
            unsigned & N,
            double & omega,
            double e_vals[3])
{
  // Get T1, the transform from the moved set COM to the origin
  //   & T2, the transform from the origin to the fixed set COM
  T1 = BTK::MATH::BTKVector(0.0);
  T2 = BTK::MATH::BTKVector(0.0);
  AtomIterator1 i1(first1);
  AtomIterator2 i2(first2);

  for (N = 0;
       i1 != last1 && i2 != last2;
       ++i1,++i2) {
    T1 += i2->position();
    T2 += i1->position();
    N++;
  }

  if (i2 != last2 || i1 != last1) {
    std::cerr << "Warning: attempting to compute RMSD between "
              << "structures of different lengths! " << std::endl
              << "Assuming atom length of smaller structure (" << N << ")...."
              << std::endl;
  }

  T1 /= -1.*static_cast<double>(N);
  T2 /= static_cast<double>(N);

  //
  // Get Eo and R
  //
  int i = 0;
  BTK::MATH::BTKMatrix tmp1(3,N),tmp2(3,N);
  for (i1 = first1, i2 = first2;
       i1 != last1 && i2 != last2;
       ++i1,++i2) {
    column(tmp1,i) = i1->position() - T2;
    column(tmp2,i) = i2->position() + T1;
    i++;
  }

  Eo = 0;
  for (unsigned j = 0; j < 3; ++j) {
    Eo += inner_prod(row(tmp1,j),row(tmp1,j)) +
      inner_prod(row(tmp2,j),row(tmp2,j));
    for (unsigned k = 0; k < 3; ++k) {
      R(j,k) = inner_prod(row(tmp1,j),row(tmp2,k));
    }
  }
  Eo /= 2.0;

  // if det(R) < 0, the rotation is degenerate (a flip).
  if (determinant(R) > 0) omega = 1;
  else omega = -1;

  RtR = prec_prod(trans(R),R);

  internal::fast_solve_3D_eigenvalues(RtR,e_vals);

  // The eigenvalues of R (what we really want) are equal to the square root
  // of the eigenvalues of RtR. The fast eigenvalue routine is somewhat 
  // unstable, and the if statements correct for small-magnitude negative
  // values that will occasionally come out of the routine when the true
  // eigenvalue is zero (b/c RtR is a positive definite symmetric real matrix
  // we know in advance that its eigenvalues will always be real and posiive, 
  // so this correction is always mathematically valid.)
  for (unsigned i = 0; i < 3; ++i) {
    if (e_vals[i] > 0) e_vals[i] = std::sqrt(e_vals[i]);
    else e_vals[i] = 0.0;
  }

  std::sort(e_vals,e_vals + 3,
            std::greater<double>());
}

} // namespace internal
#endif // DOXYGEN_SHOULD_SKIP_THIS

/// \brief Calculate the transformation resulting in the least-squares
/// superposition of two atom sets.
///
/// \ingroup SuperpositionMethods
///
/// Calculates the least-squares superposition of two sets of atoms
/// using the method of Kabsch (Acta Cryst., 1978. A34, 827-828).
///
/// The transformation of the second atom set to the superimposed
/// position can be recreated from the rotation matrix \f$U\f$ and the
/// translation vector \f$T\f$ by \f$ x'_i = U x_i + T \f$
///
/// \param U After return, contains the superposition rotation matrix.
/// \param T After return, contains superposition translation vector.
/// \param rmsd After return, contains the RMSD of the superimposed sets.
///
template <typename AtomIterator1, typename AtomIterator2>
double
leastsquares_superposition(AtomIterator1 fixed_first,
                           AtomIterator1 fixed_last,
                           AtomIterator2 superimpose_first,
                           AtomIterator2 superimpose_last,
                           BTK::MATH::BTKMatrix & U,
                           BTK::MATH::BTKVector & T)
 {
   double Eo, omega;
   double e_vals[3];
   BTK::MATH::BTKMatrix R,RtR;
   BTK::MATH::BTKVector T1;
   BTK::MATH::BTKVector T2;
   unsigned N;

   internal::setup_rmsd(fixed_first,
                        fixed_last,
                        superimpose_first,
                        superimpose_last,
                        T1,T2,R,RtR,Eo,N,
                        omega,e_vals);

   BTK::MATH::BTKVector right_evecs[3];

   // In order to create the rotation matrix, we need the
   // eigenvectors of the RtR matrix. To do this, we need
   // the eigenvalues of this matrix.  These are the
   // squares of the eigenvalues of R.
   e_vals[0] *= e_vals[0];
   e_vals[1] *= e_vals[1];
   e_vals[2] *= e_vals[2];

   internal::fast_solve_3D_eigenvectors(RtR,e_vals,right_evecs);

   // Useful algorithm information:
   // The eigenvectors of RtR are equivalent to the right eigenvectors of
   // matrix R. We need the left eigenvectors of R, which are determined by
   // Left = R * Right.
   BTK::MATH::BTKVector left_evecs[3];

   left_evecs[0] = prec_prod(R,right_evecs[0]);
   left_evecs[1] = prec_prod(R,right_evecs[1]);

   // normalize the vectors
   left_evecs[0] /= norm_2(left_evecs[0]);
   left_evecs[1] /= norm_2(left_evecs[1]);

   // in order to guarantee a right-handed system, we'll
   // determine the Z vector as the cross of X and Y.
   left_evecs[2] = cross(left_evecs[0],left_evecs[1]);

   // Construct the optimal rotation matrix.
   //
   // More algorithm details:
   // The optimal rotation matrix is the product of matrices
   // containing the left and right eigenvectors of RtR.
   //
   // Said another way: if  RtR = BSV
   //  (this is the result of singular value decomposition on RtR)
   // the optimal rotation is U = B * t(V)
   //
   U.clear();
   for (unsigned i = 0; i < 3; ++i) {
     for (unsigned j = 0; j < 3; ++j) {
       U(i,j) = (left_evecs[0][i]*right_evecs[0][j]) +
                (left_evecs[1][i]*right_evecs[1][j]) +
                (left_evecs[2][i]*right_evecs[2][j]);
     }
   }

   // construct the post-rotation translation vector
   T = prec_prod(U,T1)+T2;


   // compute the rmsd between the aligned structures.
   double rmsd = (2.0/static_cast<double>(N))*
     (Eo - e_vals[0] - e_vals[1] - omega*e_vals[2]);

   double const sqrt_min = 1e-8;
   if(rmsd > sqrt_min) { rmsd = std::sqrt(rmsd); }
   else                { rmsd = 0; }

   return rmsd;
}

/// \brief Applies the least-squares superposition of two atom sets to a
/// third atom set.
///
/// \ingroup SuperpositionMethods
///
/// Applies the least squares transformation of atom set 'superimpose'
/// onto atom set 'fixed' to atom set 'transform'.
///
/// \returns The RMSD between atom sets 'superimpose' and 'fixed'.
/// 
template <typename AtomIterator1, typename AtomIterator2, typename AtomIterator3>
double
leastsquares_superposition(AtomIterator1 fixed_first,
                           AtomIterator1 fixed_last,
                           AtomIterator2 superimpose_first,
                           AtomIterator2 superimpose_last,
                           AtomIterator3 transform_first,
                           AtomIterator3 transform_last)
{
  BTK::MATH::BTKVector T;
  BTK::MATH::BTKMatrix U;
  double rmsd =
    leastsquares_superposition(fixed_first,fixed_last,
                               superimpose_first,superimpose_last,
                               U,T);

  for(AtomIterator3 ai=transform_first; ai != transform_last; ++ai)
    ai->set_position(prec_prod(U,ai->position())+T);

  return rmsd;
}

/// \brief Calculate the superimposed RMSD of two atom sets.
///
/// \ingroup SuperpositionMethods
///
/// This method computes the optimal superposition of all atoms in the range
/// [a1_first,a1_last) to all atoms in the range [a2_first,a2_last),
/// and returns the RMSD value between the superimposed atoms.
/// If the two sets of atoms are of unequal size, the calculation will
/// be performed over the number of atoms in the smaller set.
///
template <typename AtomIterator1, typename AtomIterator2>
double
get_rmsd(AtomIterator1 a1_first,
         AtomIterator1 a1_last,
         AtomIterator2 a2_first,
         AtomIterator2 a2_last)
{
  double Eo, omega;
  double e_vals[3];
  BTK::MATH::BTKMatrix R,RtR;
  unsigned N;
  BTK::MATH::BTKVector T1,T2;

  internal::setup_rmsd(a1_first,a1_last,a2_first,a2_last,
                       T1,T2,R,RtR,Eo,N,omega,e_vals);

  double rmsd = (2.0/static_cast<double>(N))*
    (Eo - e_vals[0] - e_vals[1] - omega*e_vals[2]);

  double const sqrt_min = 1e-8;
  if(rmsd > sqrt_min) { rmsd = std::sqrt(rmsd); }
  else                { rmsd = 0; }

  return rmsd;
}

/// \brief calculate the trivial (non-superimposed) RMSD between two atom sets.
///
/// \ingroup SuperpositionMethods
///
/// Same as get_rmsd(), but sets [a1_first,a1_last) and [a2_first,a2_last) are
/// not superimposed prior to the RMSD calculation.
///
template <typename AtomIterator1, typename AtomIterator2>
double
get_trivial_rmsd(AtomIterator1 a1_first,
                 AtomIterator1 a1_last,
                 AtomIterator2 a2_first,
                 AtomIterator2 a2_last)
{
  double rmsd2 = 0.;
  unsigned N = 0;

  AtomIterator1 a1 = a1_first;
  AtomIterator2 a2 = a2_first;

  for(;a1!=a1_last && a2!=a2_last;++a1,++a2){
    BTK::MATH::BTKVector v(a1->position()-a2->position());
    rmsd2 += inner_prod(v,v);
    N++;
  }

  if(a1!=a1_last || a2!=a2_last) {
    std::cerr << "Warning: attempting to compute RMSD between "
              << "structures of different lengths! " << std::endl
              << "Assuming atom length of smaller structure (" << N << ")...."
              << std::endl;
  }

  if(N) rmsd2 /= static_cast<double>(N);

  double const sqrt_min = 1e-8;
  if(rmsd2 > sqrt_min) { rmsd2 = std::sqrt(rmsd2); }
  else                 { rmsd2 = 0; }

  return rmsd2;
}

} // ALGORITHMS
} // BTK

/**
 \page rmsd_theory The Theory Behind Root Mean Squared Deviation (RMSD)
 
 <em>
 (note: this page was adapted, with permission, from Bosco Ho's 
 discussion of RMSD, originally available at 
 http://bosco.infogami.com/Root_Mean_Square_Deviation)</em>

 If it's your day job to push proteins in silico then you will one day have 
 to calculate the RMSD of a protein. For example, you've just simulated the 
 protein GinormousA for a whole micro-second, but you don't even know if 
 GinormousA is stable. Sure, you could load up a protein viewer and eyeball the 
 stability of the protein throughout the simulation. But eye-balling is just 
 not going to cut it with your boss -- you need to slap a number on it. This is 
 where RMSD comes in.  RMSD is a quantitative measurement of difference between
 two atomic structures.

 A molecule's conformation is basically a set of 3-dimensional coordinates. 
 This means that a conformation is a set of \f$N\f$ vectors, \f$x_n\f$, where 
 each \f$x_n\f$ has three components. As the molecular conformation changes, 
 therefore, the result is a new set of vectors \f$y_n\f$. We can capture the 
 the differences between conformations using these two sets of vectors. 

 To make life easier, we first shift the center of mass of \f$y_n\f$ and 
 \f$x_n\f$ to the origin of the coordinate system (we can always shift them back 
 afterwards). Once "centered," the differences between the vector sets can be 
 captured by the total squared distance between them:

 \f[
 E = \sum_n{|x_n - y_n|^2}
 \f]

 The root mean-square deviation (RMSD) is then:
 
 \f[
 RMSD = \sqrt{\frac{E}{N}}
 \f]

 But hola, you say...this mean-square measure doesn't measure similarity 
 very well! After all, any rotation of the set of \f$y_n\f$ (which doesn't 
 change the internal arrangement of \f$y_n\f$), would distort the RMSD! What we 
 really need, then, is to find the best rotation of \f$y_n\f$ with respect to 
 \f$x_n\f$ before taking the RMSD. To rotate the vectors \f$y_n\f$, we apply a 
 rotation matrix \f$U\f$ to get \f$y'_n = U \cdot y_n\f$. We can then express 
 \f$E\f$ as a function of the rotation \f$U\f$:

 \f[
 E = \sum_n{|x_n - U \cdot y_n|^2}
 \f]

 Thus, to find the RMSD, we must first find the rotation \f$U_{min}\f$ that 
 minimizes \f$E\f$ -- the <em>optimal superposition</em> of the two structures.

 \section matching_vector_sets Matching Sets of Vectors

 The classic papers of Wolfgang Kabsch showed how to minimize \f$E\f$ to get this 
 rotation. The algorithm described in those papers have since become part of the 
 staple diet of protein analysis. As the original proof for the Kabsch equations 
 (using 6 different Lagrange multipliers) is rather tedious, we'll describe a 
 much simpler proof using standard linear algebra.

 First, we expand \f$E\f$ (which is now a function of \f$U\f$):

 \f{eqnarray*}
 E &=& \sum_n{ |x_n|^2 + |y_n|^2 } - 2 \sum_n{ x_n \cdot U y_n }\\
   &=& E_0 - 2 \sum_n{x_n \cdot U y_n}
 \f}

 The first part, \f$E_0\f$, is invariant with respect to a rotation \f$U\f$. The 
 variation resides in the last term, \f$L = \sum_n{x_n \cdot U y_n}\f$. The goal,
 therefore, is to find the matrix \f$U_{min}\f$ that gives the largest possible 
 value of \f$L\f$, from which we obtain:

 \f[
 RMSD = \sqrt{\frac{E_{min}}{N}} = \sqrt{\frac{E_0 - 2 \cdot L_{max}}{N}}
 \f]

 \section maximizing_l Maximizing L

 The trick here is to realize that we can promote the set of vectors \f$x_n\f$ 
 and \f$y_n\f$ into two \f$N \times 3\f$ matrices \f$X\f$ and \f$Y\f$. Then we can 
 write \f$L\f$ as a matrix trace:

 \f[
 L = Tr( X^T U Y )
 \f]

 where \f$X^T\f$ is a \f$3 \times N\f$ matrix and \f$Y\f$ is an \f$N \times 3\f$ matrix. 
 Juggling the matrices inside the trace, we get:

 \f{eqnarray*}
 L &=& Tr( X^T U Y )\\
   &=& Tr( U Y X^T )\\
   &=& Tr( U R )
 \f}

 Now, we still don't know what \f$U\f$ is, but we can study the matrix 
 \f$R = YX^T\f$, which is the \f$3 \times 3\f$ correlation matrix between 
 \f$X\f$ and \f$Y\f$. Furthermore, by invoking the powerful Singular Value 
 Decomposition theorem, we know that we can always write R as:

 \f[
 R = V S W
 \f]

 and obtain the \f$3 \times 3\f$ matrices \f$V\f$ and \f$W\f$ 
 (which are orthogonal), and \f$S\f$ (which is positive diagonal, with 
 elements \f$\sigma_i\f$). We can then substitute these matrices into the 
 expression for \f$L\f$:

 \f{eqnarray*}
 L &=& Tr( U V S W^T )\\ 
   &=& Tr( S W^T U V )\\
   &=& Tr( S T )
 \f}

 Because \f$S\f$ is diagonal, we can get a simple expression for \f$L\f$
 in terms of the \f$\sigma_i\f$:

 \f[
 L = Tr( S T ) = 
 \sigma_1 \cdot T_{11} + \sigma_2 \cdot T_{22} + \sigma_3 \cdot T_{33}
 \f]

 \section getting_the_rotation Determining the Rotation

 Writing L in this form, we can figure out the maximum value of \f$L\f$. The 
 secret sauce is in \f$T\f$. If we look at the definition of \f$T\f$, we can 
 see that it is a <em>product of orthogonal matrices</em>, namely, the matrices 
 \f$U\f$, \f$W^T\f$ and \f$V\f$. Thus, \f$T\f$ must also be orthogonal.

 Given this, the largest possible value for any element of \f$T\f$ is 1, 
 or \f$T_{ij} \le 1\f$. And since \f$L\f$ is a product of terms linear 
 in the diagonal elements of \f$T\f$, the maximum value of \f$L\f$ occurs 
 when the \f$T_{ii} = 1\f$:

 \f[
 L_{max} = Tr( S T_{max} ) = \sigma_1 + \sigma_2 + \sigma_3 = Tr( S )
 \f]

 Plugging this into the equation for RMSD, we get

 \f[
 RMSD = \sqrt{ \frac{E_0 - 2 (\sigma_1 + \sigma_2 + \sigma_3 )}{N} }
 \f]

 From this analysis, we can also determine the rotation that yields the 
 optimal superposition of the vectors. When \f$T_{ii} = 1\f$, then 
 \f$T = I\f$, the identity matrix. This immediately gives us an equation for 
 the optimal rotation \f$U_{min}\f$ in terms of the left and right orthogonal 
 vectors of the matrix R:

 \f[
 T_{max} = W^T U_{min} V = I
 \f]
 \f[
 U_{min} = W V^T
 \f]

\section solving_for_r How to Solve for R

 In the previous section we've shown that RMSD can be expressed in terms of the 
 singular values of \f$R\f$, but we still don't know how to find them!  Never fear.
 Kabsch, who first derived the solution, showed that to solve for \f$\sigma_i\f$,
 we should solve the matrix \f$R^TR\f$, which is a nice symmetric real matrix, 
 and is consequently much easier to solve.  Expanding on the singular value 
 decomposition from the previous section

 \f[
 R^TR = (W^T S^T V^T)(V S W) = W^T S^2 W
 \f]

 we can see that the singular value term of \f$R^TR\f$ is \f$S^2\f$. In other 
 words, the singular values of \f$R^TR\f$ (\f$\lambda_i\f$) are related to the 
 singular values of \f$R\f$ (\f$\sigma_i\f$) by the relationship 
 \f$\lambda_i = \sigma_i^2\f$. And also, the right singular vectors \f$W\f$ 
 are the same for both \f$R\f$ and \f$R^TR\f$.

 Furthermore, since \f$R^TR\f$ is a symmetric real matrix, its eigenvalues 
 are identical to its singular values \f$\lambda_i\f$.  Thus, we can use 
 any standard method (e.g. the Jacobi transformation) to find the eigenvalues
 of \f$R^TR\f$, and by substituting \f$\lambda_i = \sigma_i^2\f$ into the 
 RMSD formula:

 \f[
 RMSD = \sqrt {
    \frac{ E_0 - 2 ( \sqrt{\lambda_1} + \sqrt{\lambda_2} + \sqrt{\lambda_3} )}{N}
 }
 \f]

 \section chirality_sucks The Curse of Chirality

 Unfortunately, there's one more ambiguity that we will have to consider before 
 we get to the Kabsch equation for RMSD. This ambiguity arises from a degeneracy 
 in the resulting \f$U_{min}\f$ from the SVD theorem. You see, \f$U_{min}\f$ is 
 guaranteed to be orthogonal, but for our RMSD calculation, we don't just want 
 orthogonal matrices, we want <em>rotation</em> matrices.  And while orthogonal 
 matrices can have determinants of +1 or -1, "proper" rotation matrices have 
 determinants of +1 (unless they're also <em>reflected</em>, in which case they'll 
 have determinants of -1, and are termed "improper" rotations). 

 When does this happen?  To answer this question, take a look at your hands.
 Assuming that you're a primate, you have two of them, both alike, but reflected
 across the plane of symmetry that runs down the center of your body.  Now, try 
 to superimpose your hands.  Go ahead.  Try.  If you're observant, you'll notice
 that you simply <em>can't</em> superimpose your hands.  Even if you cut them off
 of your arms, the best you can hope to do is to align them so that their outlines
 are coincident -- but the palm of one hand will still be touching the other.  You
 can never perfectly superimpose your hands, because your hands are <em>chiral</em>.

 The same thing happens in molecular systems -- molecules are frequently chiral, 
 and when two molecules are related by chirality, it sometimes happens that their 
 optimal superposition involves a rotation, followed by a reflection.  Thus, the
 "improper" rotation.   Unfortunately, a reflection in \f$U_{min}\f$ will result 
 in an incorrect RMSD value using the stated algorithm, so we need to detect this 
 situation before it becomes a problem.

 To restrict the solutions of \f$U_{min} = WV^T\f$ to proper rotations, we 
 check the determinants of \f$W\f$ and \f$V\f$:
 
 \f[
 \omega = det( V ) \cdot det( W )
 \f]

 If \f$\omega = -1\f$ then there is a reflection in \f$U_{min}\f$, and we must 
 eliminate the reflection. The easiest way to do this is to reflect the smallest 
 principal axis in \f$U_{min}\f$, by reversing the sign of the third (smallest) 
 eignevalue, which yields the final Kabsch formula for RMSD in terms of 
 \f$\lambda_i\f$ and \f$\omega\f$:

 \f[
 RMSD = \sqrt {
    \frac{ E_0 - 2 ( \sqrt{\lambda_1} + \sqrt{\lambda_2} + \omega \sqrt{\lambda_3} )}{N}
 }
 \f]

 \section kabsch_implementations Implementation in the BTK
 
 The BTK implements an optimized version of the Kabsch algorithm in its 
 leastsquares_superposition() methods. These methods solve a system of cubic
 equations to determine the eigenvalues of \f$R^TR\f$, rather than using an
 iterative approach (such as Jacobi).  If only RMSD is desired (i.e. the 
 calculation of a rotation matrix is unecessary), the get_rmsd() method is 
 more efficient.  

 In addition to these methods, a get_trivial_rmsd() function is provided to 
 calculate the RMSD value between two structures without attempting to 
 superimpose their atoms.

**/

#endif


