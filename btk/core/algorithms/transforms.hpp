// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file transforms.hpp
/// \brief Rigid-body geometric transformations for atoms and molecules.

#ifndef BTK_ALGORITHMS_TRANSFORMS_HPP
#define BTK_ALGORITHMS_TRANSFORMS_HPP

#include <btk/core/algorithms/properties.hpp>
#include <btk/core/math/rotation.hpp>

namespace BTK {
namespace ALGORITHMS {

/// \defgroup AtomTransforms Rigid-body molecular transformations.
/// \brief Methods for 3D geometric transformations of atom-based structures.
///
/// These are iterator-based methods for translating, rotating,
/// superimposing and otherwise transforming atomic structures as rigid
/// bodies in 3-dimensions.  They will work with any iterator that is a
/// model of the AtomIterator concept.

/// \brief Translate atoms along vector T.
/// \ingroup AtomTransforms
template <typename AtomIterator>
void
translate(AtomIterator begin,
          AtomIterator end,
          BTK::MATH::BTKVector const & T)
{
  for (;begin!=end;++begin) {
    begin->set_position(begin->position()+T);
  }
}

/// \brief Center a structure.
/// \ingroup AtomTransforms
///
/// Translate the atoms in the range [first,last), such that
/// their geometric center is at the system origin.
template <typename AtomIterator>
void
center(AtomIterator begin,
       AtomIterator end)
{
  BTK::MATH::BTKVector COM(geometric_center(begin,end));

  translate(begin,end,-COM);
}

/// \brief Align the geometric centers of two molecules.
/// \ingroup AtomTransforms
///
/// The atoms in the range [mobile_begin,mobile_end) are translated
/// such that their geometric center is aligned with the geometric
/// center of the atoms in the range [fixed_begin,fixed_end).
template <typename AtomIterator1,typename AtomIterator2>
void
align_centers(AtomIterator1 fixed_begin,
              AtomIterator1 fixed_end,
              AtomIterator2 mobile_begin,
              AtomIterator2 mobile_end)
 {
   BTK::MATH::BTKVector fixedCOM(geometric_center(fixed_begin,fixed_end));
   BTK::MATH::BTKVector COM(geometric_center(mobile_begin,mobile_end));
  translate(mobile_begin,mobile_end,fixedCOM-COM);
 }


/// \brief Rotate atoms about a fixed point.
/// \ingroup AtomTransforms
///
/// This method will rotate the atoms in the range [first,last)
/// using the specified Euler angles about the specified point.
///
/// For Euler angle conventions, see \ref EulerRotation "here".
template <typename AtomIterator>
void
rotate(AtomIterator begin,
       AtomIterator end,
       double phi,
       double theta,
       double psi,
       BTK::MATH::BTKVector const & fixed_point)
{
  using BTK::MATH::create_rotation_matrix;

  BTK::MATH::BTKMatrix R(create_rotation_matrix(phi,theta,psi));
  BTK::MATH::BTKVector T(prec_prod(R,-fixed_point)+fixed_point);

  for(; begin != end; ++begin) {
    begin->set_position(prec_prod(R,begin->position())+T);
  }
}

/// \brief Rotate atoms about their geometric center.
/// \ingroup AtomTransforms
///
/// This method will rotate the atoms in the range [first,last)
/// using the specified Euler angles about their geometric center.
///
/// For Euler angle conventions, see \ref EulerRotation "here".
template <typename AtomIterator>
inline
void
rotate(AtomIterator begin,
       AtomIterator end,
       double phi,
       double theta,
       double psi)
{
  rotate(begin,end,phi,theta,psi,
         geometric_center(begin,end));
}

/// \brief Rotate atoms around an axis or vector, with respect to a point.
/// \ingroup AtomTransforms
///
/// \param fixed_point This point will be subtracted from the position of
///        every atom in the structure prior to rotation, and added to the
///        position of every atom in the structure following rotation.
template <typename AtomIterator>
void
rotate(AtomIterator begin,
       AtomIterator end,
       BTK::MATH::BTKVector const & axis,
       double theta,
       BTK::MATH::BTKVector const & fixed_point)
{
  using BTK::MATH::create_rotation_matrix;
  BTK::MATH::BTKMatrix R(create_rotation_matrix(axis,theta));
  BTK::MATH::BTKVector T(prec_prod(R,-fixed_point)+fixed_point);

  for(; begin != end; ++begin) {
    begin->set_position(prec_prod(R,begin->position())+T);
  }
}

/// \brief Rotate atoms around an axis or vector, with respect to their 
///        geometric center.
/// \ingroup AtomTransforms
///
/// Rotate atoms in the range [first,last) about a specified axis.  The
/// geometric center of these atoms will be subtracted from the position of
/// every atom in the structure prior to rotation, and added to the
/// position of every atom in the structure following rotation.
template <typename AtomIterator>
inline
void
rotate(AtomIterator begin,
       AtomIterator end,
       BTK::MATH::BTKVector const & axis,
       double theta)
{
  rotate(begin,end,axis,theta,
         geometric_center(begin,end));
}

/// \brief Rotate atoms using a rotation matrix.
/// \ingroup AtomTransforms
///
/// Applies rotation matrix rot to every atom in the range [begin,end).
template <typename AtomIterator>
void
rotate(AtomIterator  begin,
       AtomIterator end,
       BTK::MATH::BTKMatrix const & R)
{
  for (; begin != end; ++begin)
    begin->set_position(prec_prod(R,begin->position()));
}

/// \brief Combined translate/rotate.
/// \ingroup AtomTransforms
///
/// Changes every atom position \f$P\f$ in the range [begin,end) to \f$P'\f$
/// such that \f$P' = R \cdot P + T\f$
template <typename AtomIterator>
void
transform(AtomIterator begin,
          AtomIterator end,
          BTK::MATH::BTKMatrix const & R,
          BTK::MATH::BTKVector const & T)
{
  for(;begin!=end;++begin) {
    begin->set_position(prec_prod(R,begin->position())+T);
  }
}

} // namespace ALGORITHMS
} // namespace BTK

#endif 
