// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file rotation.hpp
//! \brief rigid body rotation routines.

#ifndef BTK_MATH_ROTATION_HPP
#define BTK_MATH_ROTATION_HPP

#include <btk/core/math/btk_vector.hpp>
#include <btk/core/math/btk_matrix.hpp>

namespace BTK {
namespace MATH {

/// \defgroup AxisRotation Axis rotation matrices
/// \brief Methods for 3D rotation about an axis or vector.
/// 
/// These methods allow the rotation of objects around a vector
/// or axis in 3D space.  All angles are specified in radians, unless
/// otherwise noted.  

/// \brief Initialize a pre-existing rotation matrix for rotation about an axis.
/// \ingroup AxisRotation
///
/// Given an axis of rotation (expressed as a vector), and an angle of
/// rotation (in radians) about that axis, intialize a rotation matrix.
/// For methods to apply rotations to atoms and molecules, see
/// \ref AtomTransforms
void
initialize_rotation_matrix(BTKVector const & axis,
                           double theta,
                           BTKMatrix & rm);

/// \brief Create a rotation matrix for a rotation about an axis.
/// \ingroup AxisRotation
///
/// Given an axis of rotation (expressed as a vector), and an angle of
/// rotation (in radians) about that axis, intialize a rotation matrix.
/// For methods to apply rotations to atoms and molecules, see
/// \ref AtomTransforms
inline
BTKMatrix
create_rotation_matrix(BTKVector const & axis,
                       double theta)
 {
   BTKMatrix tmp;
   initialize_rotation_matrix(axis,theta,tmp);
   return tmp;
 }

/// \defgroup EulerRotation Euler angle rotation matrices
/// \brief Methods for rigid-body 3D rotation using Euler angles.
///
/// <b>Warning: Euler angles are library-specific!</b>  There are several
/// commonly used conventions for defining Euler rotations, and therefore
/// rotations are not always transferable between software packages.
///  
/// The BTK provides the ability to create a rotation from Euler angles, 
/// assuming the 'X convention':
/// 
/// <ul>
///  <li>phi - first rotation, around the Z axis.</li>
///  <li>theta - second rotation, around the X axis 
///              (in the range \f$[0,\pi]\f$).</li>
///  <li>psi - third rotation, around Z axis.</li>
/// </ul>
///
/// These are the conventions used in Mathematica, in case you're
/// partial to that package.  They may not be the same as whatever other
/// package you're using, however, so beware!


/// \brief Initialize a pre-existing rotation matrix using Euler angles.
/// \ingroup EulerRotation
void
initialize_rotation_matrix(double phi,
                           double theta,
                           double psi,
                           BTKMatrix & rm);

/// \brief Create a new rotation matrix using Euler angles.
/// \ingroup EulerRotation
inline
BTKMatrix
create_rotation_matrix(double phi,
		       double theta,
		       double psi)
{
  BTKMatrix tmp;
  initialize_rotation_matrix(phi,theta,psi,tmp);
  return tmp;
}

} // namespace MATH
} // namespace BTK
 
#endif // BTK_MATH_ROTATION_HPP
