// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//

#include <btk/core/math/rotation.hpp>
#include <btk/core/math/linear_algebra.hpp>

using namespace BTK::MATH;

// Initialize rotation matrix for rotation around an axis
void
BTK::MATH::
initialize_rotation_matrix(BTKVector const & axis,
                           double theta,
                           BTKMatrix & rm)
{
  //  Form of the rotation matrix taken from Graphics Gems (Glassner, 1990)
  BTKVector normalized_axis(axis / uBLAS::norm_2(axis));
  double c = cos(theta);
  double s = sin(theta);
  double t = 1 - c;
  double txy = t * normalized_axis[0] * normalized_axis[1];
  double txz = t * normalized_axis[0] * normalized_axis[2];
  double tyz = t * normalized_axis[1] * normalized_axis[2];
  double sx = s * normalized_axis[0];
  double sy = s * normalized_axis[1];
  double sz = s * normalized_axis[2];
  double tx2 = t * normalized_axis[0] * normalized_axis[0];
  double ty2 = t * normalized_axis[1] * normalized_axis[1];
  double tz2 = t * normalized_axis[2] * normalized_axis[2];

  // row 1
  rm(0,0) = tx2 + c;
  rm(0,1) = txy - sz;
  rm(0,2) = txz + sy;

  // row 2
  rm(1,0) = txy + sz;
  rm(1,1) = ty2 + c;
  rm(1,2) = tyz - sx;

  // row 3
  rm(2,0) = txz - sy;
  rm(2,1) = tyz + sx;
  rm(2,2) = tz2 + c;
}


// Initialize rotation matrix for rotation using Euler angles
void
BTK::MATH::
initialize_rotation_matrix(double phi,
                           double theta,
                           double psi,
                           BTKMatrix & rm)
{
  double cphi = cos(phi), ctheta = cos(theta), cpsi = cos(psi);
  double sphi = sin(phi), stheta = sin(theta), spsi = sin(psi);
  double ctheta_spsi = ctheta * spsi;
  double ctheta_cpsi = ctheta * cpsi;

  // this matrix should be the same as the one in the mathematica
  // documentation.  double-check against that if something seems
  // wrong.

  // row 1
  rm(0,0) = cpsi * cphi - sphi * ctheta_spsi;
  rm(0,1) = cpsi * sphi + cphi * ctheta_spsi;
  rm(0,2) = spsi * stheta;

  // row 2
  rm(1,0) = -spsi * cphi - sphi * ctheta_cpsi;
  rm(1,1) = -spsi * sphi + cphi * ctheta_cpsi;
  rm(1,2) = cpsi * stheta;

  // row 3
  rm(2,0) = stheta * sphi;
  rm(2,1) = -stheta * cphi;
  rm(2,2) = ctheta;
}
