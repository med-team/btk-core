// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2001-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file linear_algebra.hpp
//! \brief vector geometry and linear algebra routines.
//!

#ifndef BTK_MATH_LINEAR_ALGEBRA_HPP
#define BTK_MATH_LINEAR_ALGEBRA_HPP

#include <cmath>
#include <iosfwd>
#include <vector>

#include <boost/numeric/ublas/symmetric.hpp>

#include <btk/core/math/constants.hpp>
#include <btk/core/math/btk_vector.hpp>
#include <btk/core/math/btk_matrix.hpp>
#include <btk/core/math/vector_math.hpp>

namespace uBLAS = boost::numeric::ublas;

namespace BTK {
namespace MATH {

/**
 * @name Vector and Matrix types.
 * The BTK* types are typedefs to appropriate classes
 * in the Boost::ublas linear algebra library.  Using these
 * types in the BTK makes it easier to write/read linear algebra code,
 * and easier to switch the code to other linear algebra systems
 * (should that ever become necessary).
 */
//@{
typedef uBLAS::symmetric_matrix<double> BTKSymmetricMatrix;
//@}

/**
 * See if two vectors are within a squared distance threshold of one another.
 * @param a BTKVector 1
 * @param b BTKVector 2
 * @param r_squared Squared distance threshold.
 * @return true if vector 1 and 2 are within sqrt(r_squared) units of one another.
 */
bool
within_sqr_dist(BTKVector const & a,
                BTKVector const & b,
                double r_squared);

/// calculate the angle formed by v1-v2 amd v3-v2
inline
double
point_angle(BTKVector const & v1,
            BTKVector const & v2,
            BTKVector const & v3){
  return vector_angle(v1-v2,v3-v2);
}


/**
 * Calculate the dihedral angle between the planes a-b-c and
 * d-c-b. This offers a more conventional "four point" access to
 * the dihedral function.
 */
double
point_dihedral(BTKVector const & a,
               BTKVector const & b,
               BTKVector const & c,
               BTKVector const & d);

/// \brief calculate vector position from a dihedral angle
///
/// Calculates the position of a vector from 3 other atoms
/// that define a a dihedral angle with the build vector
///
/// \param len34 distance between v3 and v4
/// \param ang234 angle formed by v2 - v3 and v4 - v3
/// \param dih1234 dihedral angle between v4 - v3 and v1 - v2
///                along the axis formed by v3 - v2
///
/// \return v4, the vector built from vectors v1,v2 and v3
///
BTKVector
set_vector_from_dihedral(BTKVector const & v3,
                         BTKVector const & v2,
                         BTKVector const & v1,
                         double len34,
                         double ang234,
                         double dih1234);

/// \brief Calculate vector position from two vector angles
///
/// Calculates the position of a point, v4, using three other points,
/// v1,v2,v3, given two vector angles (angle(v4-v3,v2-v3) &
/// angle(v4-v3,v1-v3)), and a vector length ( |v4-v3| ), assuming a
/// right-handed relationship between v1-v3,v2-v3 and v4-v3 (see
/// below)
///
/// Submit the vectors in order such that the three vectors v1-v3,
/// v2-v3 and v4-v3, form a right-handed system, ie. so that
/// dot(v4-v3,cross(v1-v3,v2-v3)) is a positive number, if you have a
/// left-handed arrangment, then swap the arguments v2 and v1 (and the
/// corresponding angle arguments ang234 and ang134 )
///
/// \param len34 | v4-v3 |
/// \param ang234 vector angle between v2-v3 and v4-v3
/// \param ang134 vector angle between v1-v3 and v4-v3
///
BTKVector
set_vector_from_two_angles(BTKVector const & v3,
                           BTKVector const & v2,
                           BTKVector const & v1,
                           double len34,
                           double ang234,
                           double ang134);


} // namespace MATH
} // namespace BTK

#endif // BTK_LINEAR_ALGEBRA_H
