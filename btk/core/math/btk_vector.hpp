// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file btk_vector.hpp
//! \brief Declaration of the BTKVector class.

#ifndef BTK_MATH_BTK_VECTOR_HPP
#define BTK_MATH_BTK_VECTOR_HPP

#include <ostream>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <btk/core/math/common_functions.hpp>

namespace uBLAS = boost::numeric::ublas;

namespace BTK {
namespace MATH {

class BTKVector : public uBLAS::vector<double, uBLAS::bounded_array<double,3> >
{
 public:
  typedef uBLAS::vector<double, uBLAS::bounded_array<double,3> > base_vector;

  //! \brief Default constructor.
  BTKVector() : base_vector(3) {}

  //! \brief Construct with a default value.
  BTKVector(double val) : base_vector(3)
  {
    this->operator[](0) = val;
    this->operator[](1) = val;
    this->operator[](2) = val;
  }

  //! \brief Copy constructor.
  BTKVector(BTKVector const & src) : base_vector(src) {}

  //! \brief Coordinate constructor.
  BTKVector(double x, double y, double z) : base_vector(3) {
    this->operator[](0) = x;
    this->operator[](1) = y;
    this->operator[](2) = z;
  }

  //! \brief Construction from a uBLAS vector expression.
  template <class E>
    BTKVector(uBLAS::vector_expression<E> const & e) : base_vector(e) {}

  virtual ~BTKVector() {}

  //! \brief Assignment from a uBLAS vector expression.
  template <class E>
    BTKVector & operator=(uBLAS::vector_expression<E> const & e) {
    base_vector::operator=(e);
    return *this;
  }

  //! Assignment from another BTKVector.
  BTKVector & operator=(BTKVector const & rhs) {
    base_vector::operator=(static_cast<base_vector>(rhs));
    return *this;
  }

  bool operator==(BTKVector const & rhs) const {
    return (BTK::MATH::equivalent(this->operator[](0),rhs[0]) &&
            BTK::MATH::equivalent(this->operator[](1),rhs[1]) &&
            BTK::MATH::equivalent(this->operator[](2),rhs[2]));
  }

  bool operator!=(BTKVector const & rhs) const {
    return !(*this == rhs);
  }

};

} // namespace MATH
} // namespace BTK

#endif // BTK_MATH_BTK_VECTOR_HPP
