// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>,
//                         Chris Saunders <ctsa@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file vector_math.hpp
/// \brief Basic 3D vector manipulations.

#ifndef BTK_MATH_VECTOR_MATH_HPP
#define BTK_MATH_VECTOR_MATH_HPP

#include <cmath>

#include <btk/core/math/btk_vector.hpp>

namespace BTK {
namespace MATH {

/// \group VectorMath 3D Vector Math Routines
//@{

/// \brief Normalize a vector to unit length.
/// This function normalizes the input vector.
inline
BTKVector &
normalize(BTKVector & v)
{
  v /= norm_2(v);
  return v;
}

/// \brief Get the length of a vector (aka, the l2 norm or euclidean norm).
/// Warning: if you're calling this function from a multi-calculation
/// expression, you might want to directly call uBLAS::norm_2()
/// instead, for efficiency reasons.
///
inline double length(BTKVector const & v) { return norm_2(v); }

/// \brief Compute the cross product of two 3D vectors.
inline
BTKVector
cross(BTKVector const & lhs,
      BTKVector const & rhs)
{
  BTKVector temp;

  temp[0] = lhs[1] * rhs[2] - lhs[2] * rhs[1];
  temp[1] = lhs[2] * rhs[0] - lhs[0] * rhs[2];
  temp[2] = lhs[0] * rhs[1] - lhs[1] * rhs[0];

  return temp;
}

/// \brief Project vector a onto vector b.
/// Get the component of vector a that is parallel to vector b.
BTKVector
project(BTKVector const & a,
        BTKVector const & b);

/// \brief Project the component of vector a that is normal to vector b.
/// Get the component of a that is perpendicular to b.
BTKVector
project_normal(BTKVector const & a,
               BTKVector const & b);

/// \brief Calculate the cosine of the angle between two vectors.
inline
double
cosine_vector_angle(BTKVector const & v1,
                    BTKVector const & v2)
{
  return (prec_inner_prod(v1,v2) / (norm_2(v1) * norm_2(v2)));
}

/// \brief Calculate the angle between two vectors.
inline
double
vector_angle(BTKVector const & v1,
             BTKVector const & v2)
{
  return acos(cosine_vector_angle(v1,v2));
}

/**
 * Calculate the dihedral angle formed by three vectors.
 * The dihedral angle is the angle formed between v1 and v3,
 * about vector v2.
 */
double
vector_dihedral(BTKVector const & v1,
                BTKVector const & v2,
                BTKVector const & v3);

//@}

} // namespace MATH
} // namespace BTK

#endif // BTK_VECTOR_MATH_H
