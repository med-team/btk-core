// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file constants.hpp
//! \brief Math constants and related routines.

#ifndef BTK_MATH_CONSTANTS_HPP
#define BTK_MATH_CONSTANTS_HPP

#include <limits>

namespace BTK {
namespace MATH {

/// \group MathConst Mathematical Constants.
//@{
static const double PI = 3.1415926535897323;
static const double RADIANS_PER_DEGREE = PI/180.0;
static const double DEGREES_PER_RADIAN = 180.0/PI;

//! \brief Smallest double-precision value e, where x + e != x.
static const double DOUBLE_EPSILON(std::numeric_limits<double>::epsilon());
static const float FLOAT_EPSILON(std::numeric_limits<float>::epsilon());
//@}

} // namespace MATH
} // namespace BTK

#endif
