// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2004-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file btk_matrix.hpp
//! \brief Declaration of the BTKMatrix class.

#ifndef BTK_MATH_BTK_MATRIX_HPP
#define BTK_MATH_BTK_MATRIX_HPP

#include <ostream>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <btk/core/math/common_functions.hpp>

namespace uBLAS = boost::numeric::ublas;

namespace BTK {
namespace MATH {

class BTKMatrix : public uBLAS::matrix<double>
{
 public:
  typedef uBLAS::matrix<double> base_matrix;

  //! \brief Default constructor.
  BTKMatrix(size_type x = 3, size_type y = 3) : base_matrix(x,y) {}

  //! \brief Construct a matrix with a default value.
  BTKMatrix(size_type x, size_type y, double val) : base_matrix(x,y)
  {
    for (size_type i = 0; i < x; ++i)
      for (size_type j = 0; j < y; ++j)
        this->operator()(i,j) = val;
  }

  //! \brief Copy constructor.
  BTKMatrix(BTKMatrix const & src) : base_matrix(src) {}

  //! \brief Constrution from a uBLAS matrix expression.
  template <class E>
    BTKMatrix(uBLAS::matrix_expression<E> const & e) : base_matrix(e) {}

  virtual ~BTKMatrix() {}

  //! \brief Assignment from a uBLAS matrix expression.
  template <class E>
    BTKMatrix & operator=(uBLAS::matrix_expression<E> const & e) {
    base_matrix::assign(e);
    return *this;
  }

  //! \brief Equality comparison.
  bool operator==(BTKMatrix const & rhs) const {
    base_matrix::size_type x,y;
    for (x = 0; x < 3; ++x) {
      for (y = 0; y < 3; ++y) {
        if (!BTK::MATH::equivalent(this->operator()(x,y),rhs(x,y))) 
          return false;
      }
    }
    return true;
  }

  //! \brief Inequality comparison.
  bool operator!=(BTKMatrix const & rhs) const {
    return !(*this == rhs);
  }
};

} // namespace MATH
} // namespace BTK

#endif // BTK_MATH_BTK_MATRIX_HPP
