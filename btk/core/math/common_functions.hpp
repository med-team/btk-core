// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of'
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file common_functions.hpp
/// \brief Simple, commonly-used math routines.

#ifndef BTK_MATH_COMMON_FUNCTIONS_HPP
#define BTK_MATH_COMMON_FUNCTIONS_HPP

#include <cmath>

#include <btk/core/math/constants.hpp>

namespace BTK {
namespace MATH {

/// \brief Square a value.
template <typename T>
inline T square(T val) { return val * val; }

/// \brief Cube a value.
template <typename T>
inline T cube(T val) { return val * val * val; }

template <typename T>
inline bool equivalent(T x, T y, T precision)
{
  return std::fabs(x - y) <= precision * std::fabs(x);  
}

template <typename T>
bool equivalent(T x, T y);

template <>
inline bool equivalent(float x, float y)
{
  return equivalent(x,y,FLOAT_EPSILON);
}

template <>
inline bool equivalent(double x, double y)
{
  return equivalent(x,y,DOUBLE_EPSILON);
}

inline double deg2rad(double angle_in_degrees) 
{
  return RADIANS_PER_DEGREE*angle_in_degrees;
}

inline double rad2deg(double angle_in_radians)
{
  return DEGREES_PER_RADIAN*angle_in_radians;
}

} // MATH
} // BTK

#endif // BTK_MATH_COMMON_FUNCTIONS_HPP
