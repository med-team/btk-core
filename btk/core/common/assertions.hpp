// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file assertions.hpp
/// Macros and functions for BTK library assertions.

#ifndef BTK_COMMON_ASSERTIONS_HPP
#define BTK_COMMON_ASSERTIONS_HPP

#include <string>
#include <cstdlib>

#include <btk/core/config/btk_config.hpp>
#include <btk/core/io/logging.hpp>

namespace BTK {
namespace COMMON {
namespace ASSERTIONS {

typedef enum {EXIT, CONTINUE, IGNORE} assert_code;

std::string make_assert_message(std::string const & msg,
                                std::string const & file,
                                unsigned line);

typedef assert_code (*assert_handler)(std::string const &,
                                      std::string const &,
                                      unsigned);

assert_code default_assert_handler(std::string const & msg,
                                   std::string const & file,
                                   unsigned line);

extern assert_handler current_assert_handler;

} // namespace ASSERTIONS
} // namespace COMMON
} // namespace BTK

#ifndef BTK_NDEBUG                                                          
# define BTK_ASSERT(expr,str)                                           \
  if (!(expr)) {                                                        \
    using namespace BTK::COMMON::ASSERTIONS;                           \
    static assert_code status = EXIT;                                   \
                                                                        \
    if (status != IGNORE) {                                             \
      status =                                                          \
        current_assert_handler((str),                                   \
                               __FILE__,                                \
                               __LINE__);                               \
      if (status == EXIT) std::exit(-1);                                \
    }                                                                   \
  };

# define BTK_INVARIANT(expr) BTK_ASSERT(expr,"Invariant violated")
#else
# define BTK_ASSERT(expr,str)

# define BTK_INVARIANT(expr)                                                   \
  if (!(expr)) {                                                               \
    BTK::COMMON::LOGGING::error_msg(make_assert_message("Invariant violated", \
                                                         __FILE__,__LINE__));  \
    std::exit(-1);                                                             \
  }
#endif

#endif //BTK_ASSERTIONS_H
