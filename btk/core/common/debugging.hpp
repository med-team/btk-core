// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file debugging.hpp
/// Methods and macros useful for debugging BTK code.

#ifndef BTK_COMMON_DEBUGGING_HPP
#define BTK_COMMON_DEBUGGING_HPP

#include <btk/core/config/btk_config.hpp>
#include <btk/core/io/logger_stream.hpp>

// These using declarations are necessary for older versions
// of g++, which have namespace scoping difficulties when using the
// stream insertion operator.  They should probably be selectively
// enabled only for broken compilers, but for now, I'm lazy.
using BTK::IO::LOGGING::DEBUG;
using BTK::IO::LOGGING::TRACE;

#ifndef BTK_NDEBUG
# define DBG_STREAM(L) BTK::IO::LOGGING::LoggerStream<char>((L))
# define DBG_OUT DBG_STREAM(DEBUG)
#else 
# define DBG_STREAM(L) if (0) BTK::IO::LOGGING::LoggerStream<char>((L))
# define DBG_OUT DBG_STREAM(DEBUG)
#endif

#ifdef BTK_TRACE
# define TRACE_OUT BTK::IO::LOGGING::LoggerStream<char>(TRACE) << ' '
#else
# define TRACE_OUT if (0) BTK::IO::LOGGING::LoggerStream<char>(TRACE)
#endif

#endif // BTK_COMMON_DEBUGGING_HPP
