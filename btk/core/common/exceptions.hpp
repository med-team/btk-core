// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file exceptions.hpp
/// Declarations of exception classes used by the BTK.

#ifndef BTK_COMMON_EXCEPTIONS_HPP
#define BTK_COMMON_EXCEPTIONS_HPP

#include <string>
#include <sstream>
#include <stdexcept>

namespace BTK {
namespace EXCEPTIONS {

struct BTKException
{
  BTKException(std::string const & msg,
               std::string const & file,
	       unsigned line,
	       std::string const & function)
  {
    std::ostringstream mstr;
    mstr << msg << " thrown from "
         << file << ", line " << line;

    if (!function.empty()) 
      mstr << ", fxn = " << function;

    _msg = mstr.str();
  }

  virtual ~BTKException() {}

  virtual char const * what() const throw() 
  { return _msg.c_str(); }

private:
  std::string _msg;
};

// This macro is for convenience in defining the basic BTK exception
// classes, and is undefined outside of this file.  To create exceptions
// that derive from the basic BTK exception classes, see the 
// NEW_BTK_EXCEPTION_TYPE macro, below.
#define BASE_BTK_EXCEPTION_TYPE(exception_name,base)                    \
  struct exception_name : public base, public BTKException		\
  {									\
    exception_name(std::string const & msg = #exception_name,           \
                   std::string const & file = "",                       \
                   unsigned line = 0,                                   \
                   std::string const & func = "") :                     \
      base(msg), BTKException(msg,file,line,func) {}                    \
                                                                        \
    virtual char const * what() const throw()                           \
    { return BTKException::what(); }                                    \
                                                                        \
    virtual ~exception_name() throw() {}                                \
  };

// Exceptions corresponding to the std::logic_error hierarchy
BASE_BTK_EXCEPTION_TYPE(BTKLogicError,std::logic_error);
BASE_BTK_EXCEPTION_TYPE(BTKDomainError,std::domain_error);
BASE_BTK_EXCEPTION_TYPE(BTKInvalidArgument,std::invalid_argument);
BASE_BTK_EXCEPTION_TYPE(BTKLengthError,std::length_error);
BASE_BTK_EXCEPTION_TYPE(BTKOutOfRange,std::out_of_range);

// Exceptions corresponding to the std::runtime_error hierarchy
BASE_BTK_EXCEPTION_TYPE(BTKRuntimeError,std::runtime_error);
BASE_BTK_EXCEPTION_TYPE(BTKRangeError,std::range_error);
BASE_BTK_EXCEPTION_TYPE(BTKUnderflowError,std::underflow_error);
BASE_BTK_EXCEPTION_TYPE(BTKOverflowError,std::overflow_error);

#undef BASE_BTK_EXCEPTION_TYPE

#define NEW_BTK_EXCEPTION_TYPE(exception_name,base_btk_exception)       \
  struct exception_name : public base_btk_exception                     \
  {									\
    explicit exception_name(char const *msg = #exception_name,		\
			    char const *file = "",			\
			    unsigned line = 0,				\
			    char const *func = "") :			\
      base_btk_exception(msg,file,line,func) {}                         \
                                                                        \
    virtual ~exception_name() throw() {}                                \
  }

#define BTK_THROW(btk_exception)			\
  throw BTK::EXCEPTIONS::btk_exception(#btk_exception,__FILE__,__LINE__)

#define BTK_THROW_MSG(btk_exception,msg)	\
  throw BTK::EXCEPTIONS::btk_exception(msg,__FILE__,__LINE__)

} // EXCEPTIONS
} // BTK

#endif 
