// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <iostream>
#include <sstream>
#include <cctype>

#include <btk/core/common/assertions.hpp>

using namespace std;

string 
BTK::COMMON::ASSERTIONS::
make_assert_message(string const & msg,
                    string const & file,
                    unsigned line)
{
  ostringstream assert_msg;
  assert_msg << msg << " at line " << line
	     << "in " << file << endl;
  return assert_msg.str();
}

BTK::COMMON::ASSERTIONS::assert_code
BTK::COMMON::ASSERTIONS::
default_assert_handler(string const & msg,
		       string const & file,
		       unsigned line)
{
  char response;
  using BTK::IO::LOGGING::error_msg;

  error_msg(make_assert_message(msg,file,line));

  do {
    error_msg("C)ontinue, E)xit or I)gnore forever?");
    cin >> response;
    response = toupper(response);
  } while (response != 'C' && response != 'E' && response != 'I');

  switch (response) {
  case 'C': return CONTINUE; break;
  case 'E': return EXIT; break;
  case 'I': return IGNORE; break;
  };

  return EXIT; // not necessary, but it supresses compiler warnings.
};

BTK::COMMON::ASSERTIONS::assert_handler 
BTK::COMMON::ASSERTIONS::
current_assert_handler = default_assert_handler;

