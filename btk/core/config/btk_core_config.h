#ifndef _BTK_CORE_CONFIG_BTK_CORE_CONFIG_H
#define _BTK_CORE_CONFIG_BTK_CORE_CONFIG_H 1
 
/*
btk/core/config/btk_core_config.h.
Generated
automatically
at
end
of
configure.
*/
/* btk/core/config/config.h.  Generated by configure.  */
/* btk/core/config/config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if you have the <inttypes.h> header file. */
#ifndef BTK_CORE_HAVE_INTTYPES_H 
#define BTK_CORE_HAVE_INTTYPES_H  1 
#endif

/* Define to 1 if you have the <memory.h> header file. */
#ifndef BTK_CORE_HAVE_MEMORY_H 
#define BTK_CORE_HAVE_MEMORY_H  1 
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#ifndef BTK_CORE_HAVE_STDINT_H 
#define BTK_CORE_HAVE_STDINT_H  1 
#endif

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef BTK_CORE_HAVE_STDLIB_H 
#define BTK_CORE_HAVE_STDLIB_H  1 
#endif

/* Define to 1 if you have the <strings.h> header file. */
#ifndef BTK_CORE_HAVE_STRINGS_H 
#define BTK_CORE_HAVE_STRINGS_H  1 
#endif

/* Define to 1 if you have the <string.h> header file. */
#ifndef BTK_CORE_HAVE_STRING_H 
#define BTK_CORE_HAVE_STRING_H  1 
#endif

/* Define to 1 if you have the <sys/stat.h> header file. */
#ifndef BTK_CORE_HAVE_SYS_STAT_H 
#define BTK_CORE_HAVE_SYS_STAT_H  1 
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#ifndef BTK_CORE_HAVE_SYS_TYPES_H 
#define BTK_CORE_HAVE_SYS_TYPES_H  1 
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#ifndef BTK_CORE_HAVE_UNISTD_H 
#define BTK_CORE_HAVE_UNISTD_H  1 
#endif

/* The btk_core major version number */
#ifndef BTK_CORE_MAJOR_VERSION 
#define BTK_CORE_MAJOR_VERSION  0 
#endif

/* The btk_core minor version number */
#ifndef BTK_CORE_MINOR_VERSION 
#define BTK_CORE_MINOR_VERSION  8 
#endif

/* Name of package */
#ifndef BTK_CORE_PACKAGE 
#define BTK_CORE_PACKAGE  "btk_core" 
#endif

/* Define to the address where bug reports for this package should be sent. */
#ifndef BTK_CORE_PACKAGE_BUGREPORT 
#define BTK_CORE_PACKAGE_BUGREPORT  "btk-bugs@lists.sf.net" 
#endif

/* Define to the full name of this package. */
#ifndef BTK_CORE_PACKAGE_NAME 
#define BTK_CORE_PACKAGE_NAME  "btk_core" 
#endif

/* Define to the full name and version of this package. */
#ifndef BTK_CORE_PACKAGE_STRING 
#define BTK_CORE_PACKAGE_STRING  "btk_core 0.8.1" 
#endif

/* Define to the one symbol short name of this package. */
#ifndef BTK_CORE_PACKAGE_TARNAME 
#define BTK_CORE_PACKAGE_TARNAME  "btk_core" 
#endif

/* Define to the version of this package. */
#ifndef BTK_CORE_PACKAGE_VERSION 
#define BTK_CORE_PACKAGE_VERSION  "0.8.1" 
#endif

/* Define to 1 if you have the ANSI C header files. */
#ifndef BTK_CORE_STDC_HEADERS 
#define BTK_CORE_STDC_HEADERS  1 
#endif

/* Version number of package */
#ifndef BTK_CORE_VERSION 
#define BTK_CORE_VERSION  "0.8.1" 
#endif

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef _btk_core_const */

/* Define as `__inline' if that's what the C compiler calls it, or to nothing
   if it is not supported. */
/* #undef _btk_core_inline */
 
/* once:
_BTK_CORE_CONFIG_BTK_CORE_CONFIG_H
*/
#endif
