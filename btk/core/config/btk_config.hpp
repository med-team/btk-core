// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

//! \file btk_config.hpp

#ifndef BTK_CORE_BTK_CONFIG_HPP
#define BTK_CORE_BTK_CONFIG_HPP

//! \mainpage The BTK Core Library
//! 
//! \section introduction_to_btk_core Introduction
//!
//! The Biomolecule Toolkit (%BTK) is a collection of libraries and tools 
//! for use in the computational modeling of biological macromolecules, 
//! such as proteins, DNA and RNA.  It provides a modern, flexible, 
//! standards-based C++ interface for common tasks in molecular modelling.
//!
//! This is the "core" %BTK library, representing a set of classes,
//! interfaces and formal concepts that are generally useful for 
//! computational structural biology.  In particular, this library 
//! contains code for the general representation of atoms (BTK::ATOMS),
//! and molecules (BTK::MOLECULES), as well as some essential algorithms
//! (BTK::ALGORITHMS), common mathematical routines (BTK::MATH), 
//! input-output operations (BTK::IO), and generally-useful support
//! classes and functions (BTK::COMMON, BTK::UTILITY).
//!
//! \section btk_core_design_philosophy Design Philosophy
//!
//! The %BTK libraries rely heavily on template-based C++ design,
//! and use many practices formalized by the 
//! <a href="http://www.sgi.com/tech/stl/">
//! Standard Template Library (STL)</a>, the 
//! <a href="http://www.boost.org">boost library</a>, and other modern 
//! C++ libraries.  In particular, the %BTK makes
//! heavy use of iterator-based container classes and algorithms, as 
//! well as contract-based, generic programming techniques 
//! (<a href="http://www.boost.org/more/generic_programming.html">here</a>
//! is a good introduction to a few of these ideas).  As such, the %BTK
//! is as much a collection of programming <i>concepts</i> as a library of C++
//! code (we're in the process of \ref btk_core_concepts 
//! "documenting these concepts.")
//!
//! There are three main principles guiding the design of the library:
//!
//! \subsection p1 1. Flexibility
//! 
//!    Most current molecular modelling code is
//!    application-specific, and difficult or impossible to adapt to 
//!    new problems.  For this reason, the %BTK is intended to be a flexible
//!    architecture on top of which many different applications and algorithms
//!    may be designed.  Modern C++ generic programming techniques, such as
//!    concept-based design, allow us to achieve a great deal of flexibility,
//!    without sacrificing on the second design principle:
//!
//! \subsection p2 2. Efficiency 
//!
//!    Despite decades of improvement in language development
//!    and software engineering practices, most current molecular modelling
//!    code is still written in FORTRAN. The reason?  Efficiency. Of course,
//!    FORTRAN's legendary speed has usually come at the cost of readability, 
//!    flexibility and maintainability.
//!    Again, modern C++ techniques allow careful library designers 
//!    the ability to achieve performance on par with hand-optimized FORTRAN.
//!    We are interested in obtaining the maximum performance possible for a 
//!    flexible C++ library.  Thus, whenever a design change can increase code 
//!    efficiency without sacrificing flexibility, we try to adopt that change.
//!
//! \subsection p3 3. Simplicity
//!
//!    Whenever possible we adopt designs that are simple, as well as flexible
//!    and efficient.  The many syntactic features of C++ give us a 
//!    great deal of power to create simple interfaces that are still very
//!    flexible.
//!

#include "btk_core_config.h"

////////////////////////////////////////////////////////////////////////////////
// General Debugging (goal: set/unset BTK_NDEBUG)
////////////////////////////////////////////////////////////////////////////////

// By default, follow the unix convention: 
// disable all debug code when NDEBUG is defined.
#ifdef NDEBUG
# define BTK_NDEBUG
#else
# undef BTK_NDEBUG
#endif

// Allow the user to explicitly enable BTK debugging
#ifdef BTK_DEBUG
# undef BTK_NDEBUG
#endif

#endif // BTK_CORE_BTK_CONFIG_HPP 
