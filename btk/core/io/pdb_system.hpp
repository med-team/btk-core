// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2005-2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file pdb_system.hpp
/// Declaration and definition of the PDBSystem class.

#ifndef BTK_IO_PDB_SYSTEM_HPP
#define BTK_IO_PDB_SYSTEM_HPP

#include <btk/core/io/pdb_file_parser.hpp>
#include <btk/core/molecules/system.hpp>

namespace BTK {
namespace IO {

template <typename PolymerType,
          typename ChemicalTypeSystemType = 
            typename PolymerType::chemical_type_system,
          typename StorageStrategy = std::list<PolymerType> >
class PDBSystem :
    public BTK::MOLECULES::System<PolymerType,
                                  ChemicalTypeSystemType,
                                  StorageStrategy>
{
  typedef PDBSystem<PolymerType,
                    ChemicalTypeSystemType,
                    StorageStrategy> self_type;

  typedef BTK::MOLECULES::System<PolymerType,
                                 ChemicalTypeSystemType,
                                 StorageStrategy> base_type;
 public:
  IMPORT_BTK_SYSTEM_TYPES(base_type);

  //! \group Constructors to create PDBSystem objects from PDB files.
  //! These constructors are unique to the PDBSystem class, and allow the
  //! creation of a PDBSystem object directly from a PDB file.
  //@{

  //! \brief A model-aware constructor.
  //! Creates a PDBSystem using the specified MODEL from a PDB file.
  //! \param filename Name of PDB file to parse.
  //! \param which_model The number of the MODEL to parse from the PDB file.
  //! \param load_hetatoms If true, HETATM records are parsed as atoms.
  //!                      If false, HETATM records are skipped.
  //! \param dynamic_types If true, unknown atom types/names are added to the
  //!                      specified type system.  If the type system does not
  //!                      support dynamic types, this option has no effect.
  PDBSystem(std::string const & filename,
            int which_model,
            bool load_hetatoms = true,
            bool dynamic_types = true,
            chemical_type_system const & cts = chemical_type_system()) :
    base_type(cts), 
    _file_parser(filename,which_model,cts,dynamic_types)
  {
    _file_parser.get_model(which_model,*this,load_hetatoms);
  }
 
  ///\brief Construct from a PDB file.
  /// Creates a PDBSystem from the specified PDB file.
  /// \param filename Name of PDB file to parse.
  /// \param load_hetatoms If true, HETATM records are parsed as atoms.
  ///                      If false, HETATM records are skipped.
  /// \param dynamic_types If true, unknown atom types/names are added to the
  ///                      specified type system.  If the type system does not
  ///                      support dynamic types, this option has no effect.
  PDBSystem(std::string const & filename,
            bool load_hetatoms = true,
            bool dynamic_types = true,
            chemical_type_system const & cts = chemical_type_system()) :
    base_type(cts), 
    _file_parser(filename,0,cts,dynamic_types)
  {
    _file_parser.get_current_model(*this,load_hetatoms);
  }
  //@}

  ///\group Constructors required by the STL Sequence concept.
  /// Because no PDB file is specified with these constructors, subsequent
  /// calls to get_model() and get_next_model() will fail, unless a call
  /// is made to open().
  //@{

  /// \brief Create a PDBSystem from 0 or more Chain objects.
  PDBSystem(size_type n = 0, 
            const_reference t = chain_type()) :
    base_type(n,t) {}
  
  /// \brief Create a PDBSystem from a range of Chain objects.
  template <typename ChainIterator>
  PDBSystem(ChainIterator begin,
            ChainIterator end) :
    base_type(begin,end) {}
  //@}

  /// \brief Copy a PDBSystem object.
  PDBSystem(PDBSystem const & src) :
    base_type(src), _file_parser(src._file_parser) {}

  /// \brief Destructor.
  virtual ~PDBSystem()
  {
  }

  /// \brief Open a new PDB file.
  /// Any previously loaded structures will be destroyed.
  bool open(std::string filename,
            int which_model = 0)
  {
    return _file_parser.open(filename,which_model);
  }

  /// \brief Close the current PDB file.
  /// Structures in memory are unaffected.
  void close()
  {
    _file_parser.close();
  }
  
  /// \brief Get the current PDB file header.
  std::string const & header() const { return _file_parser.get_header(); }
  
  /// \brief Get the current PDB file name.
  std::string const & filename() const { return _file_parser.get_filename(); }

  /// \brief Get a MODEL from the current PDB file.
  /// Any previously loaded structures/models will be destroyed.
  bool get_model(int which,
                 bool load_hetatoms = true)
  {
    return _file_parser.get_model(which,*this,load_hetatoms);
  }

  /// \brief Get the next model from the current PDB file.
  /// Any previously loaded structures/models will be destroyed.
  bool get_next_model(bool load_hetatoms = true)
  {
    return _file_parser.get_next_model(*this,load_hetatoms);
  }

private:
  PDBFileParser<chemical_type_system> _file_parser;
};

} // namespace IO
} // namespace BTK

#endif
