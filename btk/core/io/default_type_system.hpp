// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file default_type_system.hpp
/// Declaration of the DefaultTypeSystem class.

#ifndef BTK_IO_DEFAULT_TYPE_SYSTEM_HPP
#define BTK_IO_DEFAULT_TYPE_SYSTEM_HPP

#include <btk/core/io/type_system.hpp>
#include <btk/core/io/type_system_proxy.hpp>
#include <btk/core/elements/default_element_dictionary.hpp>
#include <btk/core/atoms/default_atom_dictionary.hpp>
#include <btk/core/molecules/default_monomer_dictionary.hpp>
#include <btk/core/molecules/default_structure_dictionary.hpp>

namespace BTK {
namespace IO {

/// The default TypeSystem class for the BTK.
///
/// This class provides dynamic atom, monomer and structure typing, for 
/// use when the user does not specify a custom TypeSystem for their own
/// code.
///
/// Because element types are constant, the DefaultTypeSystem
/// uses the DefaultElementDictionary and ElementType objects for element
/// type mapping.  Atom, monomer and structure types are dynamic, however,
/// and must be assigned by the user, or by the parser used to load a
/// structure from a file (the PDBFileParser does this automatically).
///
/// In summary, the default dictionary and ID types are:
///
/// <table>
///   <tr>
///     <td>Dictionary Type</td> <td>ID Type</td>
///   </tr>
///   <tr>
///     <td>BTK::ELEMENTS::DefaultElementDictionary</td> 
///     <td>BTK::ELEMENTS::ElementType</td>
///   </tr>
///   <tr>
///     <td>BTK::ATOMS::DefaultAtomDictionary</td> <td>integer</td>
///   </tr>
///   <tr>
///     <td>BTK::MOLECULES::DefaultMonomerDictionary</td> <td>integer</td>
///   </tr>
///   <tr>
///     <td>BTK::MOLECULES::DefaultStructureDictionary</td> <td>integer</td>
///   </tr>
/// </table>
///
/// For efficiency, the DefaultTypeSystem is always stored by proxy,
/// using the TypeSystemProxy class.  This is generally what you want.  
/// However, if you need to make a deep copy of a DefaultTypeSystem, 
/// see the TypeSystemProxy::clone() method.
typedef 
TypeSystemProxy<
  TypeSystem<BTK::ELEMENTS::DefaultElementDictionary,
             BTK::ATOMS::DefaultAtomDictionary,
             BTK::MOLECULES::DefaultMonomerDictionary,
             BTK::MOLECULES::DefaultStructureDictionary> > DefaultTypeSystem;

} // namespace IO
} // namespace BTK

#endif 
