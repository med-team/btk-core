// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file cached_logger.hpp
/// Declaration of the CachedLogger class.

#ifndef BTK_IO_CACHED_LOGGER_HPP
#define BTK_IO_CACHED_LOGGER_HPP

#include <map>
#include <string>

#include <btk/core/io/logger.hpp>

namespace BTK {
namespace IO {
namespace LOGGING {

/// \brief A Logger object that filters duplicate messages.
/// A CachedLogger stores all incoming messages, and writes one
/// copy of each identical message to the previous Logger object
/// in the global stack when the CachedLogger is destroyed, or 
/// when the flush() method is invoked.
class CachedLogger : public LoggerInterface
{
 public:
  CachedLogger(LoggerInterface const & previous_logger) :
  _previous_logger(previous_logger) {}
  
  virtual ~CachedLogger() { flush(); }
  
  /// \brief Write a log message.
  /// This message will be cached and filtered for uniqueness.
  /// For immediate output, use the Logger::error() method.
  virtual void log(std::string const & msg,
		   LoggerLevel level) const;

  virtual void error(std::string const & msg) const
  {
    _previous_logger.error(msg);
  }

  /// \brief Flush the message cache.
  /// Writes every message in the message cache to the 
  /// previous Logger object in the stack.
  void flush() const;

 private:
  LoggerInterface const & _previous_logger;
  mutable std::map<std::string,LoggerLevel> _messages;
};

} // namespace LOGGING
} // namespace IO
} // namespace BTK

#endif 
