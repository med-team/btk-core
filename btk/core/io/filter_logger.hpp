// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_IO_LOGGING_FILTER_LOGGER_HPP
#define BTK_IO_LOGGING_FILTER_LOGGER_HPP

#include <btk/core/io/logger.hpp>

namespace BTK {
namespace IO {
namespace LOGGING {

/// \brief A logger that filters for specific message types.
/// This class examines the LoggerLevel of each incoming message,
/// and passes on only those that match a given LoggerLevel mask.
class FilterLogger : public LoggerInterface
{
public:
  FilterLogger(LoggerLevel accept_mask,
	       LoggerInterface & base_logger) : 
  _mask(accept_mask), _base_logger(base_logger) {}

  virtual void log(std::string const & msg,
		   LoggerLevel msg_level) const
  {
    if (msg_level & _mask) _base_logger.log(msg,msg_level);
  }

  virtual void error(std::string const & msg) const
  {
    _base_logger.error(msg);
  }

private:
  LoggerLevel _mask;
  LoggerInterface & _base_logger;
};

} // LOGGING
} // IO
} // BTK

#endif //BTK_IO_LOGGING_FILTER_LOGGER_HPP 
