// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file logger_level.hpp
/// Declaration of the LoggerLevel class.

#ifndef BTK_IO_LOGGER_LEVEL_HPP
#define BTK_IO_LOGGER_LEVEL_HPP

namespace BTK {
namespace IO {
namespace LOGGING {

/// \brief A bit-field class for identifying logger message types.
///
/// For now, you can specify message "level" (e.g. Warning
/// versus Debug messages) flags and message "source" (where a message
/// originates) flags.  These flag sets occupy exclusive portions of the
/// bit-field, so they are combined using the bitwise-and operator (&).
/// Flags from the same set can be combined using the bitwise-or operator (|).
/// For example, here is one correct (if verbose) way to specify a mask for all 
/// messages from the container framework:
///
/// LoggerLevel l = CONTAINERS & ALL_LEVELS;
///
/// or, if you wanted to capture warning or error messages from everything:
///
/// LoggerLevel l = (ERROR | WARNING) & ALL_SOURCES;
///
/// Of course, in these cases, it would be sufficient to omit the
/// ALL_LEVELS and ALL_SOURCES flags, because these are implicit -- all of 
/// the message level flags imply an acceptance of all sources, and vice-versa.
/// A more complicated example might look like this:
///
/// LoggerLevel l = (ERROR | WARNING) & (ATOMS | CONTAINERS);
///
/// which would accept all error and warning messages from the atom and
/// container frameworks.
///
class LoggerLevel
{
 public:
  /// \brief Default constructor produces a mask that accepts nothing.
  LoggerLevel() : _lvl(0), _src(0) {}
  
  /// \brief Construct from level and source bitmasks. 
  LoggerLevel(unsigned l,unsigned s) : _lvl(l), _src(s) {}

  /// \brief Bitwise OR operator.
  const LoggerLevel operator|(LoggerLevel const & rhs) const
  {
    return LoggerLevel((_lvl | rhs._lvl), (_src | rhs._src));
  }

  /// \brief Bitwise AND operator.
  const LoggerLevel operator&(LoggerLevel const & rhs) const
  {
    return LoggerLevel((_lvl & rhs._lvl), (_src & rhs._src));
  }

  /// \brief Implicit conversion to boolean.
  operator bool() const 
  { 
    return (static_cast<bool>(_lvl) && static_cast<bool>(_src)); 
  }

private:
  unsigned _lvl;
  unsigned _src;
};

const unsigned ALL(static_cast<unsigned>(-1));

/// \group Message level constants.
//@{
static const LoggerLevel ERROR(1,ALL); ///< Error messages.
static const LoggerLevel WARNING(2,ALL); ///< Warning messages.
static const LoggerLevel TRACE(4,ALL); ///< Trace messages.
static const LoggerLevel DEBUG(8,ALL); ///< Debugging messages.

static const LoggerLevel ALL_LEVELS(ALL,ALL); ///< All message types.
//@}

/// \group Message source constants.
//@{
static const LoggerLevel ATOMS(ALL,1); ///< The atom framework.
static const LoggerLevel CONTAINERS(ALL,2); ///< The container framework.
static const LoggerLevel ITERATORS(ALL,4); ///< The iterator framework.
static const LoggerLevel ALGORITHMS(ALL,8); ///< The BTK algorithms.
static const LoggerLevel IO(ALL,16);  ///< The input/output framework.
static const LoggerLevel MATH(ALL,32); ///< The math framework.
static const LoggerLevel UTILITY(ALL,64); ///< The utility framework.

static const LoggerLevel ALL_SOURCES(ALL,ALL); ///< All message sources.
//@}

/// \brief Null LoggerLevel flag -- accept nothing.
static const LoggerLevel NOTHING(0,0);

/// \brief Universal LoggerLevel flag -- accept everything.
static const LoggerLevel EVERYTHING(ALL,ALL);

} // LOGGING
} // IO
} // BTK

#endif 
