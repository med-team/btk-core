// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file type_system.hpp
/// \brief Definition of the TypeSystem class.

#ifndef BTK_IO_TYPE_SYSTEM_HPP
#define BTK_IO_TYPE_SYSTEM_HPP

namespace BTK {
namespace IO {

/// \brief A class for representing a self-consistent set of molecular types.
///
/// A TypeSystem is a class that stores a set of molecular types that
/// are self-consistent for a particular problem.  For example, if your
/// problem requires a set of types that are specific to protein molecules,
/// you might create a ProteinTypeSystem, with atom, monomer and structure
/// types defined for proteins.  The Atom class is parameterized with
/// a TypeSystem, allowing for compile-time type safety for these
/// fundamental molecular types.
///
/// More abstractly, TypeSystem classes are envisioned as the general
/// solution to the problem of representing application-specific molecular
/// types.  Force-fields such as CHARMM and AMBER have their own
/// self-consistent sets of atom, monomer and molecule types -- a TypeSystem
/// class would be the correct way to represent these concepts in the BTK.
///
template <typename ElementDictionaryType,
	  typename AtomDictionaryType,
	  typename MonomerDictionaryType,
          typename StructureDictionaryType>
class TypeSystem
{
public:
  typedef ElementDictionaryType element_dictionary;
  typedef AtomDictionaryType atom_dictionary;
  typedef MonomerDictionaryType monomer_dictionary;
  typedef StructureDictionaryType structure_dictionary;
  
  typedef typename element_dictionary::id_type element_id_type;
  typedef typename atom_dictionary::id_type atom_id_type;
  typedef typename monomer_dictionary::id_type monomer_id_type;
  typedef typename structure_dictionary::id_type structure_id_type;

  typedef TypeSystem<element_dictionary,
                     atom_dictionary,
                     monomer_dictionary,
                     structure_dictionary> self_type;
  
  TypeSystem() {}
  TypeSystem(element_dictionary const & ed,
             atom_dictionary const & ad,
             monomer_dictionary const & md,
             structure_dictionary const & sd) :
    _ed(ed), _ad(ad), _md(md), _sd(sd) {}

  virtual ~TypeSystem() {}

  element_dictionary & get_element_dictionary() { return _ed; }
  atom_dictionary & get_atom_dictionary() { return _ad; }
  monomer_dictionary & get_monomer_dictionary() { return _md; }
  structure_dictionary & get_structure_dictionary() { return _sd; }

  element_dictionary const & get_element_dictionary() const { return _ed; }
  atom_dictionary const & get_atom_dictionary() const { return _ad; }
  monomer_dictionary const & get_monomer_dictionary() const { return _md; }
  structure_dictionary const & get_structure_dictionary() const { return _sd; }
  
private:
  element_dictionary _ed;
  atom_dictionary _ad;
  monomer_dictionary _md;
  structure_dictionary _sd;
};

} // namespace IO
} // namespace BTK

#endif 
