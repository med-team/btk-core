// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#include <iostream>
#include <iterator>
#include <algorithm>

#include <btk/core/io/cached_logger.hpp>

void
BTK::IO::LOGGING::
CachedLogger::log(std::string const & msg,
		  LoggerLevel level) const
{
  _messages.insert(std::pair<std::string,LoggerLevel>(msg,level));
}

void
BTK::IO::LOGGING::
CachedLogger::flush() const 
{
  std::map<std::string,LoggerLevel>::const_iterator i, end = _messages.end();

  // call the previous logger with each unique message 
  for (i = _messages.begin(); i != end; ++i) {
    _previous_logger.log(i->first,i->second);
  }
}
