// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file type_system_proxy.hpp
/// \brief Definition of the TypeSystemProxy class.

#ifndef BTK_IO_TYPE_SYSTEM_PROXY_HPP
#define BTK_IO_TYPE_SYSTEM_PROXY_HPP

#include <boost/shared_ptr.hpp>

namespace BTK {
namespace IO {

/// A class for representing TypeSystem objects by reference.
///
/// This class differs from a simple reference in two ways:
/// <ul> 
///  <li> It exposes a complete TypeSystem interface</li>
///  <li> It can be default constructed.</li>
/// </ul>
///
/// These properties allow it to be used in objects that must be
/// default constructible (e.g. BTK::ATOMS::Atom), while still appearing 
/// as a normal TypeSystem object.  Any object compliant with the 
/// TypeSystemConcept can be proxied by this class.
///  
/// Internally, the proxied TypeSystem is stored via a reference-counted
/// pointer.  You are therefore guaranteed that a proxied TypeSystem will
/// not be deallocated until the last proxy referring to the TypeSystem is
/// destroyed.
///
template <typename TS>
class TypeSystemProxy
{
public:
  typedef TS type_system;
  typedef TypeSystemProxy<type_system> self_type;
  
  /// \name TypeSystem type definitions.
  //@{
  typedef typename type_system::element_dictionary element_dictionary;
  typedef typename type_system::atom_dictionary atom_dictionary;
  typedef typename type_system::monomer_dictionary monomer_dictionary;
  typedef typename type_system::structure_dictionary structure_dictionary;

  typedef typename element_dictionary::id_type element_id_type;
  typedef typename atom_dictionary::id_type atom_id_type;
  typedef typename monomer_dictionary::id_type monomer_id_type;
  typedef typename structure_dictionary::id_type structure_id_type;
  //@}
  
  /// Default constructor.
  /// A new TypeSystem is created.
  TypeSystemProxy() : 
    _ptr(new type_system()) {}

  /// Copy constructor.
  /// A new proxy is created, referring to the same TypeSystem held in
  /// the src proxy.
  TypeSystemProxy(self_type const & src) :
    _ptr(src._ptr) {}

  /// Construct from a TypeSystem instance.
  /// The source TypeSystem is copied, and this copy is used to create a
  /// new TypeSystemProxy.  Thus, changes to the original TypeSystem will
  /// not be reflected in the proxy object. 
  TypeSystemProxy(type_system const & ts) : 
    _ptr(new type_system(ts)) {}

  /// \name TypeSystem interface methods.
  //@{
  element_dictionary & get_element_dictionary() 
  { return _ptr->get_element_dictionary(); }
  atom_dictionary & get_atom_dictionary() 
  { return _ptr->get_atom_dictionary(); }
  monomer_dictionary & get_monomer_dictionary() 
  { return _ptr->get_monomer_dictionary(); }
  structure_dictionary & get_structure_dictionary()
  { return _ptr->get_structure_dictionary(); }


  element_dictionary const & get_element_dictionary() const 
  { return _ptr->get_element_dictionary(); }
  atom_dictionary const & get_atom_dictionary() const
  { return _ptr->get_atom_dictionary(); }
  monomer_dictionary const & get_monomer_dictionary() const
  { return _ptr->get_monomer_dictionary(); }
  structure_dictionary const & get_structure_dictionary() const 
  { return _ptr->get_structure_dictionary(); }
  //@}

  /// Deep-copy the proxied TypeSystem object.
  self_type clone() const  
  {
    return self_type(*_ptr);
  }

  bool operator==(self_type const & rhs) const
  {
    return _ptr == rhs._ptr;
  }

  bool operator!=(self_type const & rhs) const
  {
    return _ptr != rhs._ptr;
  }

private:
  boost::shared_ptr<type_system> _ptr;
};

} // namespace IO
} // namespace BTK

#endif
