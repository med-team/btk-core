// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file dictionary.hpp
/// \brief Definition of the Dictionary class.

#ifndef BTK_IO_DICTIONARY_HPP
#define BTK_IO_DICTIONARY_HPP

#include <cctype>
#include <string>
#include <vector>
#include <map>

#include <btk/core/common/debugging.hpp>
#include <btk/core/utility/type_id_traits.hpp>

namespace BTK {
namespace IO {
  
template <typename IDType>
class Dictionary 
{
  typedef std::map<IDType,std::string> data_map;
  typedef std::map<std::string,typename data_map::iterator> symbol_index;
  
public:
  typedef Dictionary<IDType> self_type;

  typedef IDType id_type;
  typedef typename data_map::value_type value_type;
  typedef typename data_map::const_iterator const_iterator;
  typedef typename data_map::const_reverse_iterator const_reverse_iterator;

  Dictionary(bool case_sensitive = true) : 
    _data(), _symbol_index(), _case_sensitive(case_sensitive) {}

  Dictionary(self_type const & d) :
    _data(d._data), _symbol_index(), 
    _case_sensitive(d._case_sensitive)
  {
    // data already copied; build the index.
    for (typename data_map::iterator i = _data.begin(); 
         i != _data.end(); ++i) {
      update_index(i->second,i);
    }
  }
  
  virtual ~Dictionary() {}

  const_iterator begin() const { return _data.begin(); }
  const_iterator end() const { return _data.end(); }

  const_reverse_iterator rbegin() const { return _data.rbegin(); }
  const_reverse_iterator rend() const { return _data.rend(); }

  virtual const_iterator find(id_type id) const { return _data.find(id); }

  virtual const_iterator find(std::string const & sym) const 
  {
    typename symbol_index::const_iterator i;

    if (!_case_sensitive) {
      i = _symbol_index.find(make_upper_case(sym));
    } else {
      i = _symbol_index.find(sym);
    }

    if (i != _symbol_index.end()) {
      return i->second;
    }

    return _data.end();
  }

  /// Get the type ID for a name, creating a new ID if necessary.
  ///
  /// Like find(), this method will attempt to find the type ID corresponding
  /// to a name.  Unlike find(), it will also attempt to add a new type ID to
  /// the dictionary, if the initial search fails.
  ///
  /// Type IDs are added iff the type is dynamic (i.e., new type ID values can
  /// be created), and the dictionary is mutable.  Otherwise the unknown type
  /// ID value (determined using BTK::UTILITY::TypeIDTraits) is returned.
  id_type get_type(std::string const & name)
  {
    typedef BTK::UTILITY::TypeIDTraits<id_type> type_id_traits;
    const_iterator i = find(name);
    
    if (i == end()) { // name not found! try to add a new type.

      // if this is a dynamic type, add a new type id
      if (type_id_traits::dynamic) {
        id_type new_id;
        
        if (_data.empty()) {            
          // dictionary empty? use first type!
          new_id = type_id_traits::first();
        } else {  
          // get new id using generator function 
          // (the id after last defined id value)
          new_id = type_id_traits::next(rbegin()->first);
        }

        if (insert(value_type(new_id,name))) {
          // insert of new type succeeded -- return it
          return new_id;
        }
      }

      // if here, either the type ID wasn't dynamic, or the dictionary
      // insert failed.  Return unknown.
      return type_id_traits::unknown();
    }

    // normal case:  ID found.  Return it.
    return i->first;
  }

  virtual bool insert(value_type const & vt)
  {
    std::pair<typename data_map::iterator,bool> tmp;

    if (!_case_sensitive) {
      tmp = _data.insert(value_type(vt.first,
                                    make_upper_case(vt.second)));
    } else {
      tmp = _data.insert(vt);
    }

    if (tmp.second) {
      // insert succeeded, add to index.
      if (!update_index(vt.second,tmp.first)) {
        // index insert failed -- symbol already exists.
        // remove entry from data and return failure
        _data.erase(tmp.first);
        return false;
      }
    } else return false; // insert failed
     
    return true;
  }
  
  virtual bool add_alternate_symbol(id_type id,
                                    std::string const & symbol) 
  {
    typename data_map::iterator i = _data.find(id);
    if (i == _data.end()) return false;
    
    if (!_case_sensitive) 
      return update_index(make_upper_case(symbol),i);
    else
      return update_index(symbol,i);
  }

  virtual void clear() { _data.clear(); _symbol_index.clear(); }

protected:
  
  bool update_index(std::string const & s,
		    typename data_map::iterator i)
  {
    typedef typename symbol_index::value_type vt;

    if (!_case_sensitive)
      return _symbol_index.insert(vt(make_upper_case(s),i)).second;
    else
      return _symbol_index.insert(vt(s,i)).second; 
  }

  std::string make_upper_case(std::string const & s) const
  {
    std::string tmp(s);

    for (std::string::iterator i = tmp.begin(); i != tmp.end(); ++i)
      *i = std::toupper(*i);

    return tmp;
  }
  
private:
  data_map _data;
  symbol_index _symbol_index;
  bool _case_sensitive;
};

} // IO
} // BTK

#endif 
