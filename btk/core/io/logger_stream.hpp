// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

#ifndef BTK_IO_LOGGER_STREAM_HPP
#define BTK_IO_LOGGER_STREAM_HPP

#include <ostream>
#include <sstream>
#include <string>

#include <btk/core/io/logger.hpp>

namespace BTK {
namespace IO {
namespace LOGGING {

template <typename CharType, typename TraitsType = std::char_traits<CharType> >
  class LoggerStreambuf : public std::basic_stringbuf<CharType,TraitsType>
{
public:
  typedef std::basic_stringbuf<CharType,TraitsType> base_buf;
  
  LoggerStreambuf(LoggerInterface const & logger,
                  LoggerLevel level) : 
    _logger(logger), _log_level(level) {}
  
  ~LoggerStreambuf() 
  {
    sync();
  }
  
protected:
  
  int sync()
  {
    _logger.log(base_buf::str(),_log_level);
    base_buf::str(std::string());
    return 0;
  }
  
private:
  LoggerInterface const & _logger;
  LoggerLevel _log_level;
};

template <typename CharType, typename TraitsType = std::char_traits<CharType> >
class LoggerStream : public std::basic_ostream<CharType,TraitsType>
{
public:
  typedef std::basic_ostream<CharType,TraitsType> base_stream;
  typedef LoggerStreambuf<CharType,TraitsType> LoggerStreambuf;

  LoggerStream(LoggerLevel level) : 
    base_stream(new LoggerStreambuf(Logger::instance(),level)) {}

  LoggerStream(LoggerInterface const & logger,
               LoggerLevel level) :
    base_stream(new LoggerStreambuf(logger,level)) {}

  ~LoggerStream() { delete base_stream::rdbuf(); }
};

} // namespace LOGGING
} // namespace IO
} // namespace BTK

#endif 
