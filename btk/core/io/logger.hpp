// -*- mode: c++; indent-tabs-mode: nil; -*-
//
//The Biomolecule Toolkit (BTK) is a C++ library for use in the
//modeling, analysis, and design of biological macromolecules.
//Copyright (C) 2006, Tim Robertson <kid50@users.sourceforge.net>
//
//This program is free software; you can redistribute it and/or modify
//it under the terms of the GNU Lesser General Public License as published
//by the Free Software Foundation; either version 2.1 of the License, or (at
//your option) any later version.
//
//This program is distributed in the hope that it will be useful,  but
//WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//Lesser General Public License for more details.
//
//You should have received a copy of the GNU Lesser General Public License
//along with this program; if not, write to the Free Software Foundation,
//Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

/// \file logger.hpp
/// Declaration of the Logger class.

#ifndef BTK_IO_LOGGER_HPP
#define BTK_IO_LOGGER_HPP

#include <string>
#include <stack>

#include <boost/shared_ptr.hpp>

#include <btk/core/io/logger_level.hpp>

namespace BTK {
namespace IO {
namespace LOGGING {

/// \brief An abstract base class specifying the interface for 
///        all BTK Logger objects.
struct LoggerInterface
{
  virtual ~LoggerInterface() {}

  /// \brief Write a log message to a Logger.
  /// A message passed to log may be cached or filtered as necessary.
  /// If immediate output is desired, use the error() method.
  virtual void log(std::string const & msg,
		   LoggerLevel msg_level = EVERYTHING) const = 0;

  /// \brief Write an error to a Logger for immediate output.
  /// A message passed to this method will be bypass all filtering/caching
  /// mechanisms.  Use judiciously.
  virtual void error(std::string const & msg) const = 0;
};

class SimpleLogger : public LoggerInterface
{
public:
  virtual void log(std::string const & msg,
		   LoggerLevel msg_level = EVERYTHING) const;

  virtual void error(std::string const & msg) const;
};

/// \brief A class for managing all BTK-related logging and error output.
///
/// The Logger is actually an aggregate class, containing a stack of Logger
/// objects, each exposing a log() and error() method.  This class makes it 
/// possible to control all non-essential library output centrally, and thus 
/// allows library users to quickly and easily change the default
/// logging/error-handling behaviour of the library.
///
/// To override default logging behaviour, push a new Logger object onto
/// the stack, using the push_logger() method.  Likewise, use the pop_logger()
/// method to revert to an older logging behaviour.
///
class Logger
{
 public:
  typedef boost::shared_ptr<LoggerInterface> logger_ptr;

  virtual ~Logger() {}

  /// \brief Get the current Logger object.
  static LoggerInterface const & instance();

  /// \brief Push a new Logger object onto the stack.
  /// The pushed Logger object will become the new default Logger. 
  static void push_logger(logger_ptr p) { _loggers.push(p); }

  /// \brief Pop the topmost Logger object from the stack.
  /// After this call, the logging behaviour will be the behaviour of the next
  /// Logger object in the stack, if any (and default behaviour, otherwise).
  static void pop_logger() 
  {
    if (!_loggers.empty()) _loggers.pop();
  }

 protected:
  Logger() {}
   
 private:
  // disallow copy construction and assignment
  Logger(Logger const &);
  Logger & operator=(Logger const &);

  static std::stack<logger_ptr> _loggers;
};

} // LOGGING
} // IO
} // BTK

#endif // BTK_LOGGER_H
